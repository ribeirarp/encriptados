ESX = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj)
			ESX = obj 
		end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('route68_b:start')
AddEventHandler('route68_b:start', function()
	ESX.TriggerServerCallback('route68_b:check_money', function(quantity)
		if quantity >= 100 then
			SendNUIMessage({
				type = "enableui",
				enable = true,
				coins = quantity
			})
			SetNuiFocus(true, true)
		else
			--ESX.ShowNotification('Você precisa de pelo menos 100 fichas para jogar!')
			exports['mythic_notify']:SendAlert('error', 'Tens que ter pelo menos 100 fichas para jogar!')
		end
	end, '')
	--roulette_menu()
end)

RegisterNUICallback('escape', function(data, cb)
	cb('ok')
	SetNuiFocus(false, false)
	SendNUIMessage({
		type = "enableui",
		enable = false
	})
end)

RegisterNUICallback('card', function(data, cb)
	cb('ok')
	TriggerServerEvent('InteractSound_SV:PlayOnSource', 'PlayCard', 1.0)
end)

RegisterNUICallback('bet', function(data, cb)
	cb('ok')
	TriggerServerEvent('InteractSound_SV:PlayOnSource', 'betup', 1.0)
end)

RegisterNUICallback('escape2', function(data, cb)
	cb('ok')
	SetNuiFocus(false, false)
	SendNUIMessage({
		type = "enableui",
		enable = false
	})
	--TriggerEvent('pNotify:SendNotification', {text = 'Você não tem mais fichas!'})
	exports['mythic_notify']:SendAlert('error', 'Não tens mais fichas!')
end)

RegisterNUICallback('WinBet', function(data, cb)
	cb('ok')
	local count = data.bets
	TriggerServerEvent('route68_b:givemoney', count, 2)
end)

RegisterNUICallback('TieBet', function(data, cb)
	cb('ok')
	local count = data.bets
	TriggerServerEvent('route68_b:givemoney', count, 1)
end)

RegisterNUICallback('LostBet', function(data, cb)
	cb('ok')
	local count = data.bets
	--TriggerEvent('pNotify:SendNotification', {text = "Perdeu "..count.." Fichas!"})
	exports['mythic_notify']:SendAlert('error', 'Perdeste '..count..' fichas!')
end)

RegisterNUICallback('Status', function(data, cb)
	cb('ok')
	--TriggerEvent('pNotify:SendNotification', {text = data.tekst})
	exports['mythic_notify']:SendAlert('inform', {data.tekst})
end)

RegisterNUICallback('StartPartia', function(data, cb)
	cb('ok')
	local count = data.bets
	TriggerServerEvent('route68_b:removemoney', count)
end)