local ESX = nil
local isDead = false
local IsHandcuffed = false
local playerPed = nil
local LibAnim				= 'mp_arrest_paired'	
local AnimPrender 			= 'cop_p2_fwd_left'	
local AnimPreso				= 'crook_p2_fwd_left'	
local LibAnimDesalgemar     = 'melee@large_wpn@streamed_core'
local AnimDesalgemar        = 'ground_attack_on_spot'
local DragStatus              = {}
local inVehicle = false
DragStatus.IsDragged          = false

local Keys = {
    ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
    ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
    ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
    ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
    ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
    ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
    ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
    ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
    ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
    end
end)

RegisterNetEvent('algemas:CMD_algemar')
AddEventHandler('algemas:CMD_algemar', function()
	local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
    if closestPlayer ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('algemas:algemar', GetPlayerServerId(closestPlayer))
        Wait(5000)
    end
end)

RegisterNetEvent('algemas:CMD_desalgemar')
AddEventHandler('algemas:CMD_desalgemar', function()
	local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
    if closestPlayer ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('algemas:desalgemar', GetPlayerServerId(closestPlayer))
    end
end)

RegisterNetEvent('algemas:CMD_drag')
AddEventHandler('algemas:CMD_drag', function()
	local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
    if closestPlayer ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('algemas:drag', GetPlayerServerId(closestPlayer))
    end
end)

RegisterNetEvent('algemas:CMD_putInVehicle')
AddEventHandler('algemas:CMD_putInVehicle', function()
	local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
    if closestPlayer ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('algemas:putInVehicle', GetPlayerServerId(closestPlayer))
    end
end)

RegisterNetEvent('algemas:CMD_OutVehicle')
AddEventHandler('algemas:CMD_OutVehicle', function()
	local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()
    if closestPlayer ~= -1 and closestDistance <= 3.0 then
        TriggerServerEvent('algemas:OutVehicle', GetPlayerServerId(closestPlayer))
    end
end)

RegisterNetEvent('algemas:algemar')
AddEventHandler('algemas:algemar', function(valor)
	if valor == 1 then
		Citizen.Wait(3000)
	end
	IsHandcuffed = not IsHandcuffed
	local playerPed = PlayerPedId()
	Citizen.CreateThread(function()
		if IsHandcuffed then

            TriggerEvent('skinchanger:getSkin', function(skin)
                print("sexo" .. skin.sex)
                if skin.sex == 0 then --homem
                    SetPedComponentVariation(playerPed, 7, 47, 0, 0)
                else -- mulher
                    SetPedComponentVariation(playerPed, 7, 25, 0, 0)
                end
            end)
			SetEnableHandcuffs(playerPed, true)
			DisablePlayerFiring(playerPed, true)
			SetCurrentPedWeapon(playerPed, GetHashKey('WEAPON_UNARMED'), true) -- unarm player
			SetPedCanPlayGestureAnims(playerPed, false)
			--FreezeEntityPosition(playerPed, true)
			DisplayRadar(false)

		else
			ClearPedSecondaryTask(playerPed)
			SetEnableHandcuffs(playerPed, false)
			DisablePlayerFiring(playerPed, false)
			SetPedCanPlayGestureAnims(playerPed, true)
			--FreezeEntityPosition(playerPed, false)
			DisplayRadar(true)
		end
	end)

end)

RegisterNetEvent('algemas:targetPrender')
AddEventHandler('algemas:targetPrender', function(target)

	local playerPed = GetPlayerPed(-1)
	local targetPed = GetPlayerPed(GetPlayerFromServerId(target))

	RequestAnimDict(LibAnim)

	while not HasAnimDictLoaded(LibAnim) do
		Citizen.Wait(10)
	end

	AttachEntityToEntity(GetPlayerPed(-1), targetPed, 11816, -0.1, 0.45, 0.0, 0.0, 0.0, 20.0, false, false, false, false, 20, false)
	TaskPlayAnim(playerPed, LibAnim, AnimPreso, 8.0, -8.0, 5500, 33, 0, false, false, false)

	Citizen.Wait(950)
	DetachEntity(GetPlayerPed(-1), true, false)

end)

RegisterNetEvent('algemas:Prender')
AddEventHandler('algemas:Prender', function()
	local playerPed = GetPlayerPed(-1)

	RequestAnimDict(LibAnim)

	while not HasAnimDictLoaded(LibAnim) do
		Citizen.Wait(10)
	end

	TaskPlayAnim(playerPed, LibAnim, AnimPrender, 8.0, -8.0, 4200, 33, 0, false, false, false)

	Citizen.Wait(3000)

end)

RegisterNetEvent('algemas:targetDesalgemar')
AddEventHandler('algemas:targetDesalgemar', function(target, hasWrench, animation)
	
	local playerPed = GetPlayerPed(-1)
	local targetPed = GetPlayerPed(GetPlayerFromServerId(target))
	
	if animation then
		
		AttachEntityToEntity(GetPlayerPed(-1), targetPed, 11816, -0.1, 0.85, 0.0, 0.0, 0.0, 20.0, false, false, false, false, 20, false)

		if not hasWrench then
			Wait(4000)
		else
			Wait(1500)
		end
	
    	DetachEntity(GetPlayerPed(-1), true, false)
	end
	
    IsHandcuffed = false

    ClearPedSecondaryTask(playerPed)
    SetEnableHandcuffs(playerPed, false)
    DisablePlayerFiring(playerPed, false)
    SetPedCanPlayGestureAnims(playerPed, true)
    FreezeEntityPosition(playerPed, false)
	DisplayRadar(true)
	SetPedComponentVariation(playerPed, 7, -1, 0, 0)
	
end)

RegisterNetEvent('algemas:desalgemar')
AddEventHandler('algemas:desalgemar', function()
    plyPed = GetPlayerPed(-1)
	local retval, weaponHash = GetCurrentPedWeapon(plyPed, 1)
	
    if not (weaponHash == GetHashKey("WEAPON_WRENCH")) then
        SetCurrentPedWeapon(plyPed, GetHashKey('WEAPON_WRENCH'), true)
        Wait(3000)
	else
		Wait(500)
	end
     
    while not HasAnimDictLoaded(LibAnimDesalgemar) do RequestAnimDict(LibAnimDesalgemar); Citizen.Wait(0); end
    TaskPlayAnim( plyPed, LibAnimDesalgemar, AnimDesalgemar, 8.0, 8.0, -1, 4, 0, 0, 0, 0 )     
    Citizen.Wait(0)
    while IsEntityPlayingAnim(plyPed, LibAnimDesalgemar, AnimDesalgemar, 3) do Citizen.Wait(0); end   
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)

        if IsHandcuffed then
    		local playerPed = PlayerPedId()
			DisableControlAction(0, 1, true) -- Disable pan
			DisableControlAction(0, 2, true) -- Disable tilt
			DisableControlAction(0, 24, true) -- Attack
			DisableControlAction(0, 257, true) -- Attack 2
			DisableControlAction(0, 25, true) -- Aim
			DisableControlAction(0, 263, true) -- Melee Attack 1
			--DisableControlAction(0, Keys['W'], true) -- W
			--DisableControlAction(0, Keys['A'], true) -- A
			--DisableControlAction(0, 31, true) -- S (fault in Keys table!)
			--DisableControlAction(0, 30, true) -- D (fault in Keys table!)

			DisableControlAction(0, Keys['R'], true) -- Reload
			DisableControlAction(0, Keys['SPACE'], true) -- Jump
			DisableControlAction(0, Keys['Q'], true) -- Cover
			DisableControlAction(0, Keys['TAB'], true) -- Select Weapon
			--DisableControlAction(0, Keys['F'], true) -- Also 'enter'?

			DisableControlAction(0, Keys['F1'], true) -- Disable phone
			DisableControlAction(0, Keys['F2'], true) -- Inventory
			DisableControlAction(0, Keys['F3'], true) -- Animations
			DisableControlAction(0, Keys['F6'], true) -- Job

			DisableControlAction(0, Keys['V'], true) -- Disable changing view
			DisableControlAction(0, Keys['C'], true) -- Disable looking behind
			DisableControlAction(0, Keys['X'], true) -- Disable clearing animation
			DisableControlAction(2, Keys['P'], true) -- Disable pause screen

			DisableControlAction(0, 59, true) -- Disable steering in vehicle
			DisableControlAction(0, 71, true) -- Disable driving forward in vehicle
			DisableControlAction(0, 72, true) -- Disable reversing in vehicle

			DisableControlAction(2, Keys['LEFTCTRL'], true) -- Disable going stealth

--			DisableControlAction(0, 47, true)  -- Disable weapon
			DisableControlAction(0, 264, true) -- Disable melee
			DisableControlAction(0, 257, true) -- Disable melee
			DisableControlAction(0, 140, true) -- Disable melee
			DisableControlAction(0, 141, true) -- Disable melee
			DisableControlAction(0, 142, true) -- Disable melee
			DisableControlAction(0, 143, true) -- Disable melee
			DisableControlAction(0, 75, true)  -- Disable exit vehicle
			DisableControlAction(27, 75, true) -- Disable exit vehicle
			if IsEntityPlayingAnim(playerPed, 'mp_arresting', 'idle', 3) ~= 1 then
				ESX.Streaming.RequestAnimDict('mp_arresting', function()
					TaskPlayAnim(playerPed, 'mp_arresting', 'idle', 8.0, -8, -1, 49, 0.0, false, false, false)
				end)
			end
		else
			Citizen.Wait(500)
		end
	end
end)





RegisterNetEvent('algemas:drag')
AddEventHandler('algemas:drag', function(copID)
	if not IsHandcuffed then
		TriggerServerEvent("algemas:notification", copID, "error","Essa pessoa não está algemada")
		return
	end

	DragStatus.IsDragged = not DragStatus.IsDragged
	DragStatus.CopId     = tonumber(copID)
end)

Citizen.CreateThread(function()
	local playerPed
	local targetPed

	while true do
		Citizen.Wait(1)

		if IsHandcuffed then
			playerPed = PlayerPedId()

			if DragStatus.IsDragged then
				targetPed = GetPlayerPed(GetPlayerFromServerId(DragStatus.CopId))

				-- undrag if target is in an vehicle
				if not inVehicle then
					AttachEntityToEntity(playerPed, targetPed, 11816, 0.54, 0.54, 0.0, 0.0, 0.0, 0.0, false, false, false, false, 2, true)
				else
					DragStatus.IsDragged = false
					DetachEntity(playerPed, true, false)
				end

			else
				DetachEntity(playerPed, true, false)
			end
		else
			Citizen.Wait(500)
		end
	end
end)

Citizen.CreateThread(function()
	local playerPed

	while true do
		Citizen.Wait(500)
		if IsHandcuffed then
			playerPed = PlayerPedId()
			local inVehicle = IsPedSittingInAnyVehicle(targetPed)
		end
	end
end)



RegisterNetEvent('algemas:putInVehicle')
AddEventHandler('algemas:putInVehicle', function()
	local playerPed = PlayerPedId()
	local coords    = GetEntityCoords(playerPed)
	if not IsHandcuffed then
		
		return
	end

	if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then
		local vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)

		if DoesEntityExist(vehicle) then
			local maxSeats = GetVehicleMaxNumberOfPassengers(vehicle)
			local freeSeat = nil

			for i=maxSeats - 1, 0, -1 do
				if IsVehicleSeatFree(vehicle, i) then
					freeSeat = i
					break
				end
			end

			if freeSeat ~= nil then
				TaskWarpPedIntoVehicle(playerPed, vehicle, freeSeat)
				DragStatus.IsDragged = false
			end
		end
	end
end)

RegisterNetEvent('algemas:OutVehicle')
AddEventHandler('algemas:OutVehicle', function()
	local playerPed = PlayerPedId()

	if not IsPedSittingInAnyVehicle(playerPed) then
		return
	end

	local vehicle = GetVehiclePedIsIn(playerPed, false)
	TaskLeaveVehicle(playerPed, vehicle, 16)
end)