menuConfigs = {
    ['menu'] = {
        enableMenu = function()
            return true
        end,
        data = {
            keybind = "G",
            style = {
                sizePx = 650,
                slices = {
                    default = { ['fill'] = '#000000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.60 },
                    hover = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 },
                    selected = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 }
                },
                titles = {
                    default = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    hover = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    selected = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' }
                },
                icons = {
                    width = 64,
                    height = 64
                }
            },
            wheels = {
                {
                    navAngle = 270,
                    minRadiusPercent = 0.25,
                    maxRadiusPercent = 0.60,
                    labels = {"imgsrc:close.png", "imgsrc:veiculo.png", "imgsrc:animacoes.png", "imgsrc:estilodeandar.png",  "imgsrc:inventario.png", "imgsrc:telemovel.png", "imgsrc:mudarsim.png", "imgsrc:binoculos.png", "imgsrc:interacoes.png"},
                    commands = {"", "submenu1", "submenu2", "submenu4",  "openinv", "openphone", "mudarnumero", "binoculos", "submenu5"}
                },
                {
                    navAngle = 288,
                    minRadiusPercent = 0.6,
                    maxRadiusPercent = 0.9,
                    labels = {"imgsrc:cortarcoms.png", "imgsrc:pegar.png", "imgsrc:cavalitas.png" ,"imgsrc:refem.png", "imgsrc:morto.png" ,"imgsrc:cruzarbracos.png", "imgsrc:maosnoar.png", "imgsrc:faturas.png", "imgsrc:cartoes.png"},
                    commands = {"me Cortar comunicações", "pegar", "cavalitas","refem", "e passout3", "e crossarms", "levantarbracos","abrirmenufaturas", "abrircartoes"}
                }
            }
        }
    }
}

-- Submenu configuration
subMenuConfigs = {
    ['submenu1'] = {
        data = {
            keybind = "F6",
            style = {
                sizePx = 650,
                slices = {
                    default = { ['fill'] = '#000000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.60 },
                    hover = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 },
                    selected = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 }
                },
                titles = {
                    default = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    hover = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    selected = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' }
                },
                icons = {
                    width = 64,
                    height = 64
                }
            },
            wheels = {
                {
                    navAngle = 270,
                    minRadiusPercent = 0.25,
                    maxRadiusPercent = 0.60,
                    labels = {"imgsrc:close.png","imgsrc:veiculo.png", "imgsrc:animacoes.png", "imgsrc:estilodeandar.png",  "imgsrc:inventario.png", "imgsrc:telemovel.png", "imgsrc:mudarsim.png", "imgsrc:binoculos.png", "imgsrc:interacoes.png"},
                    commands = {"", "submenu1", "submenu2", "submenu4",  "openinv", "openphone", "mudarnumero", "binoculos","submenu5"}
                },
                {
                    navAngle = 270,
                    minRadiusPercent = 0.6,
                    maxRadiusPercent = 0.9,
                    labels = {"imgsrc:pilotoautomatico.png", "imgsrc:metermatricula.png", "imgsrc:portadireita.png", "imgsrc:portatraseiradireita.png", "imgsrc:capo.png" , "imgsrc:portamalas.png" , "imgsrc:portatraseiraesquerda.png", "imgsrc:portaesquerda.png", "imgsrc:removermatricula.png"},
                    commands = {"veh pilot", "metermatricula" , "door 2", "door 4", "hood", "trunk", "door 3", "door 1", "tirarmatricula"}
                }
            }
        }
    },
    ['submenu2'] = {
        data = {
            keybind = "F6",
            style = {
                sizePx = 650,
                slices = {
                    default = { ['fill'] = '#000000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.60 },
                    hover = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 },
                    selected = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 }
                },
                titles = {
                    default = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    hover = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    selected = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' }
                },
                icons = {
                    width = 64,
                    height = 64
                }
            },
            wheels = {
                {
                    navAngle = 270,
                    minRadiusPercent = 0.25,
                    maxRadiusPercent = 0.60,
                    labels = {"imgsrc:close.png","imgsrc:veiculo.png", "imgsrc:animacoes.png", "imgsrc:estilodeandar.png",  "imgsrc:inventario.png", "imgsrc:telemovel.png", "imgsrc:mudarsim.png", "imgsrc:binoculos.png", "imgsrc:interacoes.png"},
                    commands = {"e c", "submenu1", "submenu2", "submenu4", "openinv", "openphone", "mudarnumero", "binoculos","submenu5"}
                },
                {
                    navAngle = 288,
                    minRadiusPercent = 0.6,
                    maxRadiusPercent = 0.9,
                    labels = {"Sentar", "Encostar", "Fumar", "Abraçar", "Mijar", "Deitar", "Galinha", "Namaste", "Cortar garganta", "Cabeçada", "Facepalm", "Pensar", "Yeah", "Dedo do meio"},
                    commands = {"e sit3", "e lean", "e smoke", "e hug", "e pee", "e sunbathe", "e chicken", "e namaste", "e cutthroat", "e headbutt", "e facepalm", "e think", "e yeah","e finger"}
                }
            }
        }
    },
    ['submenu3'] = {
        data = {
            keybind = "F6",
            style = {
                sizePx = 650,
                slices = {
                    default = { ['fill'] = '#000000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.60 },
                    hover = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 },
                    selected = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 }
                },
                titles = {
                    default = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    hover = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    selected = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' }
                },
                icons = {
                    width = 64,
                    height = 64
                }
            },
            wheels = {
                {
                    navAngle = 270,
                    minRadiusPercent = 0.25,
                    maxRadiusPercent = 0.60,
                    labels = {"imgsrc:close.png","Véiculo", "imgsrc:animacoes.png", "Estilos de andar",  "Inventário", "Telemóvel", "Distância de microfone", "imgsrc:mudarsim.png", "imgsrc:modouber.png"},
                    commands = {"","submenu1", "submenu2", "submenu4",  "openinv", "openphone", "mudarnumero", "binoculos","submenu5"}
                },
                {
                    navAngle = 288,
                    minRadiusPercent = 0.6,
                    maxRadiusPercent = 0.9,
                    labels = {"Cortar comunicações", "submenu3_b", "submenu3_c", "submenu3_d", "submenu3_e", "submenu3_f", "submenu3_g", "submenu3_h", "submenu3_i", "submenu3_j"},
                    commands = {"me Cortar comunicações", "submenu3_b", "submenu3_c", "submenu3_d", "submenu3_e", "submenu3_f", "submenu3_g", "submenu3_h", "submenu3_i", "submenu3_j"}
                }
            }
        }
    },
    ['submenu4'] = {
        data = {
            keybind = "F6",
            style = {
                sizePx = 650,
                slices = {
                    default = { ['fill'] = '#000000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.60 },
                    hover = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 },
                    selected = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 }
                },
                titles = {
                    default = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    hover = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    selected = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' }
                },
                icons = {
                    width = 64,
                    height = 64
                }
            },
            wheels = {
                {
                    navAngle = 270,
                    minRadiusPercent = 0.25,
                    maxRadiusPercent = 0.60,
                    labels = {"imgsrc:close.png","imgsrc:veiculo.png", "imgsrc:animacoes.png", "imgsrc:estilodeandar.png",  "imgsrc:inventario.png", "imgsrc:telemovel.png", "imgsrc:mudarsim.png", "imgsrc:binoculos.png", "imgsrc:interacoes.png"},
                    commands = {"walk reset","submenu1", "submenu2", "submenu4",  "openinv", "openphone", "mudarnumero", "binoculos","submenu5"}
                },
                {
                    navAngle = 288,
                    minRadiusPercent = 0.6,
                    maxRadiusPercent = 0.9,
                    labels = {"Aleijado", "Aleijado 2","Alien", "Arrogante", "Apressado", "Bêbado", "Gangster", "Segurar mochila", "Gay", "Atleta", "Assutado"},
                    commands = {"walk injured", "walk lester" ,"walk alien", "walk arrogant", "walk hurry", "walk drunk3", "walk gangster", "walk hiking", "walk maneater", "walk runner", "walk scared"}
                }
            }
        }
    },
    ['submenu5'] = {
        data = {
            keybind = "F6",
            style = {
                sizePx = 650,
                slices = {
                    default = { ['fill'] = '#000000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.60 },
                    hover = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 },
                    selected = { ['fill'] = '#ff8000', ['stroke'] = '#000000', ['stroke-width'] = 3, ['opacity'] = 0.80 }
                },
                titles = {
                    default = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    hover = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' },
                    selected = { ['fill'] = '#ffffff', ['stroke'] = 'none', ['font'] = 'Helvetica', ['font-size'] = 16, ['font-weight'] = 'bold' }
                },
                icons = {
                    width = 64,
                    height = 64
                }
            },
            wheels = {
                {
                    navAngle = 270,
                    minRadiusPercent = 0.25,
                    maxRadiusPercent = 0.60,
                    labels = {"imgsrc:close.png","imgsrc:veiculo.png", "imgsrc:animacoes.png", "imgsrc:estilodeandar.png",  "imgsrc:inventario.png", "imgsrc:telemovel.png", "imgsrc:mudarsim.png", "imgsrc:binoculos.png", "imgsrc:interacoes.png"},
                    commands = {"","submenu1", "submenu2", "submenu4", "openinv", "openphone", "mudarnumero", "binoculos","submenu5"}
                },
                {
                    navAngle = 288,
                    minRadiusPercent = 0.6,
                    maxRadiusPercent = 0.9,
                    labels = {"imgsrc:algemar.png", "imgsrc:desalgemar.png", "imgsrc:meternocarro.png", "imgsrc:tirardocarro.png", "imgsrc:arrastar.png", "imgsrc:roubar.png"},
                    commands = {"algemar", "desalgemar", "meternocarro", "tirardocarro", "arrastar", "openthiefmenu"}
                }
            }
        }
    },
    
    
}