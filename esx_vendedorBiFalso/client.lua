ESX = nil

local store = { 
    [_U('open_store')] = { title="Pharmacy", size= 50, colour=34, id=0, x = 952.72, y = 4495.69, z = 70.73-0.98}, 
} 

local NPC = {
    {seller = true, model = "s_m_y_blackops_01", x = 952.72, y = 4495.69, z = 70.73-0.98, h = 170.69},
}
  


Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
	
	if Config.EnableNpc then
			for _, v in pairs(NPC) do
				RequestModel(GetHashKey(v.model))
				while not HasModelLoaded(GetHashKey(v.model)) do
					Wait(1)
				end
				local npc = CreatePed(4, v.model, v.x, v.y, v.z, v.h,  false, true)
				SetPedFleeAttributes(npc, 0, 0)
				SetPedDropsWeaponsWhenDead(npc, false)
				SetPedDiesWhenInjured(npc, false)
				SetEntityInvincible(npc , true)
				FreezeEntityPosition(npc, true)
				SetBlockingOfNonTemporaryEvents(npc, true)
				if v.seller then 
					RequestAnimDict("missfbi_s4mop")
					while not HasAnimDictLoaded("missfbi_s4mop") do
						Wait(1)
					end
					TaskPlayAnim(npc, "missfbi_s4mop" ,"guard_idle_a" ,8.0, 1, -1, 49, 0, false, false, false)
				else
					GiveWeaponToPed(npc, GetHashKey("WEAPON_ADVANCEDRIFLE"), 2800, true, true)
				end
			end
	end
	
	local bool = false
  
  	if bool then
  
         
  
         for _, info in pairs(store) do
        
			info.blip = AddBlipForCoord(info.x, info.y, info.z)
            SetBlipSprite(info.blip, info.id)
            SetBlipDisplay(info.blip, 4)
            SetBlipColour(info.blip, info.colour)
            SetBlipAsShortRange(info.blip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString(info.title)
            EndTextCommandSetBlipName(info.blip)
			
         end
	   
	   bool = false
   
   end
	
	
end)

Citizen.CreateThread(function()
	while true do
      
	Citizen.Wait(5)
	
		local coords = GetEntityCoords(GetPlayerPed(-1))
		for k, v in pairs(store) do
			if #(coords - vector3(v.x, v.y, v.z)) < 100 then
				DrawMarker(27, v.x, v.y, v.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 2.0, 2.0, 2.0, 255, 0, 0, 100, false, true, 2, false, false, false, false)
			end		
		end

	end
	
end)


Citizen.CreateThread(function()
	while true do
      
	Citizen.Wait(5)
	
		local coords = GetEntityCoords(GetPlayerPed(-1))
		for k, v in pairs(store) do
			if #(coords - vector3(v.x, v.y, v.z)) < 3.5 then
				DrawText3D(v.x, v.y, v.z+0.9, k, 5.80)
				if IsControlPressed(0, 38) then
					OpenPharmacyMenu()
				end
			end
		end
		
		
	end
end)

--[[function OpenPharmacyMenu()
	ESX.UI.Menu.CloseAll()


     ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'Pharmacy_markets',
         {
            title    = _U('Pharmacy_market'),
            elements = {
				{label = ('<span style="color:white;"> Medikit - <span style="color:yellow;">350€</span>'), item = 'medikit', price = 350, amount = 1},
				{label = ('<span style="color:white;"> Ligaduras - <span style="color:yellow;">100€</span>'), item = 'bandage', price = 100, amount = 1},
				--{label = ('Syrup -  <span style="color:green;">350$</span>'), item = 'sciroppo', price = 350, amount = 1},
				--{label = ('Antibiotic -  <span style="color:green;">350$</span>'), item = 'antibiotico', price = 350, amount = 1},
            }
        },
        function(data, menu)
			local item = data.current.item
			local price = data.current.price
			local amount = data.current.amount
            TriggerServerEvent('esx:Farmacia', item, price, amount)
        end,
    function(data, menu)
        menu.close()
	end)
end]]--


function OpenPharmacyMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'pharmacy', {
		title    = 'Tiagovski',
		align = 'right',
		elements = {
			{label = 'BI Falso - 750.000€', value = 'bifalso'}
			--{label = _U('pharmacy_take', _U('bandage')), value = 'bandage'}
		}
	}, function(data, menu)
		TriggerServerEvent('esx_vendedorBiFalsoz:giveItem', data.current.value)
	end, function(data, menu)
		menu.close()
	end)
end

function DrawText3D(x, y, z, text, scale)
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local pX, pY, pZ = table.unpack(GetGameplayCamCoords())
 
    SetTextScale(scale, 0.4)
    SetTextFont(4)
    SetTextProportional(1)
    SetTextEntry("STRING")
    SetTextCentre(1)
    SetTextColour(255, 255, 255, 215)
 
    AddTextComponentString(text)
    DrawText(_x, _y)
 
    local factor = (string.len(text)) / 230
 
end