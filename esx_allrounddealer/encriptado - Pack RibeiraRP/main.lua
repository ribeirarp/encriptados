Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil
local menuOpen = false
local wasOpen = false
local saveData = {}
local pedCache = {
    [0] = nil,
    [1] = nil
}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

_RequestModel = function(hash)
    if type(hash) == "string" then hash = GetHashKey(hash) end
    RequestModel(hash)
    while not HasModelLoaded(hash) do
        Wait(0)
    end
end

Citizen.CreateThread(function()
	while true do
        print(pedCache[0], pedCache[1])
        for x,ped in pairs(ConfigAll.NPCLocations) do
            _RequestModel(GetHashKey(ped.model))
            if not DoesEntityExist(pedCache[x]) then
                pedCache[x] = CreatePed(4, GetHashKey(ped.model), ped.coords.x, ped.coords.y, ped.coords.z, ped.heading)
                SetEntityAsMissionEntity(pedCache[x])
                SetBlockingOfNonTemporaryEvents(pedCache[x], true)
                FreezeEntityPosition(pedCache[x], true)
                SetEntityInvincible(pedCache[x], true)
            end
            SetModelAsNoLongerNeeded(GetHashKey(ped.model))
        end

        Citizen.Wait(500)
        break
    end
end)


local isShowUIShowing = {}

Citizen.CreateThread(function()
	for x,y in pairs(ConfigAll.NPCLocations) do
        isShowUIShowing[x] = false
    end
end)

Citizen.CreateThread(function()
	while true do
        local sleep = 500
        local playerPed = GetPlayerPed(-1)
        local playerCoords = GetEntityCoords(playerPed)
        for x,place in pairs(ConfigAll.NPCLocations) do
            if #(place.coords-playerCoords) < 2 then
                sleep = 1
                if not isShowUIShowing[x] then
                    ESX.ShowHelpNotification("Falar com o " .. place.name)
                    isShowUIShowing[x] = true          
                end
                if IsControlJustReleased(0, Keys['E']) and not IsPedInVehicle(playerPed, GetVehiclePedIsIn(playerPed, false), false) then
                    Vender()
                end
            else
                if isShowUIShowing[x] then
                    isShowUIShowing[x] = false
                    TriggerEvent('luke_textui:HideUI')
                end
            end
        end
        Citizen.Wait(sleep)
    end
end)

function Vender()
    TriggerServerEvent("esx_allrounddealerz:vender")
end