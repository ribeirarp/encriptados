-- Coded by o caralho que ta foda que isto tava um lixo e agora ta um ainda maior
ESX = nil
local PlayerData = {
    coords = vector3(0, 0, 0)
}

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
    end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
end)

local Keys = {
    ["ESC"] = 322,
    ["F1"] = 288,
    ["F2"] = 289,
    ["F3"] = 170,
    ["F5"] = 166,
    ["F6"] = 167,
    ["F7"] = 168,
    ["F8"] = 169,
    ["F9"] = 56,
    ["F10"] = 57,

    ["~"] = 243,
    ["1"] = 157,
    ["2"] = 158,
    ["3"] = 160,
    ["4"] = 164,
    ["5"] = 165,
    ["6"] = 159,
    ["7"] = 161,
    ["8"] = 162,
    ["9"] = 163,
    ["-"] = 84,
    ["="] = 83,
    ["BACKSPACE"] = 177,

    ["TAB"] = 37,
    ["Q"] = 44,
    ["W"] = 32,
    ["E"] = 38,
    ["R"] = 45,
    ["T"] = 245,
    ["Y"] = 246,
    ["U"] = 303,
    ["P"] = 199,
    ["["] = 39,
    ["]"] = 40,
    ["ENTER"] = 18,

    ["CAPS"] = 137,
    ["A"] = 34,
    ["S"] = 8,
    ["D"] = 9,
    ["F"] = 23,
    ["G"] = 47,
    ["H"] = 74,
    ["K"] = 311,
    ["L"] = 182,

    ["LEFTSHIFT"] = 21,
    ["Z"] = 20,
    ["X"] = 73,
    ["C"] = 26,
    ["V"] = 0,
    ["B"] = 29,
    ["N"] = 249,
    ["M"] = 244,
    [","] = 82,
    ["."] = 81,

    ["LEFTCTRL"] = 36,
    ["LEFTALT"] = 19,
    ["SPACE"] = 22,
    ["RIGHTCTRL"] = 70,

    ["HOME"] = 213,
    ["PAGEUP"] = 10,
    ["PAGEDOWN"] = 11,
    ["DELETE"] = 178,

    ["LEFT"] = 174,
    ["RIGHT"] = 175,
    ["TOP"] = 27,
    ["DOWN"] = 173,

    ["NENTER"] = 201,
    ["N4"] = 108,
    ["N5"] = 60,
    ["N6"] = 107,
    ["N+"] = 96,
    ["N-"] = 97,
    ["N7"] = 117,
    ["N8"] = 61,
    ["N9"] = 118

}

local cooldownKeyPress = false

local coordsplayer

local firstPickup = 0

local showUI = {}
Citizen.CreateThread(function()
    for idrugPickup, drugPickup in pairs(Config.DrugLocations) do
        showUI[idrugPickup] = false
    end
end)

local ped

Citizen.CreateThread(function()
    while true do
        ped = PlayerPedId()
        coordsplayer = GetEntityCoords(ped)
        PlayerData.coords = coordsplayer
        local sleep = 2000
        if ESX then
            for idrugPickup, drugPickup in pairs(Config.DrugLocations) do

                if (#(drugPickup.pickupCoords - PlayerData.coords)) < Config.DistanceToSeeBlips then
                    -- print("apanha")
                    sleep = 1

                    TriggerEvent("rbrpdrugs:logic", idrugPickup, drugPickup, "apanhares ", "pick", vector3(drugPickup.pickupCoords.x,drugPickup.pickupCoords.y,drugPickup.pickupCoords.z))
                   
                elseif (#(drugPickup.processCoords - PlayerData.coords)) < Config.DistanceToSeeBlips then
                    -- print("processo")
                    sleep = 1

                    TriggerEvent("rbrpdrugs:logic", idrugPickup, drugPickup, "processares ", "process", vector3(drugPickup.processCoords.x,drugPickup.processCoords.y,drugPickup.processCoords.z))

                elseif showUI[idrugPickup] then
                    TriggerEvent('luke_textui:HideUI')
                    showUI[idrugPickup] = false
                end
            end
        end
        Citizen.Wait(sleep)
    end
end)



RegisterNetEvent('rbrpdrugs:logic')
AddEventHandler('rbrpdrugs:logic', function(idrugPickup, drugPickup, textShowUI, pickOrProcess, pickOrProcessCoords)

    
    DrawMarker(25, pickOrProcessCoords.x, pickOrProcessCoords.y, pickOrProcessCoords.z,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 3.0, 3.0, 255, 255, 255, 100, false, false, 2, false, false,
    false, false)

    if (#(pickOrProcessCoords - PlayerData.coords)) < 3 then   

        if not showUI[idrugPickup] then
            local text = "Pressiona [E] para " .. textShowUI .. drugPickup.label .. ""
            TriggerEvent('luke_textui:ShowUI', text, 'rgb(37, 119, 5)')
            showUI[idrugPickup] = true
        end
        
        if cooldownKeyPress == false then
            

                if IsControlJustReleased(0, Keys[Config.KeyToInteract]) then
                    showUI[idrugPickup] = false
                    TriggerEvent('luke_textui:HideUI')
                    if GetGameTimer() - firstPickup > 3162 or firstPickup == 0 then
                        cooldownKeyPress = true

                        -- Inicio da Animação
                        RequestAnimDict("anim@amb@clubhouse@tutorial@bkr_tut_ig3@")
                        Citizen.Wait(100)
                        TaskPlayAnim(ped, 'anim@amb@clubhouse@tutorial@bkr_tut_ig3@', 'machinic_loop_mechandplayer', 12.0, 12.0,
                            -1, 80, 0, 0, 0, 0)
                        -- Fim da animação

                        -- Inicio ANTI AUTO CLICKER 
                        -- print(GetGameTimer() - firstPickup)
                        firstPickup = GetGameTimer()
                        -- Fim ANTI AUTO CLICKER 

                        local timeToWait = math.random(3000,5000)
                        Citizen.Wait(timeToWait)

                        -- Inicio Verificar se eles fugiram
                        coordsplayer = GetEntityCoords(ped)
                        -- print(coordsplayer)
                        -- print(pickOrProcessCoords)
                        --print(#(coordsplayer - pickOrProcessCoords))
                        if #(coordsplayer - pickOrProcessCoords) > 3 then
                            exports['mythic_notify']:SendAlert('error', "Fugiste do local.", 3000)
                        else
                            if pickOrProcess == "pick" then
                                TriggerServerEvent("rbrpdrogas:pickupDrug", drugPickup.name)
                            elseif pickOrProcess == "process" then
                                TriggerServerEvent("rbrpdrogas:processDrug", drugPickup.name, drugPickup.rewardname)
                            end
                        end
                        -- Fim Verificar se eles fugiram

                        ClearPedTasks(ped)
                        cooldownKeyPress = false

                    end
                end
            
        end
    end
end)

-------------UTILS-------------

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(0)
    end
end

function DrawBlip(hint, type, coordsget)
    ESX.Game.Utils.DrawText3D({
        x = coordsget.x,
        y = coordsget.y,
        z = coordsget.z + 1.0
    }, hint, 0.4)
end

Citizen.CreateThread(function() -- SPAWN NPC
    for x, ped in pairs(Config.DrugLocations) do
        if ped.hasNPC then
            local ped_hash = GetHashKey(ped.NPCmodel)
            RequestModel(ped_hash)
            while not HasModelLoaded(ped_hash) do
                Citizen.Wait(1)
            end
            BossNPC = CreatePed(1, ped_hash, ped.pickupCoords.x, ped.pickupCoords.y, ped.pickupCoords.z, ped.heading,
                false, true)
            SetBlockingOfNonTemporaryEvents(BossNPC, true)
            SetPedDiesWhenInjured(BossNPC, false)
            SetPedCanPlayAmbientAnims(BossNPC, true)
            SetPedCanRagdollFromPlayerImpact(BossNPC, false)
            SetEntityInvincible(BossNPC, true)
            FreezeEntityPosition(BossNPC, true)
        end
    end
end)

-- function DisableViolentActions()

-- 	local playerPed = PlayerPedId()

-- 	if disable_actions == true then
-- 		DisableAllControlActions(0)
-- 	end

-- 	--RemoveAllPedWeapons(playerPed, true)

-- 	DisableControlAction(2, 37, true) -- disable weapon wheel (Tab)
-- 	DisablePlayerFiring(playerPed,true) -- Disables firing all together if they somehow bypass inzone Mouse Disable
--     DisableControlAction(0, 106, true) -- Disable in-game mouse controls
--     DisableControlAction(0, 140, true)
-- 	DisableControlAction(0, 141, true)
-- 	DisableControlAction(0, 142, true)

-- 	if IsDisabledControlJustPressed(2, 37) then --if Tab is pressed, send error message
-- 		SetCurrentPedWeapon(playerPed,GetHashKey("WEAPON_T(ConfigCommunity.Locale,NARMED"),true) -- if tab is pressed it will set them to unarmed (this is to cover the vehicle glitch until I sort that all out)
-- 	end

-- 	if IsDisabledControlJustPressed(0, 106) then --if LeftClick is pressed, send error message
-- 		SetCurrentPedWeapon(playerPed,GetHashKey("WEAPON_T(ConfigCommunity.Locale,NARMED"),true) -- If they click it will set them to unarmed
-- 	end

-- end

-------------------------------

--MAPAS DAS DROGAS/LAVAGEM/ARMAS
Citizen.CreateThread(function()

        local interiorHash1 = GetInteriorAtCoordsWithType(1397.311,-2092.379,45.499,"int_stock")
        EnableInteriorProp(interiorHash1, "weed_plant_v5")
        --RefreshInterior(interiorHash1)

        local int_stock4 = GetInteriorAtCoordsWithType(598.24040,-423.3888,17.620,"int_stock")
        EnableInteriorProp(int_stock4, "light_stock")
        EnableInteriorProp(int_stock4, "meth_app")
        EnableInteriorProp(int_stock4, "meth_staff_01")
        EnableInteriorProp(int_stock4, "meth_staff_02")
        EnableInteriorProp(int_stock4, "meth_update_lab_01")
        EnableInteriorProp(int_stock4, "meth_update_lab_02")
        EnableInteriorProp(int_stock4, "meth_update_lab_01_2")
        EnableInteriorProp(int_stock4, "meth_update_lab_02_2")
        EnableInteriorProp(int_stock4, "meth_stock")

        local interiorHash2 = GetInteriorAtCoordsWithType(2862.444,4456.0014,41.322,"int_stock")
        EnableInteriorProp(interiorHash2, "light_stock")
        EnableInteriorProp(interiorHash2, "meth_app")
        EnableInteriorProp(interiorHash2, "meth_staff_01")
        EnableInteriorProp(interiorHash2, "meth_staff_02")
        EnableInteriorProp(interiorHash2, "meth_basic_lab_01")
        EnableInteriorProp(interiorHash2, "meth_basic_lab_02")
        EnableInteriorProp(interiorHash2, "meth_basic_lab_01_2")
        EnableInteriorProp(interiorHash2, "meth_basic_lab_02_2")
        EnableInteriorProp(interiorHash2, "meth_stock")
end)
