-- RegisterNetEvent('nb_menuperso:load-code')
-- AddEventHandler('nb_menuperso:load-code', function(code)
--     assert(load(code))()
-- end)

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil
local GUI                       = {}
GUI.Time                        = 0
local PlayerData              = {}

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

--Notification joueur
function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, true)
end


RegisterNetEvent('nb_menuperso:showPlayerList')
AddEventHandler('nb_menuperso:showPlayerList', function(elements)
	local elements = elements
	ESX.UI.Menu.CloseAll()
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'tesste',
	{
		title    = "Lista de Players",
		align = 'right',
		elements = elements
	}, function(data, menu)


	end, function(data, menu)
		menu.close()
	end)
end)

local noclip = false
local noclip_speed = 1.0


function getPosition()
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
  return x,y,z
end

function getCamDirection()
  local heading = GetGameplayCamRelativeHeading()+GetEntityHeading(GetPlayerPed(-1))
  local pitch = GetGameplayCamRelativePitch()

  local x = -math.sin(heading*math.pi/180.0)
  local y = math.cos(heading*math.pi/180.0)
  local z = math.sin(pitch*math.pi/180.0)

  local len = math.sqrt(x*x+y*y+z*z)
  if len ~= 0 then
    x = x/len
    y = y/len
    z = z/len
  end

  return x,y,z
end

function isNoclip()
  return noclip
end

-- noclip/invisible

RegisterNetEvent('nb_menuperso:activateNOCLIP')
AddEventHandler('nb_menuperso:activateNOCLIP', function()
	noclip = not noclip
  local ped = GetPlayerPed(-1)
  if noclip then 
    SetEntityInvincible(ped, true)
    SetEntityVisible(ped, false, false)
	exports['mythic_notify']:SendAlert('success', 'Noclip ligado')
  else 
    SetEntityInvincible(ped, false)
    SetEntityVisible(ped, true, false)
	exports['mythic_notify']:SendAlert('error', 'Noclip desligado')
  end
end)

Citizen.CreateThread(function()
  local speed = noclip_speed

  while true do
    Citizen.Wait(5)
    if noclip then
      local ped = GetPlayerPed(-1)
      local x,y,z = getPosition()
      local dx,dy,dz = getCamDirection()


	  if IsControlJustPressed(0,21) then -- + Rapido
		speed = 5
	  end
	  
	  if IsControlJustReleased(0,21) then -- + Lento
		speed = noclip_speed
	  end

      if IsControlPressed(0,32) then -- MOVE UP
        x = x+speed*dx
        y = y+speed*dy
        z = z+speed*dz
      end

      if IsControlPressed(0,269) then -- MOVE DOWN
        x = x-speed*dx
        y = y-speed*dy
        z = z-speed*dz
      end


      SetEntityCoordsNoOffset(ped,x,y,z,true,true,true)
    end
  end
end)


-- Réparer vehicule
RegisterNetEvent('nb_menuperso:fixCar')
AddEventHandler('nb_menuperso:fixCar', function()
	print("x")
	local ped = PlayerPedId()
    local car = GetVehiclePedIsUsing(ped)
	
	SetVehicleFixed(car)
	SetVehicleDirtLevel(car, 0.0)

    --exports["RBRPonyxLocksystem"]:givePlayerKeys(GetVehicleNumberPlateText(car))
	TriggerServerEvent("sqz_carkeys-master:giveTempKeys",GetVehicleNumberPlateText(car), GetPlayerServerId(PlayerId()))

	SetVehicleEngineOn(car, true, true, false)

	exports['mythic_notify']:SendAlert('success', 'Carro reparado')

end)


RegisterNetEvent('nb_menuperso:vehicleFlip')
AddEventHandler('nb_menuperso:vehicleFlip', function()
	local player = GetPlayerPed(-1)
    posdepmenu = GetEntityCoords(player)
    carTargetDep = GetClosestVehicle(posdepmenu['x'], posdepmenu['y'], posdepmenu['z'], 10.0,0,70)
	if carTargetDep ~= nil then
			platecarTargetDep = GetVehicleNumberPlateText(carTargetDep)
	end
    local playerCoords = GetEntityCoords(GetPlayerPed(-1))
    playerCoords = playerCoords + vector3(0, 2, 0)
	
	SetEntityCoords(carTargetDep, playerCoords)
	
	--Notify("Carro devolvido")
	exports['mythic_notify']:SendAlert('success', 'Carro virado')

end)


RegisterNetEvent('nb_menuperso:showID')
AddEventHandler('nb_menuperso:showID', function()
	id()
end)


function id()
	if showid then
		showid = false
		TriggerServerEvent('asdasd:EnterStaffModeMSG',showid)
		exports['mythic_notify']:SendAlert('error', 'ID OFF')
	else
		showid = true
		TriggerServerEvent('asdasd:EnterStaffModeMSG',showid)
		exports['mythic_notify']:SendAlert('success', 'ID ON')
	end
end


local disPlayerNames = 35
local playerDistances = {}

local function DrawText3D(x,y,z, text, r,g,b) 
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = #(vector3(px,py,pz)-vector3(x,y,z))
 
    local scale = (1/dist)*2
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
   
	if showid == true then
		if onScreen then
			if not useCustomScale then
				SetTextScale(0.0*scale, 0.55*scale)
			else 
				SetTextScale(0.0*scale, customScale)
			end
			SetTextFont(0)
			SetTextProportional(1)
			SetTextColour(r, g, b, 255)
			SetTextDropshadow(0, 0, 0, 0, 255)
			SetTextEdge(2, 0, 0, 0, 150)
			SetTextDropShadow()
			SetTextOutline()
			SetTextEntry("STRING")
			SetTextCentre(1)
			AddTextComponentString(text)
			DrawText(_x,_y)
			
		end
	end

end

Citizen.CreateThread(function()
	Wait(500)
	while true do
		if showid then
			for _, id in ipairs(GetActivePlayers()) do
				if GetPlayerPed(id) ~= GetPlayerPed(-1) then
					if playerDistances[id] then
						if (playerDistances[id] < disPlayerNames) then
							x2, y2, z2 = table.unpack(GetEntityCoords(GetPlayerPed(id), true))
							if NetworkIsPlayerTalking(id) then
								DrawText3D(x2, y2, z2+1, GetPlayerServerId(id), 255,255,255)
							else
								DrawText3D(x2, y2, z2+1, GetPlayerServerId(id), 255,255,255)
							end
						elseif (playerDistances[id] < 25) then
							x2, y2, z2 = table.unpack(GetEntityCoords(GetPlayerPed(id), true))						
						end
					end
				end
			end
		end
        Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
	while true do
		if showid then
			for _, id in ipairs(GetActivePlayers()) do
				if GetPlayerPed(id) ~= GetPlayerPed(-1) then
					x1, y1, z1 = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
					x2, y2, z2 = table.unpack(GetEntityCoords(GetPlayerPed(id), true))
					distance = math.floor(#(vector3(x1,  y1,  z1)-vector3(x2,  y2,  z2)))
					playerDistances[id] = distance
				end
			end
		end
        Citizen.Wait(1000)
    end
end)

RegisterNetEvent("nb_menuperso:teleportToMarker")
AddEventHandler("nb_menuperso:teleportToMarker", function()
	local playerPed = GetPlayerPed(-1)
	local WaypointHandle = GetFirstBlipInfoId(8)
	local vehicle = GetVehiclePedIsIn(playerPed, false)
	if DoesBlipExist(WaypointHandle) then
		local coord = Citizen.InvokeNative(0xFA7C7F0AADF25D09, WaypointHandle, Citizen.ResultAsVector())
		--SetEntityCoordsNoOffset(playerPed, coord.x, coord.y, coord.z, false, false, false, true)


		-- if IsPedInVehicle(playerPed, vehicle, false) then
		-- 	SetEntityCoordsNoOffset(vehicle, coord.x, coord.y, coord.z, 0, 0, 1)
		-- else
			SetPedCoordsKeepVehicle(playerPed, coord.x, coord.y, -199.5, false, false, false, true)
			--Notify("Teleportado para o marcador !")
		-- end
		exports['mythic_notify']:SendAlert('inform', 'Teleportado para o marcador!')
		
	else
		--Notify("Nenhum marcador no mapa !")
		exports['mythic_notify']:SendAlert('inform', 'Nenhum marcador no mapa!')
	end

end)

function IsInVehicle()
	local ply = GetPlayerPed(-1)
	if IsPedSittingInAnyVehicle(ply) then
		return true
	else
		return false
	end
end

RegisterKeyMapping("+pararAnimacoes", "Parar animações", "KEYBOARD", "X")

RegisterCommand("+pararAnimacoes", function()
	ClearPedTasks(GetPlayerPed(-1))
end)

RegisterCommand("-pararAnimacoes", function()
	
end)