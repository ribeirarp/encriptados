ESX                     = nil
local CurrentAction     = nil
local CurrentActionMsg  = nil
local CurrentActionData = nil
local Licenses          = {}
local CurrentTest       = nil
local CurrentTestType   = nil
local CurrentVehicle    = nil
local CurrentCheckPoint, DriveErrors = 0, 0
local LastCheckPoint    = -1
local CurrentBlip       = nil
local CurrentZoneType   = nil
local IsAboveSpeedLimit = false
local LastVehicleHealth = nil
local bossSpawned = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

function DrawMissionText(msg, time)
	ClearPrints()
	BeginTextCommandPrint('STRING')
	AddTextComponentSubstringPlayerName(msg)
	EndTextCommandPrint(time, true)
end

function StartTheoryTest()
	local hasMoney = false
	ESX.TriggerServerCallback('esx_escolaDeConducaoSexy:paycallback', function(value)
		hasMoney = value
		if hasMoney == true then
			CurrentTest = 'theory'

			SendNUIMessage({
				openQuestion = true
			})

			ESX.SetTimeout(200, function()
				SetNuiFocus(true, true)
			end)
		end

	end, Config.Prices['dmv'])

	--TriggerServerEvent('esx_escolaDeConducaoSexy:pay', Config.Prices['dmv'])

end

function StopTheoryTest(success)
	CurrentTest = nil

	SendNUIMessage({
		openQuestion = false
	})

	SetNuiFocus(false)

	if success then
		TriggerServerEvent('esx_escolaDeConducaoSexy:addLicense', 'dmv')
		exports['mythic_notify']:SendAlert('inform', _U('passed_test'), 3000)
	else
		exports['mythic_notify']:SendAlert('error', _U('failed_test'), 3000)
	end
end

function StartDriveTest(type)
	ESX.UI.Menu.CloseAll()
	local hasMoney = false
	ESX.TriggerServerCallback('esx_escolaDeConducaoSexy:paycallback', function(value)
		hasMoney = value
		if hasMoney == true then
			ESX.Game.SpawnVehicle(Config.VehicleModels[type], Config.Zones.VehicleSpawnPoint.Pos, Config.Zones.VehicleSpawnPoint.Pos.h, function(vehicle)
				CurrentTest       = 'drive'
				CurrentTestType   = type
				CurrentCheckPoint = 0
				LastCheckPoint    = -1
				CurrentZoneType   = 'residence'
				DriveErrors       = 0
				IsAboveSpeedLimit = false
				CurrentVehicle    = vehicle
				LastVehicleHealth = GetEntityHealth(vehicle)
				local carplate = GetVehicleNumberPlateText(vehicle)
				exports["RBRPonyxLocksystem"]:givePlayerKeys(carplate)
				local playerPed   = PlayerPedId()
				while not DoesEntityExist(vehicle) do
					Citizen.Wait(100)
				end
				TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
				SetVehicleEngineOn(vehicle, true, true, true)
			end)
		end

	end, Config.Prices[type])



	--TriggerServerEvent('esx_escolaDeConducaoSexy:pay', Config.Prices[type])
end

function StopDriveTest(success)
	if success then
		TriggerServerEvent('esx_escolaDeConducaoSexy:addLicense', CurrentTestType)
		exports['mythic_notify']:SendAlert('inform', _U('passed_test'), 3000)
	else
		exports['mythic_notify']:SendAlert('error', _U('failed_test'), 3000)
	end

	CurrentTest     = nil
	CurrentTestType = nil
end

function SetCurrentZoneType(type)
CurrentZoneType = type
end

RegisterNetEvent('dmvschool:OpenDMVSchoolMenu')
AddEventHandler('dmvschool:OpenDMVSchoolMenu', function(data)
    OpenDMVSchoolMenu()
end)

function OpenDMVSchoolMenu()
	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

	local elements = {}

	if not ownedLicenses['dmv'] then
		table.insert(elements, {
			label = (('%s: <span style="color:green;">%s</span>'):format(_U('theory_test'), _U('school_item', ESX.Math.GroupDigits(Config.Prices['dmv'])))),
			value = 'theory_test'
		})
	end

	if ownedLicenses['dmv'] then
		if not ownedLicenses['drive'] then
			table.insert(elements, {
				label = (('%s: <span style="color:green;">%s</span>'):format(_U('road_test_car'), _U('school_item', ESX.Math.GroupDigits(Config.Prices['drive'])))),
				value = 'drive_test',
				type = 'drive'
			})
		end

		if not ownedLicenses['drive_bike'] then
			table.insert(elements, {
				label = (('%s: <span style="color:green;">%s</span>'):format(_U('road_test_bike'), _U('school_item', ESX.Math.GroupDigits(Config.Prices['drive_bike'])))),
				value = 'drive_test',
				type = 'drive_bike'
			})
		end

		if not ownedLicenses['drive_truck'] then
			table.insert(elements, {
				label = (('%s: <span style="color:green;">%s</span>'):format(_U('road_test_truck'), _U('school_item', ESX.Math.GroupDigits(Config.Prices['drive_truck'])))),
				value = 'drive_test',
				type = 'drive_truck'
			})
		end
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'dmvschool_actions', {
		title    = _U('driving_school'),
		elements = elements,
		align    = 'right'
	}, function(data, menu)
		if data.current.value == 'theory_test' then
			menu.close()
			StartTheoryTest()
		elseif data.current.value == 'drive_test' then
			StartDriveTest(data.current.type)
		end
	end, function(data, menu)
		menu.close()

		CurrentAction     = 'dmvschool_menu'
		CurrentActionMsg  = _U('press_open_menu')
		CurrentActionData = {}
	end)
end

RegisterNUICallback('question', function(data, cb)
	SendNUIMessage({
		openSection = 'question'
	})

	cb()
end)

RegisterNUICallback('close', function(data, cb)
	StopTheoryTest(true)
	cb()
end)

RegisterNUICallback('kick', function(data, cb)
	StopTheoryTest(false)
	cb()
end)

AddEventHandler('esx_dmvschool:hasEnteredMarker', function(zone)
	if zone == 'DMVSchool' then
		CurrentAction     = 'dmvschool_menu'
		CurrentActionMsg  = _U('press_open_menu')
		CurrentActionData = {}
	end
end)

AddEventHandler('esx_dmvschool:hasExitedMarker', function(zone)
	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
end)

RegisterNetEvent('esx_dmvschool:loadLicenses')
AddEventHandler('esx_dmvschool:loadLicenses', function(licenses)
	Licenses = licenses
end)

-- Create Blips
Citizen.CreateThread(function()
	local blip = AddBlipForCoord(Config.Zones.DMVSchool.Pos.x, Config.Zones.DMVSchool.Pos.y, Config.Zones.DMVSchool.Pos.z)

	SetBlipSprite (blip, 408)
	SetBlipDisplay(blip, 4)
	SetBlipScale  (blip, 1.2)
	SetBlipAsShortRange(blip, true)

	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString(_U('driving_school_blip'))
	EndTextCommandSetBlipName(blip)
end)

-- Display markers
-- Citizen.CreateThread(function()
-- 	while true do
-- 		Citizen.Wait(0)

-- 		local coords = GetEntityCoords(PlayerPedId())

-- 		for k,v in pairs(Config.Zones) do
-- 			if(v.Type ~= -1 and #(coords - vector3(v.Pos.x, v.Pos.y, v.Pos.z)) < Config.DrawDistance) then
-- 				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
-- 			end
-- 		end
-- 	end
-- end)

-- Enter / Exit marker events
-- Citizen.CreateThread(function()
-- 	while true do

-- 		Citizen.Wait(100)

-- 		local coords      = GetEntityCoords(PlayerPedId())
-- 		local isInMarker  = false
-- 		local currentZone = nil

-- 		for k,v in pairs(Config.Zones) do
-- 			if(#(coords - vector3(v.Pos.x, v.Pos.y, v.Pos.z)) < v.Size.x) then
-- 				isInMarker  = true
-- 				currentZone = k
-- 			end
-- 		end

-- 		if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
-- 			HasAlreadyEnteredMarker = true
-- 			LastZone                = currentZone
-- 			TriggerEvent('esx_dmvschool:hasEnteredMarker', currentZone)
-- 		end

-- 		if not isInMarker and HasAlreadyEnteredMarker then
-- 			HasAlreadyEnteredMarker = false
-- 			TriggerEvent('esx_dmvschool:hasExitedMarker', LastZone)
-- 		end
-- 	end
-- end)

-- Block UI
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)

		if CurrentTest == 'theory' then
			local playerPed = PlayerPedId()

			DisableControlAction(0, 1, true) -- LookLeftRight
			DisableControlAction(0, 2, true) -- LookUpDown
			DisablePlayerFiring(playerPed, true) -- Disable weapon firing
			DisableControlAction(0, 142, true) -- MeleeAttackAlternate
			DisableControlAction(0, 106, true) -- VehicleMouseControlOverride
		else
			Citizen.Wait(500)
		end
	end
end)

-- Key Controls
-- Citizen.CreateThread(function()
-- 	while true do
-- 		Citizen.Wait(0)

-- 		if CurrentAction then
-- 			ESX.ShowHelpNotification(CurrentActionMsg)

-- 			if IsControlJustReleased(0, 38) then
-- 				if CurrentAction == 'dmvschool_menu' then
-- 					OpenDMVSchoolMenu()
-- 				end

-- 				CurrentAction = nil
-- 			end
-- 		else
-- 			Citizen.Wait(500)
-- 		end
-- 	end
-- end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(1000)
        local pedCoords = GetEntityCoords(GetPlayerPed(-1))
        local bossCoords =  vector3(241.04, -1379.22, 33.72-0.98)
        local dst = #(bossCoords - pedCoords)

        if dst < 200 and bossSpawned == false then
            TriggerEvent('esx_dmvschool:spawnBoss', bossCoords, 144.57)
            bossSpawned = true
        end
        if dst >= 201 then
            bossSpawned = false
            DeletePed(npc)
        end
    end
end)


RegisterNetEvent('esx_dmvschool:spawnBoss')
AddEventHandler('esx_dmvschool:spawnBoss', function(coords, heading)
    local hash = GetHashKey('mp_m_boatstaff_01')
    if not HasModelLoaded(hash) then
        RequestModel(hash)
        Wait(10)
    end
    while not HasModelLoaded(hash) do
        Wait(10)
    end

    bossSpawned = true
    npc = CreatePed(5, hash, coords, heading, false, false)
    FreezeEntityPosition(npc, true)
    SetEntityInvincible(npc, true)
    SetBlockingOfNonTemporaryEvents(npc, true)
    while not TaskStartScenarioInPlace(npc, "WORLD_HUMAN_CLIPBOARD_FACILITY", 0, false) do
        Wait(200)
    end
end)

Citizen.CreateThread(function()
    exports['bt-target']:AddBoxZone("CartaConducao", vector3(240.90, -1379.43, 34.08), 0.8, 0.4, {
        name = "CartaConducao",
        heading = 125.0,
        debugPoly = false,
        minZ = 25.000,
        maxZ = 36.00
    }, {
        options = {{
            event = "dmvschool:OpenDMVSchoolMenu",
            icon = "fas fa-clipboard",
            label = "Carta condução"
        }},
        job = {"all"},
        distance = 5.0
    })
end)

-- Drive test
Citizen.CreateThread(function()
    local trava = 0
	while true do
		if CurrentTest == 'drive' then
            trava = 0
			local playerPed      = PlayerPedId()
			local coords         = GetEntityCoords(playerPed)
			local nextCheckPoint = CurrentCheckPoint + 1

			if Config.CheckPoints[nextCheckPoint] == nil then
				if DoesBlipExist(CurrentBlip) then
					RemoveBlip(CurrentBlip)
				end

				CurrentTest = nil

				exports['mythic_notify']:SendAlert('inform', _U('driving_test_complete'), 3000)

				if DriveErrors < Config.MaxErrors then
					StopDriveTest(true)
				else
					StopDriveTest(false)
				end
			else

				if CurrentCheckPoint ~= LastCheckPoint then
					if DoesBlipExist(CurrentBlip) then
						RemoveBlip(CurrentBlip)
					end

					CurrentBlip = AddBlipForCoord(Config.CheckPoints[nextCheckPoint].Pos.x, Config.CheckPoints[nextCheckPoint].Pos.y, Config.CheckPoints[nextCheckPoint].Pos.z)
					SetBlipRoute(CurrentBlip, 1)

					LastCheckPoint = CurrentCheckPoint
				end

				local distance = #(coords - vector3(Config.CheckPoints[nextCheckPoint].Pos.x, Config.CheckPoints[nextCheckPoint].Pos.y, Config.CheckPoints[nextCheckPoint].Pos.z))

				if distance <= 100.0 then
					DrawMarker(1, Config.CheckPoints[nextCheckPoint].Pos.x, Config.CheckPoints[nextCheckPoint].Pos.y, Config.CheckPoints[nextCheckPoint].Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 1.5, 102, 204, 102, 100, false, true, 2, false, false, false, false)
				end

				if distance <= 3.0 then
					Config.CheckPoints[nextCheckPoint].Action(playerPed, CurrentVehicle, SetCurrentZoneType)
					CurrentCheckPoint = CurrentCheckPoint + 1
				end
			end
		else
			-- not currently taking driver test
			Citizen.Wait(500)
		end
        Citizen.Wait(trava)
	end
end)

-- Speed / Damage control
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)

		if CurrentTest == 'drive' then

			local playerPed = PlayerPedId()

			if IsPedInAnyVehicle(playerPed, false) then

				local vehicle      = GetVehiclePedIsIn(playerPed, false)
				local speed        = GetEntitySpeed(vehicle) * Config.SpeedMultiplier
				local tooMuchSpeed = false

				for k,v in pairs(Config.SpeedLimits) do
					if CurrentZoneType == k and speed > v then
						tooMuchSpeed = true

						if not IsAboveSpeedLimit then
							DriveErrors       = DriveErrors + 1
							IsAboveSpeedLimit = true

							exports['mythic_notify']:SendAlert('error', _U('driving_too_fast', v), 3000)
							exports['mythic_notify']:SendAlert('error', _U('errors', DriveErrors, Config.MaxErrors), 3000)
						end
					end
				end

				if not tooMuchSpeed then
					IsAboveSpeedLimit = false
				end

				local health = GetEntityHealth(vehicle)
				if health < LastVehicleHealth then

					DriveErrors = DriveErrors + 1

					exports['mythic_notify']:SendAlert('error', _U('you_damaged_veh'), 3000)
					exports['mythic_notify']:SendAlert('error', _U('errors', DriveErrors, Config.MaxErrors), 3000)


					-- avoid stacking faults
					LastVehicleHealth = health
					Citizen.Wait(1500)
				end
			end
		else
			-- not currently taking driver test
			Citizen.Wait(500)
		end
	end
end)
