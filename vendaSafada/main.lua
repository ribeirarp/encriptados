local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
local doorsTimeout = {}
local selling = false
local ESX = nil
local playingAnimation = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent("esx:getSharedObject", function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	for x=1, #Config.doors do
		table.insert(doorsTimeout, {lastUsed = 0})
	end
end)

function getItems(doorNumber, nameOfDrug)
	local nameOfDrug = Config.doors[doorNumber].nameOfDrug	
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
	{
		title    = "Venda de " .. nameOfDrug,
		align    = 'bottom-right',
		elements = {
			{label = "Droga não processada", value = "item"},
			{label = "Droga processada", value = "itemProcessado"},
		}
	}, function(data, menu)
		ESX.UI.Menu.CloseAll()

		local itemName = data.current.value
		selling = true
		
		TriggerServerEvent('testeScript:getDrugsToSell', doorNumber, itemName)
	end, function(data, menu)
		menu.close()
	end)
end

function CreateWaypoint(coords)
	SetNewWaypoint(coords.x, coords.y)
end

function cancelSelling()
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'stocks_menu',
	{
		title    = "Queres mesmo cancelar?",
		align    = 'bottom-right',
		elements = {
			{label = "Sim", value = true },
			{label = "Não", value = false},
		}
	}, function(data, menu)
		ESX.UI.Menu.CloseAll()
		local yesORno = data.current.value

		if yesORno then
			exports.ft_libs:RemoveArea("tmpSellDoor")
			ESX.ShowNotification("Entrega ~r~cancelada~w~")
			selling = false
		else
			ESX.ShowNotification("A entrega vai ~g~continuar~w~")
		end
	end, function(data, menu)
		menu.close()
	end)
end

function animation(dict, anim, coords, walk)
	plyPed = GetPlayerPed(-1)
	makingAnimation()
	if walk then
		TaskGoStraightToCoord(plyPed, coords.x, coords.y, coords.z, 10.0, 10, coords.h, 0.5)
		Wait(1000)
		ClearPedTasks(plyPed)
	end

	while not HasAnimDictLoaded(dict) do RequestAnimDict(dict); Citizen.Wait(0); end
	TaskPlayAnim( plyPed, dict, anim, 8.0, 8.0, -1, 4, 0, 0, 0, 0 )     
	Citizen.Wait(0)
	while IsEntityPlayingAnim(plyPed, dict, anim, 3) do Citizen.Wait(0); end   
	playingAnimation = false       
	Citizen.Wait(1000)
end

RegisterNetEvent('testeScript:timer')
AddEventHandler('testeScript:timer', function(door)
	doorsTimeout[door].lastUsed = Config.timeoutToUseSameDoor
	Citizen.CreateThread(function()
		while doorsTimeout[door].lastUsed > 0  do
			Citizen.Wait(1000)

			if doorsTimeout[door].lastUsed > 0 then
				doorsTimeout[door].lastUsed = doorsTimeout[door].lastUsed - 1
			end
		end
	end)
end)

function DrawText3D(x,y,z,text,modifier)
	local onScreen,_x,_y = World3dToScreen2d(x,y,z)
	local px,py,pz = table.unpack(GetGameplayCamCoord())
	local dist = #(vector3(px,py,pz) - vector3(x,y,z))
	if not modifier or modifier < 100 then modifier = 100; end
	local scale = ((1/dist)*2)*(1/GetGameplayCamFov())*modifier
  
	if onScreen then
	  -- Formalize the text
	  SetTextColour(220, 220, 220, 255)
	  SetTextScale(0.0*scale, 0.40*scale)
	  SetTextFont(4)
	  SetTextProportional(1)
	  SetTextCentre(true)
  
	  -- Calculate width and height
	  BeginTextCommandWidth("STRING")
	  --AddTextComponentString(text)
	  local height = GetTextScaleHeight(0.45*scale, 4)
	  local width = EndTextCommandGetWidth(4)
  
	  -- Diplay the text
	  SetTextEntry("STRING")
	  AddTextComponentString(text)
	  EndTextCommandDisplayText(_x, _y)
  
	end
  end

function makingAnimation()
	playingAnimation = true
	Citizen.CreateThread(function()
		while playingAnimation do
			Citizen.Wait(0)
			playerPed = PlayerPedId()
			DisableAllControlActions(0)
		end
	end)
end

RegisterNetEvent('testeScript:setBlip')
AddEventHandler('testeScript:setBlip', function(dataTable)
	CreateWaypoint(dataTable.location)
	exports.ft_libs:AddArea("tmpSellDoor", {
		marker = {
			type = 27,
			weight = 1,
			height = 1,
			red = 255,
			green = 255,
			blue = 255,
		},
		trigger = {
			weight = 1,
			active = {
				callback = function()
					DrawText3D(dataTable.location.x,dataTable.location.y,dataTable.location.z, "Pressiona [~r~E~s~] para vender a droga.")
					if IsControlJustPressed(0, Keys["E"]) and not IsPedInVehicle(PlayerPedId(), GetVehiclePedIsIn(PlayerPedId(), false), false) then
						animation("timetable@jimmy@doorknock@", "knockdoor_idle", dataTable.location, true)
						animation("mp_common", "givetake2_b", dataTable.location, false)
						selling = false
						TriggerServerEvent("testeScript:sellDrugs", dataTable)

						Citizen.Wait(100)
						exports.ft_libs:RemoveArea("tmpSellDoor")
					end
				end,
			},
		},
		locations = {
			{
				x = dataTable.location.x,
				y = dataTable.location.y,
				z = dataTable.location.z,
			},
			
		},
	})
end)


RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	for number,door in ipairs(Config.doors) do
		exports.ft_libs:AddArea("blipDoor_" .. number, {
			trigger = {
				weight = 1.7,
				exit = {
					callback = function()
						ESX.UI.Menu.CloseAll()							
					end,
				},
				active = {
					callback = function()
						DrawText3D(door.location.x,door.location.y,door.location.z, "Pressiona [~r~E~s~] para bater a porta.")
						if IsControlJustPressed(0, Keys["E"]) and not IsPedInVehicle(PlayerPedId(), GetVehiclePedIsIn(PlayerPedId(), false), false) then
							
							if not selling then
								if doorsTimeout[number].lastUsed == 0 then
									doorNumber = number		
									animation("timetable@jimmy@doorknock@", "knockdoor_idle", door.location, true)
									getItems(doorNumber)
								else
									ESX.ShowNotification("Espera: " .. doorsTimeout[number].lastUsed .. " segundos")
								end
							else
								cancelSelling()
							end
						end
					end,
				},
			},
			locations = {
				{
					x = door.location.x,
					y = door.location.y,
					z = door.location.z,
				},
				
			},
		})
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(30000)
		collectgarbage()
	end
end)
