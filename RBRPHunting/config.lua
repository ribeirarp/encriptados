Config = {}


Config.WaitTime = 5 * 60 * 1000 -- Para não spammarem em busca de animais mais perto


Config.empacotar = {
    ['carneveado'] = {
        removeQtt = 2,

        giveItemName = 'carnecacaembalada',
        giveItemCount = 1
    },
    ['carnejavali'] = {
        removeQtt = 2,

        giveItemName = 'carnecacaembalada',
        giveItemCount = 1
    },
    ['carnepuma'] = {
        removeQtt = 2,

        giveItemName = 'carnecacaembalada',
        giveItemCount = 1
    },
    ['carnecoiote'] = {
        removeQtt = 2,

        giveItemName = 'carnecacaembalada',
        giveItemCount = 1
    },
    ['robalo'] = {
        removeQtt = 5,

        giveItemName = 'peixeembalado',
        giveItemCount = 1
    },
    ['truta'] = {
        removeQtt = 5,

        giveItemName = 'peixeembalado',
        giveItemCount = 1
    },
    ['dourada'] = {
        removeQtt = 5,

        giveItemName = 'peixeembalado',
        giveItemCount = 1
    },
    ['atum'] = {
        removeQtt = 5,

        giveItemName = 'peixeembalado',
        giveItemCount = 1
    },

}

Config.SellLocations = {
  -- [0] = {
  --   ilegal = false,
  --   text = "Pressiona [E] para vender carne",
  --   coords = vector3(969.45, -2107.87, 31.47-0.9),
  --   distanceToSee = 2,
  --   items = {
  --     -- Veado
  --     [0] = {
  --       dbname = "carneveado",
  --       price = 140,
  --     },
  --     [1] = {
  --       dbname = "peleveado",
  --       price = 180,
  --     },

  --     -- Javali
  --     [2] = {
  --       dbname = "carnejavali",
  --       price = 135,
  --     },
  --     [3] = {
  --       dbname = "pelejavali",
  --       price = 170 ,
  --     },

  --     -- Porco
  --     [4] = {
  --       dbname = "carneporco",
  --       price = 130,
  --     },
  --     [5] = {
  --       dbname = "peleporco",
  --       price = 160,
  --     },

  --     -- Coelho
  --     [6] = {
  --       dbname = "carnecoelho",
  --       price = 125,
  --     },
  --     [7] = {
  --       dbname = "pelecoelho",
  --       price = 150,
  --     },
  --   }
  -- },
  -- [1] = {
  --   ilegal = true,
  --   text = "Pressiona [E] para vender carne",
  --   coords = vector3(991.82, -2175.56, 29.97-0.9),
  --   distanceToSee = 1,
  --   items = {
  --     -- Veado
  --     [0] = {
  --       dbname = "carnepuma",
  --       price = 280,
  --     },
  --     [1] = {
  --       dbname = "pelepuma",
  --       price = 350,
  --     },

  --     -- Javali
  --     [2] = {
  --       dbname = "carnecoiote",
  --       price = 300,
  --     },
  --     [3] = {
  --       dbname = "pelecoiote",
  --       price = 330 ,
  --     },

  --   }
  -- },
}


Config.blips = {
  [0] = {
    text = "Caça",
    enable = true,

    coords     = { x = -1491.15, y = 4981.58, z = 63.32 },
    sprite  = 141,
    scale   = 0.8,
    colour  = 31,
  },
  -- [1] = {
  --   text = "Venda de carne",
  --   enable = true,

  --   coords     = { x = 969.45, y = -2107.87, z = 31.47 },
  --   sprite  = 141,
  --   scale   = 0.5,
  --   colour  = 31,
  -- },

}

Config.spots = {
  [0] = {
    location = {
      coords = vector3(-1491.15, 4981.58, 63.32-0.9),
      distanceToSee = 2
    },

    -- Police
    minPolice = 0,

    --vehicle = { location = {0, 0, 0}, model = ""},
    nAnimals = 5, -- Quantidade de animais que vão spawnar
    animalsLoadTime = math.random(20, 30) * 1000, -- Depois de matar e recolher a sua carne, quanto tempo vai demorar para spawnarem outros
    animalLocations = {
      [0] = { x = -1157.0280761719 , y = 4628.0751953125, z = 145.7658996582 },
      [1] = { x = -1128.0930175781 , y = 4596.1435546875, z = 142.00071716309 },
      [2] = { x = -904.97430419922 , y = 4536.7348632812, z = 116.49992370605 },
      [3] = { x = -1537.134765625 , y = 4598.7963867188, z = 24.8480052948 },
      [4] = { x = -1517.4727783203 , y = 4445.927734375, z = 10.134407043457 },
      [5] = { x = -1460.0334472656 , y = 4355.6806640625, z = 3.2584688663483 },
      [6] = { x = -1376.8680419922 , y = 4384.12890625, z = 41.240756988525 },
      [7] = { x = -1576.7537841797 , y = 4336.1943359375, z = 3.2255485057831 },
      [8] = { x = -1555.6694335938 , y = 4435.5424804688, z = 9.1127891540527 },
      [9] = { x = -1423.68359375 , y = 4406.1357421875, z = 47.195518493652 },
      [10] = { x = -1292.9274902344 , y = 4496.4096679688, z = 20.872554779053 },
      [11] = { x = -1200.8538818359 , y = 4438.5590820312, z = 30.778690338135 },
      [12] = { x = -1200.0808105469 , y = 4441.6752929688, z = 31.379463195801 },
    },

    animalHashes = { -- Random Hash
        [0] = {
            label = "Veado",
            model = "a_c_deer",
            attack = {
                whenClose = false,
                distanceToAttack = 30,
            },
            dropItems = {
                [0] = {
                    dbname = "carneveado",
                    label = "carne de veado",
                    percentage = 100
                },
                [1] = {
                    dbname = "ossos",
                    label = "Ossos",
                    percentage = 100
                }
            }
        },

        -- Template Javali
        [1] = {
            label = "Javali",
            model = "a_c_boar",
            attack = {
                whenClose = false,
                distanceToAttack = 80,
            },
            dropItems = {
                [0] = {
                    dbname = "carnejavali",
                    label = "Carne de Javali",
                    percentage = 100,

                },
                [1] = {
                    dbname = "ossos",
                    label = "Ossos",
                    percentage = 100,
                }
            }
        },

        -- Template Coelho
        --   [2] = {
        --     label = "Coelho",
        --     model = "a_c_rabbit_01",
        --     attack = {
        --       whenClose = false,
        --       distanceToAttack = 80,
        --     },
        --     dropItems = {
        --       [0] = {
        --         dbname = "carnecoelho",
        --         label = "Carne de Coelho",
        --         percentage = 100,

        --       },
        --       [1] = {
        --         dbname = "pelecoelho",
        --         label = "Pele de Coelho",
        --         percentage = 25,
        --       }
        --     }
        --   },

        [2] = {
            label = "Puma",
            model = "a_c_mtlion",
            attack = {
                whenClose = true,
                distanceToAttack = 80,
            },
            dropItems = {
                [0] = {
                    dbname = "carnepuma",
                    label = "Carne de Puma",
                    percentage = 100
                },
                [1] = {
                    dbname = "ossos",
                    label = "Ossos",
                    percentage = 100
                },
                [2] = {
                    dbname = "peledepuma",
                    label = "Pele de Puma",
                    percentage = 100
                }
            }
        },
        -- Template Coiote
        [3] = {
            label = "Coiote",
            model = "a_c_coyote",
            attack = {
                whenClose = true,
                distanceToAttack = 80,
            },
            dropItems = {
                [0] = {
                    dbname = "carnecoiote",
                    label = "Carne de Coiote",
                    percentage = 100
                },
                [1] = {
                    dbname = "ossos",
                    label = "Ossos",
                    percentage = 100
                }
            }
        },
    },
  -- [1] = {
  --   location = {
  --     coords = vector3(-43.82, 1959.88, 190.35-0.9),
  --     distanceToSee = 2
  --   },

  --   -- Police
  --   minPolice = 0,

  --   --vehicle = { location = {0, 0, 0}, model = ""},
  --   nAnimals = 5, -- Quantidade de animais que vão spawnar
  --   animalsLoadTime = math.random(20, 30) * 1000, -- Depois de matar e recolher a sua carne, quanto tempo vai demorar para spawnarem outros
  --   animalLocations = {
  --     [0] = { x = 31.873271942139, y = 2105.8149414062 , z = 148.6047668457},
  --     [1] = { x = 40.21070098877, y = 2105.1662597656 , z = 144.46731567383},
  --     [2] = { x = 96.540328979492, y = 2133.0500488281 , z = 147.83338928223},
  --     [3] = { x = 223.11151123047, y = 2177.0090332031 , z = 120.10964202881},
  --     [4] = { x = 137.83708190918, y = 2275.1911621094 , z = 94.928253173828},
  --     [5] = { x = -39.86754989624, y = 2247.8513183594 , z = 121.83125305176},
  --     [6] = { x = -115.618019104, y = 2069.2446289062 , z = 180.48672485352},
  --     [7] =  { x = 28.58253288269, y = 1935.8709716797 , z = 199.07116699219},
  --     [8] = { x = 89.703315734863, y = 1817.4970703125 , z = 185.63848876953},
  --     [9] = { x = -126.63831329346, y = 2024.9145507812 , z = 193.09602355957},
  --     [10] = { x = -64.825912475586, y = 2186.5622558594 , z = 128.75170898438},
  --     [11] = { x = 31.536602020264, y = 2239.2917480469 , z = 101.85035705566},
  --     [12] = { x = 112.15436553955, y = 2244.5861816406 , z = 106.81652832031},
  --   },



  --   animalHashes = { -- Random Hash
  --   -- Template Puma
  --     [0] = {
  --       label = "Puma",
  --       model = "a_c_mtlion",
  --       attack = {
  --         whenClose = true,
  --         distanceToAttack = 80,
  --       },
  --       dropItems = {
  --         [0] = {
  --           dbname = "carnepuma",
  --           label = "Carne de Puma",
  --           percentage = 100
  --         },
  --         [1] = {
  --           dbname = "pelepuma",
  --           label = "Pele de Puma",
  --           percentage = 25
  --         }
  --       }
  --     },
  --   -- Template Coiote
  --     [1] = {
  --       label = "Coiote",
  --       model = "a_c_coyote",
  --       attack = {
  --         whenClose = true,
  --         distanceToAttack = 80,
  --       },
  --       dropItems = {
  --         [0] = {
  --           dbname = "carnecoiote",
  --           label = "Carne de Coiote",
  --           percentage = 100
  --         },
  --         [1] = {
  --           dbname = "pelecoiote",
  --           label = "Pele de Coiote",
  --           percentage = 25
  --         }
  --       }
  --     },
  --     -- Template porco
  --     [2] = {
  --     label = "Porco",
  --     model = "a_c_pig",
  --     attack = {
  --       whenClose = false,
  --       distanceToAttack = 80,
  --     },
  --     dropItems = {
  --       [0] = {
  --         dbname = "carneporco",
  --         label = "carne de veado",
  --         percentage = 100
  --       },
  --       [1] = {
  --         dbname = "peleporco",
  --         label = "pele de veado",
  --         percentage = 25
  --       }
  --     }
  --   },
  --     -- Template Veado
  --     [3] = {
  --       label = "Veado",
  --       model = "a_c_deer",
  --       attack = {
  --         whenClose = false,
  --         distanceToAttack = 30,
  --       },
  --       dropItems = {
  --         [0] = {
  --           dbname = "carneveado",
  --           label = "carne de veado",
  --           percentage = 100
  --         },
  --         [1] = {
  --           dbname = "peleveado",
  --           label = "pele de veado",
  --           percentage = 25
  --         }
  --       }
  --     }
  --   }
  -- }
    }
}