local Keys = {

    ["ESC"] = 322,
    ["F1"] = 288,
    ["F2"] = 289,
    ["F3"] = 170,
    ["F5"] = 166,
    ["F6"] = 167,
    ["F7"] = 168,
    ["F8"] = 169,
    ["F9"] = 56,
    ["F10"] = 57,

    ["~"] = 243,
    ["1"] = 157,
    ["2"] = 158,
    ["3"] = 160,
    ["4"] = 164,
    ["5"] = 165,
    ["6"] = 159,
    ["7"] = 161,
    ["8"] = 162,
    ["9"] = 163,
    ["-"] = 84,
    ["="] = 83,
    ["BACKSPACE"] = 177,

    ["TAB"] = 37,
    ["Q"] = 44,
    ["W"] = 32,
    ["E"] = 38,
    ["R"] = 45,
    ["T"] = 245,
    ["Y"] = 246,
    ["U"] = 303,
    ["P"] = 199,
    ["["] = 39,
    ["]"] = 40,
    ["ENTER"] = 18,

    ["CAPS"] = 137,
    ["A"] = 34,
    ["S"] = 8,
    ["D"] = 9,
    ["F"] = 23,
    ["G"] = 47,
    ["H"] = 74,
    ["K"] = 311,
    ["L"] = 182,

    ["LEFTSHIFT"] = 21,
    ["Z"] = 20,
    ["X"] = 73,
    ["C"] = 26,
    ["V"] = 0,
    ["B"] = 29,
    ["N"] = 249,
    ["M"] = 244,
    [","] = 82,
    ["."] = 81,

    ["LEFTCTRL"] = 36,
    ["LEFTALT"] = 19,
    ["SPACE"] = 22,
    ["RIGHTCTRL"] = 70,

    ["HOME"] = 213,
    ["PAGEUP"] = 10,
    ["PAGEDOWN"] = 11,
    ["DELETE"] = 178,

    ["LEFT"] = 174,
    ["RIGHT"] = 175,
    ["TOP"] = 27,
    ["DOWN"] = 173,

    ["NENTER"] = 201,
    ["N4"] = 108,
    ["N5"] = 60,
    ["N6"] = 107,
    ["N+"] = 96,
    ["N-"] = 97,
    ["N7"] = 117,
    ["N8"] = 61,
    ["N9"] = 118

}

local ESX = nil

local PlayerData = {
    canStartCaca = true,
    coords = vector3(0, 0, 0),
    vehicle = {
        isInside = false,
        entity = nil,
        huntingVehicle = nil
    },
    hunting = {

        isHunting = false,

        animals = {}

    },

    spotData = nil

}

Citizen.CreateThread(function()

    while ESX == nil do

        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)

        Citizen.Wait(10)

    end

end)


Citizen.CreateThread(function()

    while true do

        local sleep = 5000

        -- Coisas que não precisam de correr muito frequentemente

        PlayerData.ped = GetPlayerPed(-1)

        Citizen.Wait(sleep)

    end

end)

Citizen.CreateThread(function()

    while true do

        local sleep = 1000
		if PlayerData.ped ~= nil then
			local vehicle = GetVehiclePedIsIn(PlayerData.ped, false)

			-- Player Coords

			local coords = GetEntityCoords(PlayerData.ped)

			PlayerData.coords = coords

			-- Vehicle

			if vehicle ~= 0 then

				PlayerData.vehicle = {

					isInside = true,

					entity = vehicle

				}

			elseif vehicle == 0 and PlayerData.vehicle ~= nil and PlayerData.vehicle.entity ~= nil then

				PlayerData.vehicle = {

					isInside = false,

					entity = nil

				}

			end
		end
        Citizen.Wait(sleep)
    end
end)

function DrawM(hint, type, coords)

    ESX.Game.Utils.DrawText3D({
        x = coords.x,
        y = coords.y,
        z = coords.z + 1.0
    }, hint, 0.4)

    DrawMarker(type, coords.x, coords.y, coords.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 1.5, 255, 255, 255, 100, false,
        true, 2, false, false, false, false)

end

function stopCaca()

    DeleteEntity(HuntCar)

    for index, value in pairs(AnimalsInSession) do

        if DoesEntityExist(value.id) then

            DeleteEntity(value.id)

        end

    end

end


local data = {
    player = {
        anim = {
            dict = nil,
            anim = nil,
            isPlaying = false
        }
    }
}

local empacotar = false
local didsomething = false
RegisterNetEvent('RBRPHunting:empacotar')
AddEventHandler('RBRPHunting:empacotar', function(identifier)
	if not empacotar then
		exports["mf-inventory"]:getInventoryItems(identifier,function(items)
			for iitem, item in pairs(items) do
				if Config.empacotar[item.name] ~= nil then
					if item.count >= Config.empacotar[item.name].removeQtt then
						didsomething = true
						empacotar = true
						data.player.anim.dict = 'amb@prop_human_bum_bin@base'
						data.player.anim.anim = 'base' 
						data.player.anim.isPlaying = true
						TriggerServerEvent('farmer:removeItem', item.name, Config.empacotar[item.name].removeQtt)
						TriggerServerEvent('farmer:removeItem', 'plastico', 1)
						FreezeEntityPosition(PlayerData.ped, true)
						exports['progressBars']:startUI(15200, "A embalar...")
						Citizen.Wait(15000)
						
	
	
						FreezeEntityPosition(PlayerData.ped, false)
						ClearPedTasks(PlayerData.ped)
						data.player.anim.isPlaying = false
						data.player.anim.dict = nil
						data.player.anim.anim = nil
						TriggerServerEvent('farmer:giveItem', {item = Config.empacotar[item.name].giveItemName, count = Config.empacotar[item.name].giveItemCount})
						empacotar = false
						break
					end
				end
			end

			if didsomething then
				didsomething = false
			else
				exports['mythic_notify']:SendAlert('error', 'Não tens nada para empacotar')
			end
		end)
	else
		exports['mythic_notify']:SendAlert('error', 'Já estás a empacotar algo')
	end
end)

function LoadAnimDict(dict)

    while (not HasAnimDictLoaded(dict)) do

        RequestAnimDict(dict)

        Citizen.Wait(10)

    end

end

function LoadModel(model)

    while not HasModelLoaded(model) do

        RequestModel(model)

        Citizen.Wait(10)

    end

end

function createAnimal(id, position, animalData)

    LoadModel(animalData.model)

    local animal = CreatePed(4, GetHashKey(animalData.model), position.x, position.y, position.z, 50)

    TaskWanderStandard(animal, true, true)

    SetEntityHealth(animal, 100)

    local animalBlip = AddBlipForEntity(animal)

    SetBlipSprite(animalBlip, 153)

    SetBlipColour(animalBlip, 2)

    BeginTextCommandSetBlipName("STRING")

    AddTextComponentString(animalData.label)

    EndTextCommandSetBlipName(animalBlip)

    animalData.attack.attacking = false

    PlayerData.hunting.animals[id] = {

        ped = animal,

        blip = animalBlip,

        data = animalData,

        coords = position

    }

end

function spawnAnimals()

    if PlayerData.spotData.spot.firstTimeAnimals ~= nil then

        if PlayerData.spotData.spot.firstTimeAnimals then

            PlayerData.spotData.spot.firstTimeAnimals = false

        end

    end

    -- Escolher locais:

    local animalPositions = {}

    local x = 0

    while x < PlayerData.spotData.spot.nAnimals do

        local position = PlayerData.spotData.spot.animalLocations[math.random(0,
                             #PlayerData.spotData.spot.animalLocations)]

        local match = false

        if #animalPositions > 0 then

            for iAnimalPosition, animalPosition in pairs(animalPositions) do

                if #(vector3(animalPosition.x, animalPosition.y, animalPosition.z) -
                    vector3(position.x, position.y, position.z)) < 1 then

                    match = true

                end

            end

        end

        if not match then

            x = x + 1

            table.insert(animalPositions, position)

        end

    end

    for index, coord in pairs(animalPositions) do

        createAnimal(index, coord,
            PlayerData.spotData.spot.animalHashes[math.random(0, #PlayerData.spotData.spot.animalHashes)])

    end

end

function SlaughterAnimal(animalData)

    LoadAnimDict("amb@medic@standing@kneel@base")

    LoadAnimDict("anim@gangops@facility@servers@bodysearch@")

    TaskPlayAnim(PlayerData.ped, "amb@medic@standing@kneel@base", "base", 8.0, -8.0, -1, 1, 0, false, false, false)

    TaskPlayAnim(PlayerData.ped, "anim@gangops@facility@servers@bodysearch@", "player_search", 8.0, -8.0, -1, 48, 0,
        false, false, false)

    Citizen.Wait(5000)

    ClearPedTasks(PlayerData.ped)

    local AnimalWeight = math.random(10, 160) / 10

    -- ESX.ShowNotification('You slaughtered the animal and recieved an meat of ' ..AnimalWeight.. 'kg')

    TriggerServerEvent('RBRPHunting:getAnimalItems', animalData)

    DeleteEntity(animalData.ped)

end

function deleteAnimals()

    for x, animal in pairs(PlayerData.hunting.animals) do

        DeleteEntity(animal.ped)

    end

end

DrawBusySpinner = function(text)

    SetLoadingPromptTextEntry("STRING")

    AddTextComponentSubstringPlayerName(text)

    ShowLoadingPrompt(3)

end

function createBlip(id, blip)

    exports.ft_libs:AddBlip("blipCaca: " .. id, {

        x = blip.coords.x,

        y = blip.coords.y,

        z = blip.coords.z,

        text = blip.text,

        imageId = blip.sprite,

        colorId = blip.colour,

        scale = blip.scale

    })

end


RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    for x, blip in pairs(Config.blips) do
        if blip.enable then
            createBlip(x, blip)
        end
    end
end)

Citizen.CreateThread(function()

    while true do

        local sleep = 500

        if PlayerData.ped ~= nil and PlayerData.hunting ~= nil then
            if PlayerData.hunting.isHunting then

                if #PlayerData.hunting.animals == 0 and PlayerData.spotData.spot.firstTimeAnimals ~= nil and
                    PlayerData.spotData.spot.firstTimeAnimals == false then

                    DrawBusySpinner("A procurar por mais animais")

                    Citizen.Wait(PlayerData.spotData.spot.animalsLoadTime)

                    spawnAnimals()

                    RemoveLoadingPrompt()

                else

                    for ianimal, animal in pairs(PlayerData.hunting.animals) do

                        if DoesEntityExist(animal.ped) then

                            local AnimalCoords = GetEntityCoords(animal.ped)

                            local AnimalHealth = GetEntityHealth(animal.ped)

                            local distance = #(PlayerData.coords - AnimalCoords)

                            if AnimalHealth <= 0 then

                                SetBlipColour(animal.blip, 1)

                                if distance < 2.0 then

                                    ESX.Game.Utils.DrawText3D({
                                        x = AnimalCoords.x,
                                        y = AnimalCoords.y,
                                        z = AnimalCoords.z + 1
                                    }, '[E] Remover carne', 0.4)

                                    if IsControlJustReleased(0, Keys['E']) and not PlayerData.vehicle.isInside then

                                        if GetSelectedPedWeapon(PlayerData.ped) == GetHashKey('WEAPON_KNIFE') then

                                            if DoesEntityExist(animal.ped) then

                                                table.remove(PlayerData.hunting.animals, ianimal)

                                                SlaughterAnimal(animal)

                                            end

                                        else

                                            exports['mythic_notify']:SendAlert('error', 'Tens de usar uma faca', 5000)

                                        end

                                    end

                                end

                            end

                            if distance < animal.data.attack.distanceToAttack and animal.data.attack.whenClose then

                                TaskCombatPed(animal.ped, PlayerData.ped, 0, 16)

                            end

                        end

                    end

                end

                sleep = 5
            end
        end

        Citizen.Wait(sleep)

    end

end)

RegisterNetEvent("RBRPHunting:startCaca")

AddEventHandler("RBRPHunting:startCaca", function(x, spot)

    PlayerData.canStartCaca = false

    Citizen.Wait(2000)

    exports['mythic_notify']:SendAlert('inform', 'Siga à caça', 5000)

    RemoveLoadingPrompt()

    PlayerData.hunting.isHunting = true

    PlayerData.spotData = {

        id = x,

        spot = spot

    }

    PlayerData.spotData.spot.firstTimeAnimals = true

    spawnAnimals()

    SetTimeout(Config.WaitTime, function()

        PlayerData.canStartCaca = true

    end)

end)

RegisterNetEvent("RBRPHunting:cantStartCaca")
AddEventHandler("RBRPHunting:cantStartCaca", function()
    Citizen.Wait(2000)
    exports['mythic_notify']:SendAlert('inform', 'Não andam nenhuns animais por aqui', 5000)
    RemoveLoadingPrompt()
end)


Citizen.CreateThread(function()
    while true do
        local sleep = 500
        if ESX and PlayerData.ped ~= nil and PlayerData.vehicle ~= nil then
            for x, spot in pairs(Config.spots) do
                if PlayerData.vehicle ~= nil and not PlayerData.vehicle.isInside and #(spot.location.coords - PlayerData.coords) < spot.location.distanceToSee then
                    if PlayerData.hunting ~= nil then
                        if PlayerData.hunting.isHunting then
                            DrawM("[E] para terminar a caça", 27, spot.location.coords)
                        else
                            DrawM("[E] para começar a caçar", 27, spot.location.coords)
                        end
                        if not PlayerData.hunting.isHunting and IsControlJustReleased(0, Keys['E']) then
                            -- Come�ou a ca�ar
                            if PlayerData.canStartCaca then
                                DrawBusySpinner("A procurar animais")
                                TriggerServerEvent("RBRPHunting:startCaca", x, spot)
                                sleep = 5000
                            else
                                exports['mythic_notify']:SendAlert('inform', 'Espera pah, tem calminha...', 5000)
                            end

                        elseif PlayerData.hunting.isHunting and IsControlJustReleased(0, Keys['E']) then
                            -- Terminou a ca�a
                            PlayerData.hunting.isHunting = false
                            PlayerData.spotData = nil
                            deleteAnimals()
                        end
                        if sleep == 500 then
                            sleep = 1
                        end
                    end
                end
            end
        end
        Citizen.Wait(sleep)
    end
end)

Citizen.CreateThread(function()
    while true do
        local sleep = 500
		if PlayerData.ped ~= nil and PlayerData.vehicle ~= nil then
			for x, spot in pairs(Config.SellLocations) do
				if not PlayerData.vehicle.isInside and #(spot.coords - PlayerData.coords) < spot.distanceToSee then
					DrawM("[E] para vendar carne", 27, spot.coords)
					if IsControlJustReleased(0, Keys['E']) then
						TriggerServerEvent("RBRPHunting:pay", spot)
					end
					sleep = 2
				end
			end
		end
        Citizen.Wait(sleep)
    end
end)


function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(0)
    end
end

Citizen.CreateThread(function()
    local lastIsPlayingAnim = nil
    while true do
        local sleep = 200
		if PlayerData.ped ~= nil then
            if data.player.anim.isPlaying then
				SetCurrentPedWeapon(PlayerData.ped,GetHashKey("WEAPON_UNARMED"),true)
                if not IsEntityPlayingAnim(PlayerData.ped,data.player.anim.dict,data.player.anim.anim,3) then
					loadAnimDict(data.player.anim.dict)
					TaskPlayAnim(PlayerData.ped,data.player.anim.dict,data.player.anim.anim,4.0, 1.0, -1,49,0, 0, 0, 0)
                end
                sleep = 50
            end
    
            if lastIsPlayingAnim == true and lastIsPlayingAnim ~= data.player.anim.isPlaying then
                    ClearPedTasks(PlayerData.ped)
            end
    
            lastIsPlayingAnim = data.player.anim.isPlaying
        end

        Citizen.Wait(sleep)
    end
end)