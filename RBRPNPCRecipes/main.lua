local ESX						= nil
local Keys = {
    ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
    ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
    ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
    ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
    ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
    ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
    ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
    ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
    ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
local Time = GetGameTimer()
local function canClick(ms)
    if (GetGameTimer() - Time) > ms then
        Time = GetGameTimer()
        return true
    else
        Time = GetGameTimer()
        return false
    end
end

local saveData = {}
local pedCache = {
    [0] = nil,
    [1] = nil
}



local function DrawText3D(coords, text)
    SetDrawOrigin(coords)
    SetTextScale(0.35, 0.35)
    SetTextFont(4)
    SetTextEntry("STRING")
    SetTextCentre(1)
    AddTextComponentString(text)
    DrawText(0.0, 0.0)
    DrawRect(0.0, 0.0125, 0.015 + text:gsub("~.-~", ""):len() / 370, 0.03, 45, 45, 45, 150)
    ClearDrawOrigin()
end


local function openMenu(placeid, place)
    local elements = {}
    table.insert(elements, {value = "sim", label = "Sim [" .. place.price .. "€" .. "]"})
    table.insert(elements, {value = "nao", label = "Não"})


    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'menu',
    {
        title    = "Queres mesmo comprar uma receita?",
        align = 'right',
        elements = elements
    }, function(data, menu)
        menu.close()
        if data.current.value == "sim" then
            TriggerServerEvent("RBRPNPCRecipes:getRecipe", placeid, place)
        end
    end, function(data, menu)
        menu.close()
    end)

end

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

_RequestModel = function(hash)
    if type(hash) == "string" then hash = GetHashKey(hash) end
    RequestModel(hash)
    while not HasModelLoaded(hash) do
        Wait(0)
    end
end

Citizen.CreateThread(function()
	while true do
        print(pedCache[0], pedCache[1])
        for x,ped in pairs(Config.NPCLocations) do
            _RequestModel(GetHashKey(ped.model))
            if not DoesEntityExist(pedCache[x]) then
                pedCache[x] = CreatePed(4, GetHashKey(ped.model), ped.coords.x, ped.coords.y, ped.coords.z, ped.heading)
                SetEntityAsMissionEntity(pedCache[x])
                SetBlockingOfNonTemporaryEvents(pedCache[x], true)
                FreezeEntityPosition(pedCache[x], true)
                SetEntityInvincible(pedCache[x], true)
            end
            SetModelAsNoLongerNeeded(GetHashKey(ped.model))
        end

        Citizen.Wait(500)
        break
    end
end)


function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
	if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end


local showingUI = {}
Citizen.CreateThread(function()
    while Config.NPCLocations == nil do
        Citizen.Wait(500)
    end

    for x,i in pairs(Config.NPCLocations) do
        showingUI[x] = false
    end

end)

Citizen.CreateThread(function()
	while true do
        local sleepThread = 500
        local playerPed = GetPlayerPed(-1)
        local playerCoords = GetEntityCoords(playerPed)
        for x,place in pairs(Config.NPCLocations) do
            if #(place.coords-playerCoords) < 2 then
                if not showingUI[x] then
                    showingUI[x] = true
                    ESX.ShowHelpNotification("Falar com o " .. place.name)
                end
                if IsControlJustReleased(0, Keys['E']) and not IsPedInVehicle(playerPed, GetVehiclePedIsIn(playerPed, false), false) then
                    TriggerEvent('luke_textui:HideUI')
                    if canClick(500) then
                        openMenu(x, place)
                    else
                        exports['mythic_notify']:SendAlert('inform', "Espera campeããão")
                    end
                end
            else
                if showingUI[x] then
                    showingUI[x] = false
                    TriggerEvent('luke_textui:HideUI')
                end
            end
            
            sleepThread = 1
        end

        Citizen.Wait(sleepThread)
    end
end)
