Config = {}



Config.NPCLocations = {
    --Mimo vespucci componentes armas
    [0] = {

        model = "s_m_y_mime",

        price = 4000,

        name = "Top Boy",

        coords = vector3(-1176.03, -1547.68, 17.32-0.9),

        heading = 306.14,

        timeBetweenAnotherRecipe = 60 * 60 * 1000,

        phrases = {

            [0] = "Para fazer um corpo de pistola acho que preciso de um gatilho e cabos", -- corpo pistola
			
            [1] = "Estive até agora a limpar o ferrolho e a mira do meu cano de pistol", -- cano pistola

            [2] = "O Artur que mora para os lados de Sandy disse-me que preciso de placas de madeira, cao de arma, gatilho e coronha para fazer um Corpo de Shotgun", -- corpo shotgun

            [3] = "Não consigo ir à caça enquanto não mudar o cano da minha shotgun. Preciso de cano médio e serrote", -- cano shotgun

            [4] = "Um corpo pequeno pode provocar grandes danos. Vou precisar de placas de alumínio, gatilho, cabos e um pente", -- corpo pequeno

            [5] = "O cano curto é composto por placas de alumínio, mira, ferrolho e cão", -- cano curto

            [6] = "Vou daqui a nada à serraçao e à mina para ir buscar placas de madeira e placas de alumínio. Junto-lhe receptáculo, coronha, gatilho e um pente e tenho o meu corpo médio", -- corpo medio

            [7] = "Ando à procura de placas de madeira, placas de alumínio, receptáculo, coronha, gatilho e pente para fazer um corpo médio", -- cano medio

            [8] = "Uma espingarda bem feita necessita de um corpo grande e muito investimento. Madeira nobre, placas de madeira receptaculo, coronha, gatilho e pente são os seus componentes", -- corpo grande

            [9] = "O Jorge de Paleto anda à procura de placas de alumínio, titanio, mira, ferrolho e cao de arma para fazer um cano longo", -- cano longo

            [10] = "As ideias puxam o gatilho, mas o instinto abastece a arma. E esta hein? Precisas de fragmentos de metal, aço, parafusos chave de fendas e um maçarico para fabricar um", -- gatilho

            [11] = "Queres fazer um tambor? Olha que este nao é de tocar música, é de disparar. Vais precisar de alumínio, aço, fragmentos de metal parafusos e um maçarico", -- tambor

            [12] = "Aço, mola, martelo e um maçarico é o que precisas para fazer um cão. Cuidado que este não ladra. Dispara!", -- cao arma

            [13] = "Só me falta uma coronha nova para voltar à caça. Vou precisar de tábuas de madeira, parafusos, martelo e serrote ", -- coronha

            [14] = "O receptáculo é pesado?! Como não se leva aço, alumínio, parafusos, mola, chave de fendas e alicate", -- receptaculo

            [15] = "Se estiver na minha MIRA é xeque-mate! Para fazeres uma necessitas de fragmentos de metal, aço, parafusos, alicate e chave de fendas", -- mira

            [16] = "Fragmentos de metal, alumínio, aço, martelo e um maçarico. É tudo o que preciso para fazer PLACAS DE ALUMÍNIO", -- placa aluminio

            [17] = "Ajudas-me a fazer umas placas de madeira. Traz tábuas de madeira, madeira, martelo e um serrote", -- placa madeira

            [18] = "Vou ali com o Xavier à procura de MOLAS. Se não encontrar vou ter de ir comprar fragmentos de metal, aço, alicate e um maçarico", -- mola

            [19] = "Ouvi dizer que os CABOS estão a dar bom dinheiro. Temos de arranjar parafusos, tábuas de madeira, alicate e chave de fendas", -- cabos

            [20] = "Como assim, uns à porta, outros ao FERROLHO?! Vai mas é buscar fragmentos de metal, aço, mola, alicate e chave de fendas", -- ferrolho

            [21] = "Não é PENTE para o cabelo, é para a minha arma. Vou ter de pedir ao Zé Júlio para me arranjar alumínio, mola, aço, alicate e um maçarico", -- pente

            [22] = "Não sou de me esconder, dou o corpo às balas. Por falar nisso estou à procura de aço, mola e pólvora para fazer um CLIP", -- clip

            -- [23] = "", -- 

        }

    },

    --Mimo sandy armas
    [1] = {

        model = "s_m_y_mime",

        price = 4000,

        name = "Horrid",

        coords = vector3(1529.05, 3794.56, 34.45-0.9),

        heading = 294.8,

        timeBetweenAnotherRecipe = 60 * 60 * 1000,

        phrases = {

            [0] = "Curto bue de deagles, se calhar consigo fazer uma com placas de alumínio, placas de madeira, corpo pequeno e cano curto", -- 
			
            [1] = "As pistolas normais são demasiado leves para mim, se calhar ficam mais PESADAS com cano de pistola e corpo de pistola", -- 

            [2] = "Adoro cortar os canos às shotguns, ficam um mimo com cano de shotgun, corpo de shotgun, ferrolho e mira", -- 

            [3] = "Na altura dos cowboys é que era o que eles não faziam com placas de madeira, placas de alumínio, tambor, corpo pequeno e cano médio", -- 

            [4] = "Se um cano não chega experimenta com dois basta teres placas de alumínio, placas de madeira, corpo de shotgun e cano de shotgun", -- 

            [5] = "Odeio pistolas que disparam devagar, mas isso muda se eu usar cano curto e corpo de pistola", -- 

            [6] = "Ser mini nem sempre é mau, morrem na mesma com corpo médio e cano médio ", -- 

            [7] = "MICROscopios são para cientistas aqui usamos cano curto e corpo pequeno", -- 

            [8] = "Não consigo por uma AK no bolso, mas talvez consiga se a compactar com placas de madeira, placas de alumínio, corpo médio e cano médio", -- 

            [9] = "Embora decorativas pistolas antigas são mortíferas, basta usar placas de madeira, placas de alumínio, corpo pequeno e cano curto", -- 

            [10] = "Se os mafiosos usam a Tommygun não pode ser má, talvez crave placas de madeira, placas de alumínio, corpo médio e cano médio para tentar", -- 

            [11] = "Se queres ser um assasino FAMoso não te esqueçAS de usar placas de madeira, placas de alumínio, corpo médio e cano médio", -- 

            [12] = "Os russos sempre disseram, Калашников лучшее оружие e eu concordo, bota usar placas de madeira, placas de alumínio, cano longo e corpo grande", -- 

            [13] = "Adorava ver os MOScãoteiros QUE serie TrEmenda, mas ficava melhor com placas de alumínio, placas de madeira, corpo grande e cano longo", -- 

            -- [14] = "", --  

        }

    },

    -- Mimo vinhas COCA
    [2] = {

        model = "s_m_y_mime",

        price = 4000,

        name = "Carlitos LaGangzz",

        coords = vector3(-1897.95, 2079.42, 140.99-0.9),

        heading = 277.8,

        timeBetweenAnotherRecipe = 60 * 60 * 1000,

        phrases = {
	
            [0] = "A farinha dos bolos não é o unico PÓ BRANCO onde deves juntar BICARBONATO DE SÓDIO", -- bicarbonato sodio
			
            [1] = "Será que fica mais fácil que ÁCIDO CLORÍDRICO na FARINHA da moca e faz crescer", -- acido cloridrico

            [2] = "GASOLINA vai te dar as octanas que a tua FARINHA precisa para bater mais", -- Gasolina	

            [3] = "A FARINHA dos melhores cartéis no mundo contém BICARBONATO DE SÓDIO, ÁCIDO CLORÍDRICO, GASOLINA E FOLHAS DE COCA na sua composição", -- folhas coca	

            [4] = "Com PLáSTICO suficiente posso meter a minha FARINHA em SACOS decentes", -- cocaina	

            [5] = "Um SACO DE COCA talvez precise de varias COCAINAS ate encher o SACO DE DROGA", -- saco de droga	

            [6] = "Drogados? Químicos. Cientistas? LABORATÓRIO. Hotel? Trivago.", -- loja quimicos	

            [7] = "Há sempre um cheiro a peixe e barcos na minha COCA, se calhar devia lá ir", -- spot processo	

            [8] = "Já vi americanos com cada nariz! De certeza que me compram a FARINHA toda. Mas deve haver quem compre melhor", -- venda

            [9] = "Sempre quis entrar na Dark Web. Talvez com um laptop e um raspberry possa dar bypass nas restrições", -- hacking laptop

            -- [10] = "", --  

        }

    },

    --Mimo paleto META
    [3] = {

        model = "s_m_y_mime",

        price = 4000,

        name = "Lil Ricardo",

        coords = vector3(-466.97, 6288.07, 13.61-0.9),

        heading = 96.38,

        timeBetweenAnotherRecipe = 60 * 60 * 1000,

        phrases = {

			
			[0] = "de vez em quando antes de chegar à META preciso de dar umas bombadas de, EFEDRINA", -- efedrina	
			
            [1] = "nunca curti do cheiro a ACETONA na minha META", -- acetona

            [2] = "fico mais eléctrico quando usam BATERIAS DE LÍTIO na minha META", -- bateria de litio

            [3] = "nunca METAs os dedos em ÁCIDO SULFÚRICO", -- acido sulfurico	

            [4] = "META que é meta tem que ter HIDRÓXIDO DE SÓDIO", -- hidroxido de sodio

            [5] = "GASOLINA não é só para carros, também a podes pôr na META", -- Gasolina

            [6] = "ÓLEO DOS TRAVÕES para travar melhor e para uma META melhor", -- oleo travoes

            [7] = "Talvez se eu juntar, EFEDRINA, ACETONA , BATERIAS DE LÍTIO, ÁCIDO SULFÚRICO, HIDRÓXIDO DE SÓDIO, GASOLINA e ÓLEO DOS TRAVÕES consiga uns cristais de META bonitos", -- cristal de meta	

            [8] = "Com PLÁSTICO suficiente posso meter a minha META em SACOS decentes", -- saco de droga

            [9] = "Como um CRISTAL de meta dentro de um SACO DE DROGA fica sozinho, talvez dois sejam melhores nos meus SACOS DE META", -- saco de meta

            [10] = "Drogado? Cientista? Não interessa, ambos dão usos aos seus LABORATORIOS", -- loja quimicos

            [11] = "Conheci um drogado que fazia, perto do monte CHILLIAD dentro na sua JOURNEY, a melhor META do mundo", -- processo cristais

            [12] = "Uma JOURNEY talvez tenha espaço suficiente lá dentro para eu fechar uns SACOS DE META", -- ensacamento

            [13] = "Talvez estes americanos comprem os meus SACOS DE META, mas talvez haja um sítio melhor?", -- venda


        }

    },

}
