bpPlayerInfo = {}


Recipes = {
--- ITENS ---
  ["item_all_1"] = {
    itemDBName = "mola",
    items = {
      [1] = {name = "fragmentosmetal", count = 30},
      [2] = {name = "aco", count = 10},
      [3] = {name = "alicate", count = 1},
      [4] = {name = "blowtorch", count = 1},
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
  ["item_all_2"] = {
    itemDBName = "placasmadeira",
    items = {
      [1] = {name = "packaged_plank", count = 40},
      [2] = {name = "wood", count = 20},
      [3] = {name = "martelo", count = 1},
      [4] = {name = "serrote", count = 1},
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
  ["item_all_3"] = {
        itemDBName = "placasaluminio",
        items = {
          [1] = {name = "fragmentosmetal", count = 360},
          [2] = {name = "aluminio", count = 250},
          [3] = {name = "aco", count = 50},
          [4] = {name = "martelo", count = 1},
          [5] = {name = "blowtorch", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_4"] = {
        itemDBName = "pente",
        items = {
          [1] = {name = "aluminio", count = 50},
          [2] = {name = "mola", count = 510},
          [3] = {name = "aco", count = 5},
          [4] = {name = "alicate", count = 1},
          [5] = {name = "blowtorch", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_5"] = {
        itemDBName = "coronha",
        items = {
          [1] = {name = "packaged_plank", count = 30},
          [2] = {name = "parafusos", count = 15},
          [3] = {name = "martelo", count = 1},
          [4] = {name = "serrote", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_6"] = {
        itemDBName = "receptaculo",
        items = {
          [1] = {name = "aco", count = 55},
          [2] = {name = "aluminio", count = 50},
          [3] = {name = "parafusos", count = 35},
          [4] = {name = "mola", count = 20},
          [5] = {name = "chavedefendas", count = 1},
          [6] = {name = "alicate", count = 1},
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_7"] = {
        itemDBName = "cabos",
        items = {
          [1] = {name = "parafusos", count = 50},
          [2] = {name = "packaged_plank", count = 45},
          [3] = {name = "alicate", count = 1},
          [4] = {name = "chavedefendas", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_8"] = {
        itemDBName = "gatilho",
        items = {
          [1] = {name = "fragmentosmetal", count = 250},
          [2] = {name = "aco", count = 100},
          [3] = {name = "parafusos", count = 100},
          [4] = {name = "chavedefendas", count = 1},
          [5] = {name = "blowtorch", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_9"] = {
        itemDBName = "cao_arma",
        items = {
          [1] = {name = "aco", count = 20},
          [2] = {name = "mola", count = 10},
          [3] = {name = "martelo", count = 1},
          [4] = {name = "blowtorch", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_10"] = {
        itemDBName = "tambor",
        items = {
          [1] = {name = "aluminio", count = 200},
          [2] = {name = "aco", count = 150},
          [3] = {name = "fragmentosmetal", count = 150},
          [4] = {name = "parafusos", count = 100},
          [5] = {name = "blowtorch", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_11"] = {
        itemDBName = "ferrolho_arma",
        items = {
          [1] = {name = "fragmentosmetal", count = 160},
          [2] = {name = "aco", count = 65},
          [3] = {name = "mola", count = 20},
          [4] = {name = "alicate", count = 1},
          [5] = {name = "chavedefendas", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_12"] = {
        itemDBName = "mira",
        items = {
          [1] = {name = "fragmentosmetal", count = 210},
          [2] = {name = "aco", count = 90},
          [3] = {name = "parafusos", count = 85},
          [4] = {name = "alicate", count = 1},
          [5] = {name = "chavedefendas", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_13"] = {
        itemDBName = "corpo_pistola",
        items = {
          [1] = {name = "gatilho", count = 1},
          [2] = {name = "cabos", count = 1},
          [3] = false,
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_14"] = {
        itemDBName = "cano_pistola",
        items = {
          [1] = {name = "ferrolho_arma", count = 1},
          [2] = {name = "mira", count = 1},
          [3] = false,
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_15"] = {
        itemDBName = "corpo_shotgun",
        items = {
          [1] = {name = "placasmadeira", count = 2},
          [2] = {name = "cao_arma", count = 1},
          [3] = {name = "gatilho", count = 1},
          [4] = {name = "coronha", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_16"] = {
        itemDBName = "cano_shotgun",
        items = {
          [1] = {name = "cano_medio", count = 1},
          [2] = {name = "serrote", count = 1},
          [3] = false,
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_17"] = {
        itemDBName = "corpo_grande",
        items = {
          [1] = {name = "madeiranobre", count = 10},
          [2] = {name = "placasmadeira", count = 10},
          [3] = {name = "receptaculo", count = 1},
          [4] = {name = "coronha", count = 1},
          [5] = {name = "gatilho", count = 1},
          [6] = {name = "pente", count = 1},
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_18"] = {
        itemDBName = "cano_curto",
        items = {
          [1] = {name = "placasaluminio", count = 5},
          [2] = {name = "mira", count = 1},
          [3] = {name = "ferrolho_arma", count = 1},
          [4] = {name = "cao_arma", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_19"] = {
        itemDBName = "corpo_pequeno",
        items = {
          [1] = {name = "placasaluminio", count = 10},
          [2] = {name = "gatilho", count = 1},
          [3] = {name = "cabos", count = 1},
          [4] = {name = "pente", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_20"] = {
        itemDBName = "corpo_medio",
        items = {
          [1] = {name = "placasmadeira", count = 10},
          [2] = {name = "placasaluminio", count = 5},
          [3] = {name = "receptaculo", count = 1},
          [4] = {name = "coronha", count = 1},
          [5] = {name = "gatilho", count = 1},
          [6] = {name = "pente", count = 1},
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_21"] = {
        itemDBName = "cano_medio",
        items = {
          [1] = {name = "placasaluminio", count = 10},
          [2] = {name = "mira", count = 1},
          [3] = {name = "ferrolho_arma", count = 1},
          [4] = {name = "cao_arma", count = 1},
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_22"] = {
        itemDBName = "cano_longo",
        items = {
          [1] = {name = "placasaluminio", count = 10},
          [2] = {name = "titanio", count = 1},
          [3] = {name = "mira", count = 1},
          [4] = {name = "ferrolho_arma", count = 1},
          [5] = {name = "cao_arma", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_23"] = {
        itemDBName = "WEAPON_COMPACTRIFLE",
        items = {
          [1] = {name = "placasmadeira", count = 3},
          [2] = {name = "placasaluminio", count = 1},
          [3] = {name = "corpo_medio", count = 1},
          [4] = {name = "cano_medio", count = 1},
          [5] = {name = "bp_compactrifle", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_24"] = {
        itemDBName = "WEAPON_HEAVYPISTOL",
        items = {
          [1] = {name = "cano_pistola", count = 1},
          [2] = {name = "corpo_pistola", count = 1},
          [3] = {name = "bp_heavypistol", count = 1},
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_25"] = {
        itemDBName = "WEAPON_MACHINEPISTOL",
        items = {
          [1] = {name = "cano_curto", count = 1},
          [2] = {name = "corpo_pistola", count = 1},
          [3] = {name = "bp_machinepistol", count = 1},
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_26"] = {
        itemDBName = "WEAPON_MICROSMG",
        items = {
          [1] = {name = "cano_curto", count = 1},
          [2] = {name = "corpo_pequeno", count = 1},
          [3] = {name = "bp_microsmg", count = 1},
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_27"] = {
        itemDBName = "WEAPON_SAWNOFFSHOTGUN",
        items = {
          [1] = {name = "cano_shotgun", count = 1},
          [2] = {name = "corpo_shotgun", count = 1},
          [3] = {name = "ferrolho_arma", count = 1},
          [4] = {name = "mira", count = 1},
          [5] = {name = "bp_sawnoffshotgun", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_28"] = {
        itemDBName = "WEAPON_VINTAGEPISTOL",
        items = {
          [1] = {name = "placasmadeira", count = 7},
          [2] = {name = "placasaluminio", count = 5},
          [3] = {name = "corpo_pequeno", count = 1},
          [4] = {name = "cano_curto", count = 1},
          [5] = {name = "bp_vintagepistol", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_29"] = {
        itemDBName = "WEAPON_PISTOL50",
        items = {
          [1] = {name = "placasaluminio", count = 7},
          [2] = {name = "placasmadeira", count = 5},
          [3] = {name = "corpo_pequeno", count = 1},
          [4] = {name = "cano_curto", count = 1},
          [5] = {name = "bp_pistol50", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_30"] = {
        itemDBName = "WEAPON_REVOLVER",
        items = {
          [1] = {name = "placasmadeira", count = 10},
          [2] = {name = "placasaluminio", count = 10},
          [3] = {name = "tambor", count = 1},
          [4] = {name = "corpo_pequeno", count = 1},
          [5] = {name = "cano_medio", count = 1},
          [6] = {name = "bp_revolver", count = 1},
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_31"] = {
        itemDBName = "WEAPON_DBSHOTGUN",
        items = {
          [1] = {name = "placasaluminio", count = 10},
          [2] = {name = "placasmadeira", count = 5},
          [3] = {name = "corpo_shotgun", count = 1},
          [4] = {name = "cano_shotgun", count = 2},
          [5] = {name = "bp_dbshotgun", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_32"] = {
        itemDBName = "WEAPON_MINISMG",
        items = {
          [1] = {name = "corpo_medio", count = 1},
          [2] = {name = "cano_medio", count = 1},
          [3] = {name = "bp_minismg", count = 1},
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_33"] = {
        itemDBName = "WEAPON_ASSAULTSMG",
        items = {
          [1] = {name = "placasmadeira", count = 5},
          [2] = {name = "placasaluminio", count = 5},
          [3] = {name = "corpo_medio", count = 1},
          [4] = {name = "cano_medio", count = 1},
          [5] = {name = "bp_assaultsmg", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_34"] = {
        itemDBName = "WEAPON_GUSENBERG",
        items = {
          [1] = {name = "placasmadeira", count = 10},
          [2] = {name = "placasaluminio", count = 7},
          [3] = {name = "corpo_medio", count = 1},
          [4] = {name = "cano_medio", count = 1},
          [5] = {name = "bp_gusenbergSweeper", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_35"] = {
        itemDBName = "WEAPON_MUSKET",
        items = {
          [1] = {name = "placasaluminio", count = 20},
          [2] = {name = "placasmadeira", count = 20},
          [3] = {name = "corpo_grande", count = 1},
          [4] = {name = "cano_longo", count = 1},
          [5] = {name = "bp_musket", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },
  ["item_all_36"] = {
        itemDBName = "WEAPON_ASSAULTRIFLE",
        items = {
          [1] = {name = "placasmadeira", count = 20},
          [2] = {name = "placasaluminio", count = 20},
          [3] = {name = "cano_longo", count = 1},
          [4] = {name = "corpo_grande", count = 1},
          [5] = {name = "bp_assaultrifle", count = 1},
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },

  ["item_all_37"] = {
        itemDBName = "hacking_laptop",
        items = {
          [1] = {name = "laptop", count = 1},
          [2] = {name = "raspberry", count = 1},
          [3] = false,
          [4] = false,
          [5] = false,
          [6] = false,
          [7] = false,
          [8] = false,
          [9] = false,
        }
      },

--- OUTROS ---
  ["item_all_38"] = {
        itemDBName = "advancedlockpick",
        items = {
        [1] = {name = "fragmentosmetal", count = 10},
        [2] = {name = "plastico", count = 2},
        [3] = {name = "mola", count = 2},
        [4] = {name = "lockpick", count = 1},
        [5] = false,
        [6] = false,
        [7] = false,
        [8] = false,
        [9] = false,
        }
      },
  ["item_all_39"] = {
    itemDBName = "armor",
    items = {
      [1] = {name = "fabric", count = 20},
      [2] = {name = "nailon", count = 20},
      [3] = false,
      [4] = false,
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
  ["item_all_40"] = {
    itemDBName = "lockpick",
    items = {
      [1] = {name = "gancho", count = 1},
      [2] = {name = "mola", count = 1},
      [3] = {name = "alicate", count = 1},
      [4] = false,
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
  ["item_all_41"] = {
    itemDBName = "sacodroga",
    items = {
      [1] = {name = "plastico", count = 1},
      [2] = false,
      [3] = false,
      [4] = false,
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
  ["item_all_42"] = {
    itemDBName = "c4_bank",
    items = {
      [1] = {name = "polvora", count = 15},
      [2] = false,
      [3] = false,
      [4] = false,
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
    ["item_all_43"] = {
    itemDBName = "clip",
    items = {
      [1] = {name = "aco", count = 3},
      [2] = {name = "mola", count = 2},
      [3] = {name = "polvora", count = 1},
      [4] = false,
      [5] = false,
      [6] = false,
      [7] = false,
      [8] = false,
      [9] = false,
    }
  },
--- ITENS ANTIGOS ----
--   ["item_2"] = {
--     itemDBName = "quimico",
--     items = {
--       [1] = {name = "fertilizantes", count = 2},
--       [2] = {name = "lixivia", count = 1},
--       [3] = {name = "regador", count = 1},
--       [4] = false,
--       [5] = {name = "bp_quimico", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_3"] = {
--     itemDBName = "balanca",
--     items = {
--       [1] = {name = "iron", count = 20},
--       [2] = {name = "plastico", count = 5},
--       [3] = {name = "ecra", count = 1},
--       [4] = false,
--       [5] = {name = "bp_balanca", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_4"] = {
--     itemDBName = "armor",
--     items = {
--       [1] = {name = "fabric", count = 60},
--       [2] = {name = "aco", count = 10},
--       [3] = {name = "nailon", count = 10},
--       [4] = false,
--       [5] = {name = "bp_kevlar", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_5"] = {
--     itemDBName = "clip",
--     items = {
--       [1] = {name = "plastico", count = 3},
--       [2] = {name = "aco", count = 2},
--       [3] = {name = "iron", count = 10},
--       [4] = false,
--       [5] = {name = "bp_clip", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_6"] = {
--     itemDBName = "weaponsuppressor",
--     items = {
--       [1] = {name = "fabric", count = 20},
--       [2] = {name = "aco", count = 5},
--       [3] = {name = "iron", count = 10},
--       [4] = false,
--       [5] = {name = "bp_suppressor", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_7"] = {
--     itemDBName = "weaponflashlight",
--     items = {
--       [1] = {name = "plastico", count = 3},
--       [2] = {name = "aco", count = 2},
--       [3] = {name = "iron", count = 10},
--       [4] = false,
--       [5] = {name = "bp_flashlight", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_8"] = {
--     itemDBName = "weaponluxary_finish",
--     items = {
--       [1] = {name = "lataspray", count = 1},
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = {name = "bp_skin", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },

--   ["item_10"] = {
--     itemDBName = "tuning_laptop",
--     items = {
--       [1] = {name = "motherboard", count = 1},
--       [2] = {name = "ecra", count = 1},
--       [3] = {name = "cpu", count = 1},
--       [4] = {name = "plastico", count = 20},
--       [5] = {name = "bp_tuning_laptop", count = 1},
--       [6] = {name = "cabos", count = 3},
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_11"] = {
--     itemDBName = "maquina_contar",
--     items = {
--       [1] = {name = "ecra", count = 1},
--       [2] = {name = "cpu", count = 1},
--       [3] = {name = "motherboard", count = 1},
--       [4] = {name = "iron", count = 50},
--       [5] = {name = "bp_maquina_contar", count = 1},
--       [6] = {name = "plastico", count = 20},
--       [7] = {name = "cabos", count = 3},
--       [8] = false,
--       [9] = false,
--     }
--   },
--   --- TODA GENTE PODE CRAFTAR A PARTIR DAQUI ---
--   ["item_all_1"] = {
--     itemDBName = "kevlar2",
--     items = {
--       [1] = {name = "fabric", count = 30},
--       [2] = {name = "aco", count = 5},
--       [3] = {name = "nailon", count = 5},
--       [4] =  false,
--       [5] = {name = "bp_kevlar2", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_2"] = {
--     itemDBName = "cano_arma",
--     items = {
--       [1] = {name = "iron", count = 20},
--       [2] = {name = "aco", count = 5},
--       [3] = false,
--       [4] = false,
--       [5] = {name = "bp_cano", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_3"] = {
--     itemDBName = "ferrolho_arma",
--     items = {
--       [1] = {name = "iron", count = 30},
--       [2] = {name = "mola", count = 2},
--       [3] = false,
--       [4] = false,
--       [5] = {name = "bp_ferrolho", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_4"] = {
--     itemDBName = "gatilho_arma",
--     items = {
--       [1] = {name = "aco", count = 5},
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = {name = "bp_gatilho", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_5"] = {
--     itemDBName = "motherboard",
--     items = {
--       [1] = {name = "iphone", count = 3},
--       [2] = {name = "cabos", count = 3},
--       [3] = {name = "estanho", count = 5},
--       [4] = false,
--       [5] = {name = "bp_motherboard", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_6"] = {
--     itemDBName = "ecra",
--     items = {
--       [1] = {name = "laptop", count = 2},
--       [2] = {name = "lockpick", count = 1},
--       [3] = false,
--       [4] = false,
--       [5] = {name = "bp_ecra", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_7"] = {
--     itemDBName = "cpu",
--     items = {
--       [1] = {name = "gold", count = 10},
--       [2] = {name = "estanho", count = 3},
--       [3] = {name = "plastico", count = 5},
--       [4] = false,
--       [5] = {name = "bp_cpu", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_8"] = {
--     itemDBName = "mola",
--     items = {
--       [1] = {name = "molaenferrujada", count = 1},
--       [2] = {name = "limador", count = 1},
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },

--   ["item_all_10"] = {
--     itemDBName = "placasmadeira",
--     items = {
--       [1] = {name = "cutted_wood", count = 100},
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_11"] = {
--     itemDBName = "placasferro",
--     items = {
--       [1] = {name = "iron", count = 100},
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_12"] = {
--     itemDBName = "fabric",
--     items = {
--       [1] = {name = "wool", count = 20},
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_13"] = {
--     itemDBName = "ecra",
--     items = {
--       [1] = {name = "laptop", count = 2},
--       [2] = {name = "advancedlockpick", count = 1},
--       [3] = false,
--       [4] = false,
--       [5] = {name = "bp_ecra", count = 1},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_14"] = {
--     itemDBName = "plastico",
--     items = {
--       [1] = {name = "lixo", count = 250},
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_15"] = {
--     itemDBName = "fertilizantes",
--     items = {
--       [1] = false,
--       [2] = {name = "lixo", count = 1000},
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_16"] = {
--     itemDBName = "lixivia",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = {name = "lixo", count = 1150},
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_17"] = {
--     itemDBName = "nailon",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = false,
--       [4] = {name = "lixo", count = 1200},
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_18"] = {
--     itemDBName = "cabos",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = {name = "lixo", count = 1300},
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_19"] = {
--     itemDBName = "regador",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = {name = "lixo", count = 1450},
--       [7] = false,
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_20"] = {
--     itemDBName = "molaenferrujada",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = {name = "lixo", count = 1500},
--       [8] = false,
--       [9] = false,
--     }
--   },
--   ["item_all_21"] = {
--     itemDBName = "estanho",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = {name = "lixo", count = 1500},
--       [9] = false,
--     }
--   },
--   ["item_all_22"] = {
--     itemDBName = "aco",
--     items = {
--       [1] = false,
--       [2] = false,
--       [3] = false,
--       [4] = false,
--       [5] = false,
--       [6] = false,
--       [7] = false,
--       [8] = false,
--       [9] = {name = "lixo", count = 1800},
--     }
--   },
-- }
}
KeepItems = {
---- KEEP ITENS ----
  ["item_all_1"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_2"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_3"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_4"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_5"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_6"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_7"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_8"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_9"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_10"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_11"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_12"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_13"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_14"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_15"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_16"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_17"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_18"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_19"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_20"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_21"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_22"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_23"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_24"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_25"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_26"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_27"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_28"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_29"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_30"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_31"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_32"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_33"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_34"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_35"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_36"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_37"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_38"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_39"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_40"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_41"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_42"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
  ["item_all_43"] = {
    false,         false,      false,
    false,         false,      false,
    false,         false,      false,
  },
}


RecipeRewards = {
  ["item_all_1"] = 1,
  ["item_all_2"] = 1,
  ["item_all_3"] = 1,
  ["item_all_4"] = 1,
  ["item_all_5"] = 1,
  ["item_all_6"] = 1,
  ["item_all_7"] = 1,
  ["item_all_8"] = 1,
  ["item_all_9"] = 1,
  ["item_all_10"] = 1,
  ["item_all_11"] = 1,
  ["item_all_12"] = 1,
  ["item_all_13"] = 1,
  ["item_all_14"] = 1,
  ["item_all_15"] = 1,
  ["item_all_16"] = 2,
  ["item_all_17"] = 1,
  ["item_all_18"] = 1,
  ["item_all_19"] = 1,
  ["item_all_20"] = 1,
  ["item_all_21"] = 1,
  ["item_all_22"] = 1,
  ["item_all_23"] = 1,
  ["item_all_24"] = 1,
  ["item_all_25"] = 1,
  ["item_all_26"] = 1,
  ["item_all_27"] = 1,
  ["item_all_28"] = 1,
  ["item_all_29"] = 1,
  ["item_all_30"] = 1,
  ["item_all_31"] = 1,
  ["item_all_32"] = 1,
  ["item_all_33"] = 1,
  ["item_all_34"] = 1,
  ["item_all_35"] = 1,
  ["item_all_36"] = 1,
  ["item_all_37"] = 1,
  ["item_all_38"] = 1,
  ["item_all_39"] = 1,
  ["item_all_40"] = 1,
  ["item_all_41"] = 2,
  ["item_all_42"] = 1,
  ["item_all_43"] = 1,
}

CraftTime = {
  -- ["weapon_pistol_1"] = 15.0,
  -- ["weapon_pistol_2"] = 20.0,
  -- ["weapon_pistol_3"] = 24.0,
  -- ["weapon_pistol_4"] = 30.0,
  -- ["weapon_pistol_5"] = 30.0,
  -- ["weapon_micros_1"] = 35.0,
  -- ["weapon_micros_2"] = 30.0,
  -- ["weapon_shotgun_1"] = 30.0,
  -- ["weapon_shotgun_2"] = 40.0,
  -- ["weapon_pesadas_1"] = 40.0,
  -- ["weapon_pesadas_2"] = 40.0,
  -- ["weapon_pesadas_3"] = 50.0,
  -- ["weapon_pesadas_4"] = 60.0,
  -- ["item_1"] = 15.0,
  -- ["item_2"] = 20.0,
  -- ["item_3"] = 30,
  -- ["item_4"] = 20,
  -- ["item_5"] = 5,
  -- ["item_6"] = 10,
  -- ["item_7"] = 10,
  -- ["item_8"] = 10,
  -- ["item_9"] = 10,
  -- ["item_10"] = 120,
  -- ["item_11"] = 60,
  ["item_all_1"] = 15,
  ["item_all_2"] = 15,
  ["item_all_3"] = 15,
  ["item_all_4"] = 15,
  ["item_all_5"] = 20,
  ["item_all_6"] = 20,
  ["item_all_7"] = 20,
  ["item_all_8"] = 20,
  ["item_all_9"] = 20,
  ["item_all_10"] = 20,
  ["item_all_11"] = 20,
  ["item_all_12"] = 20,
  ["item_all_13"] = 20,
  ["item_all_14"] = 20,
  ["item_all_15"] = 25,
  ["item_all_16"] = 25,
  ["item_all_17"] = 30,
  ["item_all_18"] = 30,
  ["item_all_19"] = 30,
  ["item_all_20"] = 20,
  ["item_all_21"] = 20,
  ["item_all_22"] = 30,
  ["item_all_23"] = 20,
  ["item_all_24"] = 20,
  ["item_all_25"] = 25,
  ["item_all_26"] = 30,
  ["item_all_27"] = 20,
  ["item_all_28"] = 40,
  ["item_all_29"] = 40,
  ["item_all_30"] = 60,
  ["item_all_31"] = 60,
  ["item_all_32"] = 20,
  ["item_all_33"] = 20,
  ["item_all_34"] = 15,
  ["item_all_35"] = 20,
  ["item_all_36"] = 10,
  ["item_all_37"] = 10,
  ["item_all_38"] = 25,
  ["item_all_39"] = 20,
  ["item_all_40"] = 10,
  ["item_all_41"] = 10,
  ["item_all_42"] = 20,
  ["item_all_43"] = 15,
}

bpDestroyable = {

}

bpItemsAndTypes = {
 
}
