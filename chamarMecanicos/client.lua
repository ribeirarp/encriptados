ESX = nil
local pos_before_assist,assisting,assist_target,last_assist = nil, false, nil, nil
local PlayerJob = nil
currentBlip = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerJob = job.name
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerJob = xPlayer.job.name
end)

function CreateBlipAt(x, y, z)
	
	
	tmpBlip = AddBlipForCoord(x,y,z)
	SetBlipRoute(tmpBlip, true)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Destino")
	EndTextCommandSetBlipName(tmpBlip)
	return tmpBlip
end


RegisterNetEvent("chamarMecanicos:acceptedAssist")
AddEventHandler("chamarMecanicos:acceptedAssist",function(t)
	if assisting then return end
	local ped = GetPlayerPed(-1)
	pos_before_assist = GetEntityCoords(ped)
	assisting = true
	assist_target = t
	DeleteWaypoint()
	SetNewWaypoint(t.x, t.y)
end)

RegisterNetEvent("chamarMecanicos:assistDone")
AddEventHandler("chamarMecanicos:assistDone",function()
	if assisting then
		assisting = false
		DeleteWaypoint()
		SetWaypointOff()
		assist_target = nil
	end
end)

RegisterNetEvent("chamarMecanicos:newWaypoint")
AddEventHandler("chamarMecanicos:newWaypoint",function()
	if assisting then
		DeleteWaypoint()
		SetNewWaypoint(assist_target.x, assist_target.y)
	end
end)

if Config.assist_keys then
	Citizen.CreateThread(function()
		while true do
			Citizen.Wait(6)
			if IsControlJustPressed(0, Config.assist_keys.accept) and PlayerJob == Config.acceptCallsJob then
				TriggerServerEvent("chamarMecanicos:acceptAssistKey")
			end
			if IsControlJustPressed(0, Config.assist_keys.finalizar) and PlayerJob == Config.acceptCallsJob then
				TriggerServerEvent("chamarMecanicos:finalizarKey")
			end
		end
	end)
end

Citizen.CreateThread(function()
	while true do
		 Citizen.Wait(30000)
		 collectgarbage()
	end
end)