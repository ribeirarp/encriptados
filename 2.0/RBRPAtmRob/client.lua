local holdingup = false



ESX = nil

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end
end)

RegisterNetEvent('RBRPAtmRob:getClosestAtm')
AddEventHandler('RBRPAtmRob:getClosestAtm', function()
    local ped = GetPlayerPed(-1)
    local coordsplayer = GetEntityCoords(ped)

    for i, atm in pairs(Config.atms) do
        if (#((vector3(atm.x, atm.y, atm.z)) - coordsplayer)) < 2 then
            TriggerServerEvent('RBRPAtmRob:start', i)
            break
        end
    end

end)


RegisterNetEvent('RBRPAtmRob:start')
AddEventHandler('RBRPAtmRob:start', function()
    loadAnimDict('anim@heists@prison_heiststation@cop_reactions')
    TaskPlayAnim(GetPlayerPed(-1), 'anim@heists@prison_heiststation@cop_reactions', 'cop_b_idle', 5.0, 1.0, 1.0, 1, 0.0, 0, 0, 0)
    exports['progressBars']:startUI(3000, "A iniciar hacking!")
    Citizen.Wait(3000)

    exports["memorygame"]:thermiteminigame(Config.correctBlocks, Config.incorrectBlocks, Config.timetoShow,
        Config.timetoLose, function() -- success
            exports["memorygame"]:thermiteminigame(Config.correctBlocks, Config.incorrectBlocks,
                Config.timetoShow, Config.timetoLose, function() -- success
                    exports["memorygame"]:thermiteminigame(Config.correctBlocks, Config.incorrectBlocks, Config.timetoShow, Config.timetoLose, function() -- success
                            -------------------SUCESSO-------------------
                            ClearPedTasks(GetPlayerPed(-1))
                        --    Citizen.Wait(1000)

                            TriggerServerEvent("RBRPAtmRob:warnPolice")

                            FreezeEntityPosition(GetPlayerPed(-1), true)
                            TriggerEvent('esx_blowtorch:startblowtorch')
                            exports['progressBars']:startUI(6000, "A rebentar a ATM!")
                            Citizen.Wait(6000)
                            FreezeEntityPosition(GetPlayerPed(-1), false)
                            TriggerEvent('esx_blowtorch:finishclear')
                            TriggerEvent('esx_blowtorch:stopblowtorching')
                            
                            ClearPedTasks(GetPlayerPed(-1))
                            Citizen.Wait(1000)
            
                            animCaixa()
                            TriggerServerEvent("RBRPAtmRob:hmmhmhm")
                            exports['mythic_notify']:SendAlert('success', 'Assalto com sucesso, foge oh artista!', 4000)

                            -------------------FIM SUCESSO-------------------
                        end, function() -- failure
                            exports['mythic_notify']:SendAlert('error', "Falhaste.", 2000)
                            TriggerServerEvent("RBRPAtmRob:warnPolice")
                        end)
                end, function() -- failure
                    exports['mythic_notify']:SendAlert('error', "Falhaste.", 2000)
                    TriggerServerEvent("RBRPAtmRob:warnPolice")
                end)
        end, function() -- failure
            exports['mythic_notify']:SendAlert('error', "Falhaste.", 2000)
            TriggerServerEvent("RBRPAtmRob:warnPolice")
    end)
    ClearPedTasks(GetPlayerPed(-1))

end)

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(0)
    end
end

function animCaixa()
    RequestAnimDict('anim@heists@ornate_bank@grab_cash_heels')
    while not HasAnimDictLoaded('anim@heists@ornate_bank@grab_cash_heels') do
        Citizen.Wait(50)
    end
    local PedCoords = GetEntityCoords(GetPlayerPed(-1))
    torba = CreateObject(GetHashKey('prop_cs_heist_bag_02'),PedCoords.x, PedCoords.y,PedCoords.z, true, true, true)
    AttachEntityToEntity(torba, GetPlayerPed(-1), GetPedBoneIndex(GetPlayerPed(-1), 57005), 0.0, 0.0, -0.16, 250.0, -30.0, 0.0, false, false, false, false, 2, true)
    TaskPlayAnim(PlayerPedId(), "anim@heists@ornate_bank@grab_cash_heels", "grab", 8.0, -8.0, -1, 1, 0, false, false, false)
    FreezeEntityPosition(GetPlayerPed(-1), true)
    Citizen.Wait(10000)
    DeleteEntity(torba)
    ClearPedTasks(GetPlayerPed(-1))
    FreezeEntityPosition(GetPlayerPed(-1), false)
    SetPedComponentVariation(GetPlayerPed(-1), 5, 45, 0, 2)
    Citizen.Wait(1000)
end
