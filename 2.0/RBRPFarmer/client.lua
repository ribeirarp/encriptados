local ESX = nil
local data = {
	started = false,

	player = {
		ped = nil,
		coords = nil,
		anim = {
			isPlaying = false,
			dict = nil,
			anim = nil
		},
		vehicle = {
			isInside = false,
			entity = nil
		}
	},
	object = {
		id = nil,
		entity = nil,
		inHand = false
	},

	animation = {
		playingAnim = false,
		dict = nil,
		anim = nil
	}
}


local createdObj = nil
local hasEntityInHand = false
local plants = {}
local isPlayingAnim = false

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
  

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(10)
    end


	while not data.started do
		data.started = true
		if data.player.ped == nil then
			data.started = false
		end


		if data.player.coords == nil then
			data.started = false
		end


		Citizen.Wait(1000)
	end

	exports['bt-target']:AddBoxZone("AgricultorFilipe",
        Config.NPC.coords, 0.8, 0.4, {
            name = "Agricultor Filipe",
            heading = Config.NPC.heading,
            debugPoly = false,
            minZ = Config.NPC.coords.z - 3,
            maxZ = Config.NPC.coords.z + 3
        }, {
            options = {{
                event = "farmeR:talkWithNPC",
                icon = "fas fa-clipboard",
                label = "Falar com o Sr Filipe"
            }},
            job = {"all"},
            distance = 2.0
	})
end)



local cooldown = false
RegisterNetEvent('farmer:talkWithNPC')
AddEventHandler('farmeR:talkWithNPC', function()
	if not cooldown then
		cooldown = true
		TriggerEvent('luke_textui:ShowUI', Config.NPC.informations[math.random(0, #Config.NPC.informations)])
		Citizen.Wait(5000)
		TriggerEvent('luke_textui:HideUI')
		SetTimeout(Config.NPC.timeout, function()
			cooldown = false
		end)
	else
		TriggerEvent('luke_textui:ShowUI', 'Agora pera, tou a ver o campo')
		Citizen.Wait(5000)
		TriggerEvent('luke_textui:HideUI')
	end
end)

Citizen.CreateThread(function()
    while true do
        local sleep = 5000
        -- Coisas que não precisam de correr muito frequentemente
        data.player.ped = GetPlayerPed(-1)
        Citizen.Wait(sleep)
    end
end)

Citizen.CreateThread(function()
    while true do
        local sleep = 1000
		if data.player.ped ~= nil then
			local vehicle = GetVehiclePedIsIn(data.player.ped, false)

			-- Player Coords

			local coords = GetEntityCoords(data.player.ped)

			data.player.coords = coords

			-- Vehicle

			if vehicle ~= 0 then
				data.player.vehicle = {
					isInside = true,
					entity = vehicle
				}
			elseif vehicle == 0 and data.player.vehicle ~= nil and data.player.vehicle.entity ~= nil then
				data.player.vehicle = {
					isInside = false,
					entity = nil
				}
			end
		end

		Citizen.Wait(sleep)
    end
end)






local shovelid = nil
local spatulamodel = "bkr_prop_coke_spatula_04"

function animPlant(sleep)
	FreezeEntityPosition(data.player.ped, true)
	TaskStartScenarioInPlace(PlayerPedId(), "world_human_gardener_plant", 0, false)
	local shovelid = CreateObject(GetHashKey(spatulamodel), data.player.coords.x, data.player.coords.y, data.player.coords.z, 1, 1, 1)
	AttachEntityToEntity(shovelid,GetPlayerPed(PlayerId()),GetPedBoneIndex(GetPlayerPed(PlayerId()), 28422),-0.005,0.0,0.0,190.0,190.0,-50.0,1,1,0,1,0,1)
	Citizen.Wait(sleep)
	ClearPedTasks(PlayerPedId())
	DeleteEntity(shovelid)
	FreezeEntityPosition(data.player.ped, false)
end

-- RegisterCommand('anim',function(source,args)
-- 	loadAnimDict(args[1])
-- 	TaskPlayAnim(GetPlayerPed(-1),args[1],args[2],4.0, 1.0, -1,49,0, 0, 0, 0)
	
-- end)


local planting = false

RegisterNetEvent('farmer:plant')
AddEventHandler('farmer:plant', function(plantData)
	if not data.player.vehicle.isInside then
		local insideFarmingArea = isInsideFarmingArea()

		while insideFarmingArea == nil do
			Citizen.Wait(10)
		end
	
		if insideFarmingArea then
			if not planting then
				local menosDistancia = 50
				for iplant, plant in pairs(plants) do
					if #(plant.coords - data.player.coords) <= menosDistancia then
						menosDistancia = #(plant.coords - data.player.coords)
					end
				end
				if menosDistancia > 2.0 then
					planting = true
					TriggerServerEvent('farmer:removeItem', plantData.removeItem, 1)
					animPlant(14000)
					local model = plantData.model
					local entitycoords = vector3(GetEntityCoords(GetPlayerPed(-1)))-vector3(0,0,2)
					RequestModel(model)
					while not HasModelLoaded(model) do
						Citizen.Wait(0)
					end
					createdObj = CreateObject(model, entitycoords, 1, 1, 0)
					SetEntityCoords(createdObj, entitycoords)
					plantid = #plants+1
					plants[plantid] = {
						svData = plantData,
						show = true,
						entity = createdObj, 
						coords = entitycoords,
						finalCoords = entitycoords+vector3(0,0,1),
						
						data = {
							status = {
								dead = false,
								readyToHarvest = false
							},
				
							infection = {
								isInfected = false,
								progress = 0,
							},
				
							quality = 100,
							currentcycle = 0,
							progress = 0,
							water = 100,
							fertilizer = 100,
						}
					}
					planting = false
					trackplant(plantid)
				else
					exports['mythic_notify']:SendAlert('inform', 'Outra planta está muito perto')
				end
			else
				exports['mythic_notify']:SendAlert('inform', 'Já estás a plantar')
			end
			
		else
			exports['mythic_notify']:SendAlert('inform', 'Não podes plantar aqui')
		end
	else
		exports['mythic_notify']:SendAlert('inform', 'Não podes plantar nada se estás em um veículo')
	end
end)

function round(num, numDecimalPlaces)
	local mult = 10^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end

function trackplant(plantid)
	local secondspassed = 0
	local progress = 0
	while secondspassed <= 360 and not plants[plantid].data.status.dead do
		local sleep = 1000 
		progress = round((secondspassed/360)*100, 2)
		
		if secondspassed % 40 == 0 and plants[plantid].data.fertilizer > 0 then
			newcoords = plants[plantid].coords+vector3(0,0,0.1)
			SetEntityCoords(plants[plantid].entity, newcoords)
			plants[plantid].coords = newcoords
		end

		if secondspassed % 4 == 0 and secondspassed > 0 and plants[plantid].data.water == 0 and plants[plantid].data.quality > 0 then
			plants[plantid].data.quality = plants[plantid].data.quality - 5
		end

		if secondspassed % 4 == 0 and secondspassed > 0 and plants[plantid].data.water > 0 then
			plants[plantid].data.water = plants[plantid].data.water - 5
		end

		if secondspassed % 7 == 0 and secondspassed > 0 and plants[plantid].data.fertilizer > 0 then
			plants[plantid].data.fertilizer = plants[plantid].data.fertilizer - 5
		end

		if plants[plantid].data.fertilizer > 0 then
			plants[plantid].data.progress = progress
			secondspassed = secondspassed + (sleep/1000)
		--else


			-- deteriora a planta  
			-- if plants[plantid].data.quality  >= 5 then
			-- 	plants[plantid].data.quality = plants[plantid].data.quality - 5
			-- end
		end


		if plants[plantid].data.quality == 0 then
			plants[plantid].data.status.dead = true
			break
		end

		Citizen.Wait(sleep)
	end

	if secondspassed >= 360 then
		if not plants[plantid].data.status.dead then
			plants[plantid].data.status.readyToHarvest = true
		end
	end

end


function isInsideFarmingArea()
	local coords = data.player.coords
	local valid = false
	for x,y in pairs(Config.Farmlands) do
		if coords.x <= y.x1 and coords.x >= y.x3 and coords.y >= y.y2 and coords.y <= y.y4 then
			valid = true
			break
		end

	end

	if valid then
		return true
	else
		return false
	end
end





Citizen.CreateThread(function()
    local lastIsPlayingAnim = nil
    while true do
        local sleep = 200
        if data.started then
            if data.player.anim.isPlaying then
				SetCurrentPedWeapon(data.player.ped,GetHashKey("WEAPON_UNARMED"),true)
                if not IsEntityPlayingAnim(data.player.ped,data.player.anim.dict,data.player.anim.anim,3) then
					loadAnimDict(data.player.anim.dict)
					TaskPlayAnim(data.player.ped,data.player.anim.dict,data.player.anim.anim,4.0, 1.0, -1,49,0, 0, 0, 0)
                end
                sleep = 50
            end
    
            if lastIsPlayingAnim == true and lastIsPlayingAnim ~= data.player.anim.isPlaying then
                    ClearPedTasks(data.player.ped)
            end
    
            lastIsPlayingAnim = data.player.anim.isPlaying
        end

        Citizen.Wait(sleep)
    end
end)

local isFertilizing = false
local isWatering = false
function putFertilizer(plantToGive)
	if plants[plantToGive].data.fertilizer == 100 then
		exports['mythic_notify']:SendAlert('inform', 'Não precisas de fertilizar por agora')
	else
		isFertilizing = true
		FreezeEntityPosition(data.player.ped, true)
		data.player.anim.dict = 'missarmenian3_gardener'
		data.player.anim.anim = 'idle_a' 
		data.player.anim.isPlaying = true
		

		if plants[plantToGive].data.fertilizer+30 > 100 then
			plants[plantToGive].data.fertilizer = 100
		else
			plants[plantToGive].data.fertilizer = plants[plantToGive].data.fertilizer + 30
		end

		TriggerServerEvent('farmer:removeItem', 'fertilizantes', 1)
		Citizen.Wait(2500)
		FreezeEntityPosition(data.player.ped, false)
		ClearPedTasks(data.player.ped)
		data.player.anim.isPlaying = false
		data.player.anim.dict = nil
		data.player.anim.anim = nil
		isFertilizing = false
	end
end




RegisterNetEvent('farmer:putFertilizer')
AddEventHandler('farmer:putFertilizer', function()
	if not data.player.vehicle.isInside then
		local menosDistancia = 50
		local plantToGive = nil
		for iplant, plant in pairs(plants) do
			if #(plant.coords - data.player.coords) <= menosDistancia then
				menosDistancia = #(plant.coords - data.player.coords)
				plantToGive = iplant
			end
		end
	
		if menosDistancia <= 2 then
			if not isFertilizing then
				if not isWatering then
					putFertilizer(plantToGive)
				else
					exports['mythic_notify']:SendAlert('inform', 'Ainda não és um polvo')
				end
			else
				exports['mythic_notify']:SendAlert('inform', 'Já estás a colocar fertilizante')
	
			end
		else
			exports['mythic_notify']:SendAlert('inform', 'Estás muito longe para fertilizar')
		end
	else
		exports['mythic_notify']:SendAlert('inform', 'Não podes fertilizar se estás em um veículo')
	end
end)


RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    exports.ft_libs:AddBlip("farmer:blip", {
		x = 253.81,
		y = 6454.17,
		z = 31.39,
		imageId = 514,
		colorId = 5,
		scale = 0.8,
		text = "Quinta do Filipe",
	})
end)


function putWater(plantToGive)
	if plants[plantToGive].data.water == 100 then
		exports['mythic_notify']:SendAlert('inform', 'Não precisas de regar por agora')
	else
		isWatering = true
		FreezeEntityPosition(data.player.ped, true)
		data.player.anim.dict = 'missarmenian3_gardener'
		data.player.anim.anim = 'idle_a' 
		data.player.anim.isPlaying = true
		

		if plants[plantToGive].data.water+30 > 100 then
			plants[plantToGive].data.water = 100
		else
			plants[plantToGive].data.water = plants[plantToGive].data.water + 30
		end

		TriggerServerEvent('farmer:removeItem', 'water', 1)
		Citizen.Wait(2500)
		FreezeEntityPosition(data.player.ped, false)
		ClearPedTasks(data.player.ped)
		data.player.anim.isPlaying = false
		data.player.anim.dict = nil
		data.player.anim.anim = nil
		isWatering = false
	end
	
end

local empacotar = false
local didsomething = false
RegisterNetEvent('farmer:empacotar')
AddEventHandler('farmer:empacotar', function(identifier)
	if not empacotar then
		exports["mf-inventory"]:getInventoryItems(identifier,function(items)
			for iitem, item in pairs(items) do
				if Config.empacotar[item.name] ~= nil then
					if item.count >= Config.empacotar[item.name].removeQtt then
						didsomething = true
						empacotar = true
						data.player.anim.dict = 'amb@prop_human_bum_bin@base'
						data.player.anim.anim = 'base' 
						data.player.anim.isPlaying = true
						TriggerServerEvent('farmer:removeItem', item.name, Config.empacotar[item.name].removeQtt)
						TriggerServerEvent('farmer:removeItem', 'caixa', 1)
						FreezeEntityPosition(data.player.ped, true)
						exports['progressBars']:startUI(5200, "A empacotar...")
						Citizen.Wait(5000)
						
	
	
						FreezeEntityPosition(data.player.ped, false)
						ClearPedTasks(data.player.ped)
						data.player.anim.isPlaying = false
						data.player.anim.dict = nil
						data.player.anim.anim = nil
						TriggerServerEvent('farmer:giveItem', {item = Config.empacotar[item.name].giveItemName, count = Config.empacotar[item.name].giveItemCount})
						empacotar = false
						break
					end
				end
			end

			if didsomething then
				didsomething = false
			else
				exports['mythic_notify']:SendAlert('error', 'Não tens nada para empacotar')
			end
		end)
	else
		exports['mythic_notify']:SendAlert('error', 'Já estás a empacotar algo')
	end
end)


RegisterNetEvent('farmer:putWater')
AddEventHandler('farmer:putWater', function(identifier)
	if not data.player.vehicle.isInside then
		exports["mf-inventory"]:getInventoryItems(identifier,function(items)
			local water = nil
			for iitem,item in pairs(items) do
				if item.name == 'water' then
					water = item
					break
				end
			end

			if water ~= nil then
				if water.count >= 1 then
					local menosDistancia = 50
					local plantToGive = nil
					for iplant, plant in pairs(plants) do
						if #(plant.coords - data.player.coords) <= menosDistancia then
							menosDistancia = #(plant.coords - data.player.coords)
							plantToGive = iplant
						end
					end
					
					if menosDistancia <= 2 then
						if not isWatering then
							if not isFertilizing then
								putWater(plantToGive)
							else
								exports['mythic_notify']:SendAlert('inform', 'Ainda não és um polvo')
							end
						else
							exports['mythic_notify']:SendAlert('inform', 'Já estás a regar')
						end
					else
						exports['mythic_notify']:SendAlert('inform', 'Estás muito longe para regar')
					end
				else
					exports['mythic_notify']:SendAlert('inform', 'Não tens água')
				end
			else
				exports['mythic_notify']:SendAlert('inform', 'Não tens água')
			end
			
		end)
	else
		exports['mythic_notify']:SendAlert('inform', 'Não consegues regar nada em um veículo')
	end
	
end)

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(0)
    end
end

function removePlant(plantid)
	DeleteEntity(plants[plantid].entity)
	plants[plantid] = nil
end

function giveItems(plantData, quality)
	TriggerServerEvent('farmer:giveItems', plantData, quality)
end

DrawText3D = function(x, y, z, text)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)
 
    local scale = (1/dist)*2
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
   
    if onScreen then
        SetTextScale(0.0*scale, 0.2*scale)
        SetTextFont(0)
        SetTextProportional(1)
        -- SetTextScale(0.0, 0.55)
        SetTextColour(255, 255, 255, 255)
        SetTextDropshadow(0, 0, 0, 0, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end



Citizen.CreateThread(function()
	while true do
		local sleep = 1000
		if #plants > 0 then
			for iplant, plant in pairs(plants) do
				local plantdistance = #(plant.coords - data.player.coords)
				if plantdistance < 5 and plant.show then
					sleep = 1
					
					text = 'Água ' .. plant.data.water .. ' \nAdubo ' .. plant.data.fertilizer .. "\nProgresso " .. plant.data.progress .. "%" 
					
					if plant.data.status.dead then
						text = "\n[E] para tirar a planta morta"
					elseif not plant.data.status.dead and not plant.data.status.readyToHarvest then
						text = text .. "\nQualidade " .. plant.data.quality
					else
						text = "\n[E] para colher a planta"
					end
					
					if plantdistance <= 2 and (plant.data.status.dead or plant.data.status.readyToHarvest) then
						if IsControlJustPressed(0, Keys['E']) and not data.player.vehicle.isInside then
							plants[iplant].show = false
							if plant.data.status.dead then
								animPlant(14000)
								removePlant(iplant)
							
							elseif plant.data.status.readyToHarvest then
								animPlant(14000)
								giveItems(plant.svData, plant.data.quality)
								removePlant(iplant)
							end
							
						end
					end
					

					DrawText3D(plant.finalCoords.x, plant.finalCoords.y, plant.finalCoords.z + 1, text)
				end
			end
		end
		Citizen.Wait(sleep)
	end
end)

local npcspawned = false
local npc = nil

function createNPC(coords, heading)
	local hash = GetHashKey('a_m_m_farmer_01')
    if not HasModelLoaded(hash) then
        RequestModel(hash)
        Wait(10)
    end
    while not HasModelLoaded(hash) do
        Wait(10)
    end
    
    npcspawned = true
    npc = CreatePed(5, hash, coords, heading, false, false)
    FreezeEntityPosition(npc, true)
    SetEntityInvincible(npc, true)
    SetBlockingOfNonTemporaryEvents(npc, true)
end


Citizen.CreateThread(function()
    while true do
		if data.started then
			local dst = #(Config.NPC.coords - data.player.coords)
			if dst < 200 and npcspawned == false then
				createNPC(Config.NPC.coords, Config.NPC.heading)
				npcspawned = true
			end
			if dst >= 201 and npcspawned then
				npcspawned = false
				DeleteEntity(npc)
			end
		end
        Citizen.Wait(3000)
    end
end)

function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
	if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end
