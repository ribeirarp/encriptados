local ESX = nil
local data = {
	started = false,
	
	player = {
		ped = nil,
		coords = nil,
		vehicle = {
			isInside = false,
			entity = nil
		},

		anim = {
			isPlaying = false,
			dict = nil,
			anim = nil,

			options = {
				weapon = {
					setPedWeapon = false,
					weaponHash = nil,
				},

				freezePosition = false,
			},

			blockKeys = {},
		},

        walking = {
            defaultWalk = function(ped)
                ResetPedMovementClipset(ped)
            end,

            useCustomWalk = false,
            customWalk = ''
        }
	},
	
}

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


local function loadModel(model)
	RequestModel(model)
	while not HasModelLoaded(model) do
		RequestModel(model)
		Citizen.Wait(10)                
	end
end

local function loadAnimDict(dict)
	RequestAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(10)
    end
end

local function RequestWalking(set)
    RequestAnimSet(set)
    while not HasAnimSetLoaded(set) do
      Citizen.Wait(1)
    end 
end

local function WalkStart(walk, ped)
    RequestWalking(walk)
    SetPedMovementClipset(ped, walk, 0.2)
    RemoveAnimSet(walk)
end
  

  


Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(10)
    end


	while not data.started do
		data.started = true
		if data.player.ped == nil then
			data.started = false
		end


		if data.player.coords == nil then
			data.started = false
		end

		Citizen.Wait(1000)
	end
end)


Citizen.CreateThread(function()
    while true do
        -- Coisas que não precisam de correr muito frequentemente
        data.player.ped = GetPlayerPed(-1)
        Citizen.Wait(5000)
    end
end)

Citizen.CreateThread(function()
    while true do
		if data.player.ped ~= nil then
			local vehicle = GetVehiclePedIsIn(data.player.ped, false)

			-- Player Coords

			local coords = GetEntityCoords(data.player.ped)

			data.player.coords = coords

			-- Vehicle

			if vehicle ~= 0 then
				data.player.vehicle = {
					isInside = true,
					entity = vehicle
				}
			elseif vehicle == 0 and data.player.vehicle ~= nil and data.player.vehicle.entity ~= nil then
				data.player.vehicle = {
					isInside = false,
					entity = nil
				}
			end
		end

		Citizen.Wait(1000)
    end
end)

exports('getCoords', function()
	return data.player.coords
end)

exports('playAnim', function(animData)
	if animData.animDetails ~= nil then
		data.player.anim.isPlaying = true
		data.player.anim.dict = animData.animDetails.dict
		data.player.anim.anim = animData.animDetails.anim

		if animData.options.freezePosition then
			FreezeEntityPosition(data.player.ped, true)
		end

		if animData.progressBar ~= nil then
			if animData.progressBar.activate then
				exports["progressBars"]:startUI(animData.progressBar.duration, animData.progressBar.text);
			end
		end
		
		Citizen.Wait(animData.animDetails.duration)


		data.player.anim.isPlaying = false
		data.player.anim.dict = nil
		data.player.anim.anim = nil

		if animData.options.freezePosition then
			FreezeEntityPosition(data.player.ped, false)
		end

		

	else
		print("Couldn't start animation")
	end
end)
  

exports('useCustomWalk', function(walkData)
    --[[
        walkData = {
            walk = ''
        }
    ]]

    if walkData.walk ~= nil then
        data.player.walking.useCustomWalk = true
        data.player.walking.customWalk = walkData.walk
        WalkStart(walkData.walk, data.player.ped)
    else
        print("No Walk passed as argument")
    end
end)
  

exports('cancelWalk', function()
    data.player.walking.useCustomWalk = false
    data.player.walking.customWalk = ''
    data.player.walking.defaultWalk(data.player.ped)
end)

Citizen.CreateThread(function()
    local lastIsPlayingAnim = nil
    while true do
        local sleep = 500
        if data.started then
            if data.player.anim.isPlaying then
				if data.player.anim.options.weapon.setPedWeapon then
					SetCurrentPedWeapon(data.player.ped, data.player.anim.options.weapon.weaponHash,true)
				end

                if not IsEntityPlayingAnim(data.player.ped,data.player.anim.dict,data.player.anim.anim,3) then
					loadAnimDict(data.player.anim.dict)
					TaskPlayAnim(data.player.ped,data.player.anim.dict,data.player.anim.anim,4.0, 1.0, -1,49,0, 0, 0, 0)
                end

                sleep = 50
            end
    
            if lastIsPlayingAnim == true and lastIsPlayingAnim ~= data.player.anim.isPlaying then
				ClearPedTasks(data.player.ped)
            end
    
            lastIsPlayingAnim = data.player.anim.isPlaying
         
        end

        Citizen.Wait(sleep)
    end
end)



Citizen.CreateThread(function()
    local lastUseCustomWalk = nil
    while true do
        local sleep = 5000
        if data.started then
            if data.player.walking.useCustomWalk then
                WalkStart(data.player.walking.customWalk, data.player.ped)
            end

            if lastUseCustomWalk == true and lastUseCustomWalk ~= data.player.walking.useCustomWalk then
                data.player.walking.defaultWalk(data.player.ped)
            end

            lastUseCustomWalk = data.player.walking.useCustomWalk
        end
        Citizen.Wait(sleep)
    end
end)








-- Citizen.CreateThread(function()
--     while true do
--         local sleep = 500
--         if data.started then
--             if data.player.anim.isPlaying then
				
--                 sleep = 5
--             end
--         end

--         Citizen.Wait(sleep)
--     end
-- end)



function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
	if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end