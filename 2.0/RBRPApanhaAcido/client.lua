local ESX = nil
local PlayerData = nil
local Keys = {
    ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
    ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
    ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
    ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
    ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
    ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
    ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
    ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
    ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

  
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)


local showingUI = false
local cancelApanha = false
local processing = false
local inveh = false
RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    exports.ft_libs:AddArea("apanhaMeta", {
        marker = {
            type = 27,
            weight = 1,
            height = 1,
            red = 255,
            green = 255,
            blue = 255,
            showDistance = 5,
            alpha = 15
        },
        trigger = {
            weight = 2,
            exit = {
                callback = function()
                    cancelApanha = true
                    if showingUI then
                        showingUI = false
                        TriggerEvent('luke_textui:HideUI')
                   end
                end
            },

            active = {
                callback = function()
                    TriggerEvent('luke_textui:ShowUI', 'Apanhar ácido sulfúrico')
                    if IsControlJustPressed(0, Keys['E']) and not processing then
                        if GetVehiclePedIsIn(GetPlayerPed(-1), false) == 0 then
                            TriggerEvent('luke_textui:HideUI')
                            showingUI = true

                            processing = true
                            exports['mythic_notify']:SendAlert('inform', 'A apanhar')

                            SetTimeout(10000, function()
                                if not cancelApanha then
                                    TriggerServerEvent('RBRPApanhaAcido:getitem')
                                    processing = false
                                    showingUI = false
                                else
                                    showingUI = false
                                    processing = false
                                    exports['mythic_notify']:SendAlert('error', 'Não devias ter saído do sítio')
                                end

                                cancelApanha = false
                            end)
                        else
                            exports['mythic_notify']:SendAlert('error', 'Sai do veículo')
                        end
                    end
                end,
            },
        },
        locations = {
            {
                x = 1244.57,
                y = -3010.97,
                z = 9.33,
            },
        },
    })

end)
