ESX = nil
local PlayerData = {}
local isMenuOn = false
local isCutting = false

local shopCostMenu = 0
local goldOresCount = 0
local axeCounter = 0
local ironOresCount = 0
local impacts = 0

local event_destination = nil
local isGetting = false
local newSpawnReady = true


NPCInfo = {
    coords = vector3(-570.96, 5365.75, 69.21),
    heading = 314.65,
    timeout = 15 * 60 * 1000, -- primeiro número = minutos

    informations = {
        [0] = 'É só preciso um machado. Esperavas que fosse difícil?',
        [1] = 'Cuida bem das caixas, há quem precise'
    }
}




local npcspawned = false
local npc = nil

function createNPC(coords, heading)
	local hash = GetHashKey('s_m_m_cntrybar_01')
    if not HasModelLoaded(hash) then
        RequestModel(hash)
        Wait(10)
    end
    
    while not HasModelLoaded(hash) do
        Wait(10)
    end
    
    npcspawned = true
    npc = CreatePed(5, hash, coords, heading, false, false)
    FreezeEntityPosition(npc, true)
    SetEntityInvincible(npc, true)
    SetBlockingOfNonTemporaryEvents(npc, true)
end


local cooldown = false
RegisterNetEvent('woodcutter:talkWithNPC')
AddEventHandler('woodcutter:talkWithNPC', function()
	if not cooldown then
		cooldown = true
		TriggerEvent('luke_textui:ShowUI', NPCInfo.informations[math.random(0, #NPCInfo.informations)])
		Citizen.Wait(5000)
		TriggerEvent('luke_textui:HideUI')
		SetTimeout(NPCInfo.timeout, function()
			cooldown = false
		end)
	else
		TriggerEvent('luke_textui:ShowUI', 'Agora pera, tenho de mamar uma cerveja')
		Citizen.Wait(5000)
		TriggerEvent('luke_textui:HideUI')
	end
end)

Citizen.CreateThread(function()
    while true do
        local dst = #(NPCInfo.coords - GetEntityCoords(GetPlayerPed(-1)))
        if dst < 200 and npcspawned == false then
            createNPC(NPCInfo.coords, NPCInfo.heading)
            npcspawned = true
        end
        if dst >= 201 and npcspawned then
            npcspawned = false
            DeleteEntity(npc)
        end
        Citizen.Wait(3000)
    end
end)


local apanha_lenha = {
    [0] = {
        coords = vector3(-543.53, 5383.30, 70.53),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha0",
        heading = 340.16
    },
    [1] = {
        coords = vector3(-541.47, 5382.40, 70.56),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha1",
        heading = 340.16
    },
    [2] = {
        coords = vector3(-539.33, 5381.40, 70.59),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha2",
        heading = 340.16
    },
    [3] = {
        coords = vector3(-537.49, 5380.58, 70.60),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha3",
        heading = 340.16
    },
    [4] = {
        coords = vector3(-535.89, 5379.88, 70.69),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha4",
        heading = 340.16
    },
    [5] = {
        coords = vector3(-536.44, 5372.54, 71.06),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha5",
        heading = 161.57
    },
    [6] = {
        coords = vector3(-534.89, 5372.06, 71.06),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha6",
        heading = 161.57
    },
    [7] = {
        coords = vector3(-533.26, 5371.56, 71.02),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha7",
        heading = 161.57
    },
    [8] = {
        coords = vector3(-531.49, 5371.02, 70.96),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha8",
        heading = 161.57
    },
    [9] = {
        coords = vector3(-530.07, 5370.58, 70.99),
        isCutted = false,
        minZ = 65.0,
        maxZ = 75.0,
        name = "Lenha9",
        heading = 161.57
    }
}

local corte_lenha = {
    [0] = {
        coords = vector3(-533.19, 5292.70, 74.21),
        minZ = 70.0,
        maxZ = 78.0,
        name = "Wash9"
    }
}


local Keys = {

    ["ESC"] = 322,
    ["F1"] = 288,
    ["F2"] = 289,
    ["F3"] = 170,
    ["F5"] = 166,
    ["F6"] = 167,
    ["F7"] = 168,
    ["F8"] = 169,
    ["F9"] = 56,
    ["F10"] = 57,

    ["~"] = 243,
    ["1"] = 157,
    ["2"] = 158,
    ["3"] = 160,
    ["4"] = 164,
    ["5"] = 165,
    ["6"] = 159,
    ["7"] = 161,
    ["8"] = 162,
    ["9"] = 163,
    ["-"] = 84,
    ["="] = 83,
    ["BACKSPACE"] = 177,

    ["TAB"] = 37,
    ["Q"] = 44,
    ["W"] = 32,
    ["E"] = 38,
    ["R"] = 45,
    ["T"] = 245,
    ["Y"] = 246,
    ["U"] = 303,
    ["P"] = 199,
    ["["] = 39,
    ["]"] = 40,
    ["ENTER"] = 18,

    ["CAPS"] = 137,
    ["A"] = 34,
    ["S"] = 8,
    ["D"] = 9,
    ["F"] = 23,
    ["G"] = 47,
    ["H"] = 74,
    ["K"] = 311,
    ["L"] = 182,

    ["LEFTSHIFT"] = 21,
    ["Z"] = 20,
    ["X"] = 73,
    ["C"] = 26,
    ["V"] = 0,
    ["B"] = 29,
    ["N"] = 249,
    ["M"] = 244,
    [","] = 82,
    ["."] = 81,

    ["LEFTCTRL"] = 36,
    ["LEFTALT"] = 19,
    ["SPACE"] = 22,
    ["RIGHTCTRL"] = 70,

    ["HOME"] = 213,
    ["PAGEUP"] = 10,
    ["PAGEDOWN"] = 11,
    ["DELETE"] = 178,

    ["LEFT"] = 174,
    ["RIGHT"] = 175,
    ["TOP"] = 27,
    ["DOWN"] = 173,

    ["NENTER"] = 201,
    ["N4"] = 108,
    ["N5"] = 60,
    ["N6"] = 107,
    ["N+"] = 96,
    ["N-"] = 97,
    ["N7"] = 117,
    ["N8"] = 61,
    ["N9"] = 118

}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
end)

local blips = {{
    title = "Lenhador",
    colour = 10,
    id = 77,
    x = -543.35,
    y = 5378.54,
    z = 70.56
}, {
    title = "Zona de Corte",
    colour = 22,
    id = 285,
    x = -533.05,
    y = 5291.67,
    z = 74.20
}}

Citizen.CreateThread(function()

    for _, info in pairs(blips) do
        info.blip = AddBlipForCoord(info.x, info.y, info.z)
        SetBlipSprite(info.blip, info.id)
        SetBlipDisplay(info.blip, 4)
        SetBlipScale(info.blip, 0.9)
        SetBlipColour(info.blip, info.colour)
        SetBlipAsShortRange(info.blip, true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(info.title)
        EndTextCommandSetBlipName(info.blip)
    end

    exports['bt-target']:AddBoxZone('ped_woodcutter', vector3(-570.78, 5365.92, 70.51), 0.8, 1.2, {
        name = 'Lenhador',
        heading = 246.00,
        debugPoly = false,
        minZ = 70.51-1.5,
        maxZ = 70.51+1.5
    }, {
        options = {{
            event = "woodcutter:talkWithNPC",
            icon = "fas fa-cut",
            label = "Falar com o Lenhador",
            params = {},
        }},
        job = {"all"},
        distance = 2
    })

    for ilenha, lenha in pairs(apanha_lenha) do
        exports['bt-target']:AddBoxZone(lenha.name, lenha.coords, 0.8, 1.2, {
            name = lenha.name,
            heading = 230.679,
            debugPoly = false,
            minZ = lenha.minZ,
            maxZ = lenha.maxZ
        }, {
            options = {
                {
                    event = "apanharMadeira:woodcutterjob",
                    icon = "fas fa-cut",
                    label = "Apanhar",
                    params = {
                        param = ilenha,
                        heading = lenha.heading
                    }
                }
            
            },
            job = {"all"},
            distance = 4
        })
    end

    for icorte, corte in pairs(corte_lenha) do
        exports['bt-target']:AddBoxZone(corte.name, corte.coords, 0.8, 1.2, {
            name = corte.name,
            heading = 302.679,
            debugPoly = false,
            minZ = corte.minZ,
            maxZ = corte.maxZ
        }, {
            options = {
                {
                    event = "cortarMadeira:woodcutterjob",
                    icon = "fas fa-cut",
                    label = "Cortar"
                },
                {
                    event = "apanharMadeira:woodToBox",
                    icon = "fas fa-cut",
                    label = "Transformar em caixa",
                }
            },
            job = {"all"},
            distance = 2
        })
    end

end)

local gettedSpots = 0
local headingatual = 0

local isMakingABox = false
RegisterNetEvent("apanharMadeira:woodToBox")
AddEventHandler("apanharMadeira:woodToBox", function(data)
    local player = GetPlayerPed(-1)
    local playerLoc = GetEntityCoords(player)

    if not isMakingABox and not isCutting then
        exports["mf-inventory"]:getInventoryItems(PlayerData.identifier,function(items)
            local titem = {has = false, db = nil}
            for iitem, item in pairs(items) do
                if item.name == 'cutted_wood' then
                    titem.has = true
                    titem.db = item
                    break
                end
            end

            if titem.has and titem.db.count >= 1 then
                isMakingABox = true
                exports['progressBars']:startUI(8000, "A fazer uma caixa")
                headingatual = 36.85
                loadAnimDict('mini@repair')
                TaskPlayAnim(GetPlayerPed(-1), 'mini@repair', 'fixing_a_ped', 5.0, 1.0, 1.0, 1, 0.0, 0, 0, 0)
                Citizen.Wait(8000)
                ClearPedTasks(GetPlayerPed(-1))
                TriggerServerEvent('cutWoodSV:givebox')
                isMakingABox = false
            else
                exports['mythic_notify']:SendAlert('error', 'Não tens blocos de madeira', 2000)
            end

            
        end)
        
        
    else
        exports['mythic_notify']:SendAlert('error', 'Já estás trabalhar, não abuses!', 2000)
    end
end)

RegisterNetEvent("apanharMadeira:woodcutterjob")
AddEventHandler("apanharMadeira:woodcutterjob", function(data)
    local player = GetPlayerPed(-1)
    local playerLoc = GetEntityCoords(player)

    if isGetting == false then

        if apanha_lenha[data.param].isCutted == false then

            exports["mf-inventory"]:getInventoryItems(PlayerData.identifier,function(items)
                local titem = nil
                for item,item in pairs(items) do
                    if item.name == 'machado' then
                        titem = item
                        break
                    end
                end
        
        
                if titem ~= nil then
                    if titem.count >= 1 then
                        exports['progressBars']:startUI(8000, "Cortar Madeira . . .")
                        headingatual = apanha_lenha[data.param].heading
                        Animacionmadera(data.heading)
                        local randomDestroy = math.random(1, 100)
                        if randomDestroy > 90 then
                            exports['mythic_notify']:SendAlert('error', 'Machado partiu!', 3000)
                            TriggerServerEvent("cutWoodSV:removeAxe")
                        end
                        apanha_lenha[data.param].isCutted = true
                    end
                else
                    exports['mythic_notify']:SendAlert('error', 'Precisas de um machado!', 2000)
                end
        
            end)

            gettedSpots = gettedSpots + 1
            if gettedSpots >= #apanha_lenha then
                clearIsGetted()
            end
        else
            exports['mythic_notify']:SendAlert('error', 'Já cortaste esta parte, salta para outra tono!', 2000)
        end
    else
        exports['mythic_notify']:SendAlert('error', 'Já estás a cortar, não abuses!', 2000)
    end

    

end)

function clearIsGetted()
    gettedSpots = 0
    for ilenha, lenha in pairs(apanha_lenha) do
        apanha_lenha[ilenha].isCutted = false
    end
end

function Animacionmadera(heading)
    Citizen.CreateThread(function()
        while impacts < 3 do
            isGetting = true
            Citizen.Wait(1)
            local ped = PlayerPedId()
            -- exports['progressBars']:startUI(2500, "Recolectando")
            RequestAnimDict("amb@world_human_hammering@male@base") -- melee@large_wpn@streamed_core
            Citizen.Wait(100)
            TaskPlayAnim((ped), 'amb@world_human_hammering@male@base', 'base', 8.0, 8.0, -1, 80, 0, 0, 0, 0)
            SetEntityHeading(ped, heading)

            if impacts == 0 then
                pickaxe = CreateObject(GetHashKey('prop_w_me_hatchet'), 0, 0, 0, true, true, true)
                AttachEntityToEntity(pickaxe, PlayerPedId(), GetPedBoneIndex(PlayerPedId(), 57005), 0.15, -0.02, -0.02,
                    350.0, 100.00, 280.0, true, true, false, true, 1, true)
            end
            Citizen.Wait(2500)
            ClearPedTasks(ped)
            impacts = impacts + 1
            if impacts == 3 then
                ClearPedTasks(GetPlayerPed(-1))
                DetachEntity(pickaxe, 1, true)
                DeleteEntity(pickaxe)
                DeleteObject(pickaxe)
                TriggerServerEvent("addItemsz:woodcutterJobz")
                isGetting = false
            end
        end
        impacts = 0
    end)
end


RegisterNetEvent("removeAxe:woodcutterJob")
AddEventHandler("removeAxe:woodcutterJob", function()
    RemoveWeaponFromPed(GetPlayerPed(-1), GetHashKey("weapon_hatchet"))
end)


RegisterNetEvent("cortarMadeira:woodcutterjob")
AddEventHandler("cortarMadeira:woodcutterjob", function()
    if not isCutting and not isMakingABox then
        isCutting = true
        exports['progressBars']:startUI(5000, "A cortar a madeira . . .")
        loadAnimDict('mini@repair')
        TaskPlayAnim(GetPlayerPed(-1), 'mini@repair', 'fixing_a_ped', 5.0, 1.0, 1.0, 1, 0.0, 0, 0, 0)
        Citizen.Wait(5000)
        ClearPedTasks(GetPlayerPed(-1))
        TriggerServerEvent("cutWoodSV:woodcutterJobz")
        isCutting = false
    else
        exports['mythic_notify']:SendAlert('error', 'Já estás a trabalhar, não abuses!', 1500)
    end

end)

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(0)
    end
end
