ESX = nil
local charge = 0

local display = false

local bossSpawned = false

local PlayerJob = nil

local isCooking = false

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(0)
    end

    

    RegisterNetEvent('esx:setJob')
    AddEventHandler('esx:setJob', function(job)
        PlayerJob = job
    end)

    while PlayerJob == nil do
        Citizen.Wait(100)
        PlayerJob = ESX.GetPlayerData().job
    end
    if Config.EnableBlip then
        blip = AddBlipForCoord(-1193.86200, -894.37900, 14.01902)
        SetBlipSprite(blip, 106)
        SetBlipDisplay(blip, 4)
        SetBlipScale(blip, 0.8)
        SetBlipColour(blip, 1)
        SetBlipAsShortRange(blip, true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString("Burger Shot")
        EndTextCommandSetBlipName(blip)
    end

    local burgerManager = {u_m_y_burgerdrug_01}

    exports['bt-target']:AddBoxZone("Boss", vector3(-1191.89, -901.04, 14.15), 0.8, 0.4, {
        name = "Boss",
        heading = 125.0,
        debugPoly = false,
        minZ = 13.000,
        maxZ = 15.00
    }, {
        options = {{
            event = "gl-burgershot_pixa:openBossMenu",
            icon = "fas fa-clipboard",
            label = "Menu Patrão"
        }},
        job = {"burgershot"},
        distance = 1.5
    })  

    exports['bt-target']:AddBoxZone("MakeMeal", vector3(-1197.92, -897.94, 13.92), 0.8, 1.8, {
        name = "MakeMeal",
        heading = 317.2,
        debugPoly = false,
        minZ = 13.7,
        maxZ = 14.1
    }, {
        options = {{
            event = "gl-burgershot_pixa:makeMeal1",
            icon = "fas fa-hamburger",
            label = "Menu Moneyshot"
        }, {
            event = "gl-burgershot_pixa:makeMeal2",
            icon = "fas fa-hamburger",
            label = "Menu Bleeder"
        }, {
            event = "gl-burgershot_pixa:makeMeal3",
            icon = "fas fa-hamburger",
            label = "Menu Heartstopper"
        }},
        job = {"burgershot"},
        distance = 2.5
    })

    -- Cash Register
    -- exports['bt-target']:AddBoxZone("BurgerPay", vector3(-1193.33, -895.05, 14.09839), 0.8, 0.4, {
    --     name = "BurgerPay",
    --     heading = 125.0,
    --     debugPoly = false,
    --     minZ = 14.000,
    --     maxZ = 14.30
    -- }, {
    --     options = {{
    --         event = "gl-burgershot_pixa:payorder",
    --         icon = "fas fa-cash-register",
    --         label = "Interact"
    --     }},
    --     job = {"all"},
    --     distance = 1.5
    -- })

    -- exports['bt-target']:AddBoxZone("BurgerPay2", vector3(-1192.50, -906.81, 13.89), 0.8, 0.4, {
    --     name = "BurgerPay2",
    --     heading = 125.0,
    --     debugPoly = false,
    --     minZ = 13.700,
    --     maxZ = 14.00
    -- }, {
    --     options = {{
    --         event = "gl-burgershot_pixa:payorder",
    --         icon = "fas fa-cash-register",
    --         label = "Interact"
    --     }},
    --     job = {"all"},
    --     distance = 1.5
    -- })

    --Freezer
    exports['bt-target']:AddBoxZone("Freezer", vector3(-1203.19, -895.79, 13.69), 0.8, 1.2, {
        name = "Freezer",
        heading = 302.679,
        debugPoly = false,
        minZ = 10.0,
        maxZ = 14.6
    }, {
        options = {{
            event = "gl-burgershot_pixa:inventory",
            icon = "fas fa-temperature-high",
            label = "Congelador"
        }},
        job = {"burgershot"},
        distance = 1.5
    })

    -- Frier
    exports['bt-target']:AddBoxZone("Frier", vector3(-1201.789, -899.025, 13.943), 0.8, 1.8, {
        name = "Frier",
        heading = 304.2,
        debugPoly = false,
        minZ = 13.6,
        maxZ = 14.3
    }, {
        options = {{
            event = "gl-burgershot_pixa:makeFries",
            icon = "fas fa-temperature-high",
            label = "Fritar batatas"
        }},
        job = {"burgershot"},
        distance = 1.5
    })

    exports['bt-target']:AddBoxZone("PrepareBurger", vector3(-1197.518, -899.39, 13.92), 0.8, 0.8, {
        name = "PrepareBurger",
        heading = 304.2,
        debugPoly = false,
        minZ = 13.6,
        maxZ = 14.3
    }, {
        options = {{
            event = "gl-burgershot_pixa:prepareMoneyShot",
            icon = "fas fa-hamburger",
            label = "Preparar hamburguer Moneyshot"
        }, {
            event = "gl-burgershot_pixa:prepareBleeder",
            icon = "fas fa-hamburger",
            label = "Preparar hamburguer Bleeder"
        }, {
            event = "gl-burgershot_pixa:prepareHeartStopper",
            icon = "fas fa-hamburger",
            label = "Preparar hamburguer Heartstopper"
        }},
        job = {"burgershot"},
        distance = 1.0
    })

    exports['bt-target']:AddBoxZone("DrinkMachineBS", vector3(-1199.679, -895.84, 13.909), 1.2, 1.7, {
        name = "DrinkMachineBS",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.6,
        maxZ = 15.3
    }, {
        options = {
        {
            event = "gl-burgershot_pixa:grabDrinkSmall",
            icon = "fab fa-gulp",
            label = "Cola Pequena"
        },
        {
            event = "gl-burgershot_pixa:grabDrink",
            icon = "fab fa-gulp",
            label = "Cola Média"
        },{
            event = "gl-burgershot_pixa:grabDrinkBig",
            icon = "fab fa-gulp",
            label = "Cola Grande"
        }},
        job = {"burgershot"},
        distance = 1.5
    })

    exports['bt-target']:AddBoxZone("CookMeat", vector3(-1202.791, -897.289, 13.96), 0.8, 1.2, {
        name = "CookMeat",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.6,
        maxZ = 14.3
    }, {
        options = {{
            event = "gl-burgershot_pixa:cookMeat",
            icon = "fas fa-temperature-high",
            label = "Cozinhar carne de porco"
        },
        {
            event = "gl-burgershot_pixa:cookChicken",
            icon = "fas fa-temperature-high",
            label = "Cozinhar carne de frango"
        }},
        job = {"burgershot"},
        distance = 1.5
    })

    exports['bt-target']:AddBoxZone("CortarCarne", vector3(-1190.49, -903.15, 13.92), 0.8, 0.4, {
        name = "CortarCarne",
        heading = 125.0,
        debugPoly = false,
        minZ = 10.000,
        maxZ = 15.00
    }, {
        options = {{
            event = "gl-burgershot_pixa:cutMeat",
            icon = "fas fa-temperature-high",
            label = "Cortar Carne Porco"
        },
        {
            event = "gl-burgershot_pixa:cutChicken",
            icon = "fas fa-temperature-high",
            label = "Cortar Carne Frango"
        }},
        job = {"burgershot"},
        distance = 1.5
    })

    exports['bt-target']:AddBoxZone("Tabuleiro1", vector3(-1194.06, -894.2797, 14.04), 0.8, 1.2, {
        name = "Tabuleiro1",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.8,
        maxZ = 14.6
    }, {
        options = {{
            event = "gl-burgershot_pixa:tabuleiro1",
            icon = "fas fa-temperature-high",
            label = "Tabuleiro"
        }},
        job = {"all"},
        distance = 1.5
    })

    exports['bt-target']:AddBoxZone("Tabuleiro2", vector3(-1195.35, -892.39, 14.04), 0.8, 1.2, {
        name = "Tabuleiro2",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.8,
        maxZ = 14.6
    }, {
        options = {{
            event = "gl-burgershot_pixa:tabuleiro2",
            icon = "fas fa-temperature-high",
            label = "Tabuleiro"
        }},
        job = {"all"},
        distance = 1.5
    })

    exports['bt-target']:AddBoxZone("TabuleiroDriveThru", vector3(-1194.01, -907.16, 14.00), 0.8, 1.2, {
        name = "TabuleiroDriveThru",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.8,
        maxZ = 14.6
    }, {
        options = {{
            event = "gl-burgershot_pixa:tabuleiroDriveTru",
            icon = "fas fa-temperature-high",
            label = "Tabuleiro"
        }},
        job = {"all"},
        distance = 2.5
    })

    exports['bt-target']:AddBoxZone("InventarioHaburguerAtendedor", vector3(-1196.66, -895.28, 14.39), 0.8, 1.2, {
        name = "InventarioHaburguer",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.8,
        maxZ = 14.9
    }, {
        options = {{
            event = "gl-burgershot_pixa:inventoryOnlyFood",
            icon = "fas fa-temperature-high",
            label = "Inventário"
        }},
        job = {"burgershot"},
        distance = 2.5
    })

    exports['bt-target']:AddBoxZone("InventarioHaburguerCozinheiro", vector3(-1197.07, -895.99, 14.14), 0.8, 1.2, {
        name = "InventarioHaburguer",
        heading = 302.679,
        debugPoly = false,
        minZ = 13.8,
        maxZ = 14.9
    }, {
        options = {{
            event = "gl-burgershot_pixa:inventoryOnlyFood",
            icon = "fas fa-temperature-high",
            label = "Inventário"
        }},
        job = {"burgershot"},
        distance = 2.5
    })

end)


-- Spawn Boss NPC When you get close, delete when you leave
-- Citizen.CreateThread(function()
--     while true do
--         Citizen.Wait(1000)
--         local pedCoords = GetEntityCoords(GetPlayerPed(-1))
--         local bossCoords = vector3(-1195.873, -901.336, 12.995)
--         local dst = #(bossCoords - pedCoords)

--         if dst < 200 and bossSpawned == false then
--             TriggerEvent('gl-burgershot_pixa:spawnBoss', bossCoords, 14.032)
--             bossSpawned = true
--         end
--         if dst >= 201 then
--             bossSpawned = false
--             DeletePed(npc)
--         end
--     end
-- end)




-- INVENTORY STUFF --
RegisterNetEvent('gl-burgershot_pixa:tabuleiro1')
AddEventHandler('gl-burgershot_pixa:tabuleiro1', function()
    exports["mf-inventory"]:openOtherInventory('burgershot_tabuleiro:1')
end)
RegisterNetEvent('gl-burgershot_pixa:tabuleiro2')
AddEventHandler('gl-burgershot_pixa:tabuleiro2', function()
    exports["mf-inventory"]:openOtherInventory('burgershot_tabuleiro:2')
end)
RegisterNetEvent('gl-burgershot_pixa:tabuleiroDriveTru')
AddEventHandler('gl-burgershot_pixa:tabuleiroDriveTru', function()
    exports["mf-inventory"]:openOtherInventory('burgershot_tabuleiro:3')
end)

RegisterNetEvent('gl-burgershot_pixa:inventory')
AddEventHandler('gl-burgershot_pixa:inventory', function()
    exports["mf-inventory"]:openOtherInventory('burgershot_inventory:1')
end)

RegisterNetEvent('gl-burgershot_pixa:inventoryOnlyFood')
AddEventHandler('gl-burgershot_pixa:inventoryOnlyFood', function()
    exports["mf-inventory"]:openOtherInventory('burgershot_inventory:2')
end)




-- PAYMENT STUFF --
-- RegisterNetEvent('gl-burgershot_pixa:payorder')
-- AddEventHandler('gl-burgershot_pixa:payorder', function()
--     if PlayerJob.name == 'burgershot' then
--         TriggerEvent('gl-burgershot_pixa:drawNui')
--     else
--         TriggerServerEvent('gl-burgershot_pixa:chargeMe')
--         TriggerServerEvent('gl-burgershot_pixa:notifyPaymentServer')

--     end
-- end)

-- RegisterNetEvent('gl-burgershot_pixa:notifyPaymentClient')
-- AddEventHandler('gl-burgershot_pixa:notifyPaymentClient', function(OrderTotal)
--     if PlayerJob.name == 'burgershot' then
--         exports['mythic_notify']:SendAlert('success', "An order was paid for, amount: $" .. OrderTotal, 8500)
--     end
-- end)

-- -- Debug Command only for Video Recording 
-- RegisterCommand('chargeMe', function()
--     TriggerServerEvent('gl-burgershot_pixa:chargeMe')
--     TriggerServerEvent('gl-burgershot_pixa:notifyPaymentServer')
-- end)

-- -- Grab raw Meat --
-- RegisterNetEvent('gl-burgershot_pixa:grabMeat')
-- AddEventHandler('gl-burgershot_pixa:grabMeat', function()
--     TriggerServerEvent('gl-burgershot_pixa:addItem', 'meat')
--     TriggerServerEvent('gl-burgershot_pixa:chargeSociety', Config.MeatPrice)
--     exports['mythic_notify']:SendAlert('success', "For the Meat, Burgershot paid " .. Config.MeatPrice, 8500)
-- end)

-- -- Grab Potatoe --
-- RegisterNetEvent('gl-burgershot_pixa:grabPotatoe')
-- AddEventHandler('gl-burgershot_pixa:grabPotatoe', function()
--     TriggerServerEvent('gl-burgershot_pixa:addItem', 'potatoe')
--     TriggerServerEvent('gl-burgershot_pixa:chargeSociety', Config.PotatoePrice)
--     exports['mythic_notify']:SendAlert('success', "For the potatoe, Burgershot paid " .. Config.PotatoePrice, 8500)
-- end)

-- -- Grab Bun --
-- RegisterNetEvent('gl-burgershot_pixa:grabBun')
-- AddEventHandler('gl-burgershot_pixa:grabBun', function()
--     TriggerServerEvent('gl-burgershot_pixa:addItem', 'bun')
--     TriggerServerEvent('gl-burgershot_pixa:chargeSociety', Config.BunPrice)
--     exports['mythic_notify']:SendAlert('success', "For the bun, Burgershot paid " .. Config.BunPrice, 8500)
-- end)

-- RegisterNetEvent('gl-burgershot_pixa:getcharged')
-- AddEventHandler('gl-burgershot_pixa:getcharged', function(OrderTotal)
--     if OrderTotal ~= 0 then
--         TriggerServerEvent('gl-burgershot_pixa:sendpayamount', OrderTotal)
--         exports['mythic_notify']:SendAlert('success', "You paid " .. OrderTotal, 8500)
--         TriggerServerEvent('gl-burgershot_pixa:createCharge', 0)
--     else
--         exports['mythic_notify']:SendAlert('error', "No Pending Charges ", 8500)
--     end
-- end)
-- END PAYMENT STUFF --


-- Cook Meat --
RegisterNetEvent('gl-burgershot_pixa:cookMeat')
AddEventHandler('gl-burgershot_pixa:cookMeat', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'cookedmeat')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end
end)
RegisterNetEvent('gl-burgershot_pixa:cookChicken')
AddEventHandler('gl-burgershot_pixa:cookChicken', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'cookedChicken')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end  
end)

--cortar carne
RegisterNetEvent('gl-burgershot_pixa:cutMeat')
AddEventHandler('gl-burgershot_pixa:cutMeat', function()
    if isCooking == false then

        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:cortarCarneOuFrango', "packaged_pig", "bifeporco")

        loadAnimDict("mini@repair")
        TaskPlayAnim(GetPlayerPed(-1), "mini@repair", "fixing_a_ped", 8.0, 1.0, -1, 2, 0, 0, 0, 0)

        Citizen.Wait(2500)
        ClearPedTasks(GetPlayerPed(-1))

        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end  
end)

--cortar frango
RegisterNetEvent('gl-burgershot_pixa:cutChicken')
AddEventHandler('gl-burgershot_pixa:cutChicken', function()
    if isCooking == false then
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:cortarCarneOuFrango', "packaged_chicken", "bifefrango")

        loadAnimDict("mini@repair")
        TaskPlayAnim(GetPlayerPed(-1), "mini@repair", "fixing_a_ped", 8.0, 1.0, -1, 2, 0, 0, 0, 0)

        Citizen.Wait(2500)
        ClearPedTasks(GetPlayerPed(-1))
        
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end  
end)

-- Grab a Drink Big --
RegisterNetEvent('gl-burgershot_pixa:grabDrinkSmall')
AddEventHandler('gl-burgershot_pixa:grabDrinkSmall', function()
    loadAnimDict("anim@mp_player_intupperspray_champagne")
    TaskPlayAnim(GetPlayerPed(-1), "anim@mp_player_intupperspray_champagne", "idle_a", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
    Wait(2000)
    ClearPedTasks(GetPlayerPed(-1))
    TriggerServerEvent('gl-burgershot_pixa:addItem', 'cocacolapequena')
end)

-- Grab a Drink --
RegisterNetEvent('gl-burgershot_pixa:grabDrink')
AddEventHandler('gl-burgershot_pixa:grabDrink', function()
    loadAnimDict("anim@mp_player_intupperspray_champagne")
    TaskPlayAnim(GetPlayerPed(-1), "anim@mp_player_intupperspray_champagne", "idle_a", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
    Wait(2000)
    ClearPedTasks(GetPlayerPed(-1))
    TriggerServerEvent('gl-burgershot_pixa:addItem', 'cocacola')
end)

-- Grab a Drink Big --
RegisterNetEvent('gl-burgershot_pixa:grabDrinkBig')
AddEventHandler('gl-burgershot_pixa:grabDrinkBig', function()
    loadAnimDict("anim@mp_player_intupperspray_champagne")
    TaskPlayAnim(GetPlayerPed(-1), "anim@mp_player_intupperspray_champagne", "idle_a", 8.0, 1.0, -1, 2, 0, 0, 0, 0)
    Wait(2000)
    ClearPedTasks(GetPlayerPed(-1))
    TriggerServerEvent('gl-burgershot_pixa:addItem', 'cocacolagrande')
end)

--[[ MEALS]]

-- Make Money Shot Burger --
RegisterNetEvent('gl-burgershot_pixa:makeMeal1')
AddEventHandler('gl-burgershot_pixa:makeMeal1', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'meal1')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

RegisterNetEvent('gl-burgershot_pixa:makeMeal2')
AddEventHandler('gl-burgershot_pixa:makeMeal2', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'meal2')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

RegisterNetEvent('gl-burgershot_pixa:makeMeal3')
AddEventHandler('gl-burgershot_pixa:makeMeal3', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'meal3')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

--[[ END MEALS ]]

-- Make Money Shot Burger --
RegisterNetEvent('gl-burgershot_pixa:prepareMoneyShot')
AddEventHandler('gl-burgershot_pixa:prepareMoneyShot', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'moneyshot')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

-- Make Bleeder Burger --
RegisterNetEvent('gl-burgershot_pixa:prepareBleeder')
AddEventHandler('gl-burgershot_pixa:prepareBleeder', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'bleeder')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

-- Make Heart Stopper Burger --
RegisterNetEvent('gl-burgershot_pixa:prepareHeartStopper')
AddEventHandler('gl-burgershot_pixa:prepareHeartStopper', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'heartstopper')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

-- Burger prepare animation
RegisterNetEvent('gl-burgershot_pixa:cookAnimation')
AddEventHandler('gl-burgershot_pixa:cookAnimation', function(Animation)
    local ped = GetPlayerPed(-1)
    TaskStartScenarioInPlace(ped, Animation, 0, false)
    Wait(10000)
    ClearPedTasks(ped)
end)

-- Make Fries --
RegisterNetEvent('gl-burgershot_pixa:makeFries')
AddEventHandler('gl-burgershot_pixa:makeFries', function()
    if isCooking == false then
        exports["progressBars"]:startUI(10000, "A cozinhar . . .");
        isCooking = true
        TriggerServerEvent('gl-burgershot_pixa:checkCanCraft', 'fries')
        Citizen.Wait(9500)
        isCooking = false
    else
        exports['mythic_notify']:SendAlert('error', "Um de cada vez.", 3000)
    end 
end)

-- Open Boss Menu
RegisterNetEvent('gl-burgershot_pixa:openBossMenu')
AddEventHandler('gl-burgershot_pixa:openBossMenu', function()
    TriggerEvent('esx_society:openBossMenu', 'burgershot', function(data, menu)
        menu.close()
    end)
end)

------- Functions if needed ----------

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(5)
    end
end

-- NUI STUFF --

RegisterNetEvent('gl-burgershot_pixa:drawNui')
AddEventHandler('gl-burgershot_pixa:drawNui', function()
    SetDisplay(not display)
end)

-- very important cb 
RegisterNUICallback("exit", function(data)
    SetDisplay(false)
end)

-- this cb is used as the main route to transfer data back 
-- and also where we hanld the data sent from js
RegisterNUICallback("main", function(data)
    amount = tonumber(data.text)
    TriggerServerEvent('gl-burgershot_pixa:createCharge', amount)
    SetDisplay(false)
end)

RegisterNUICallback("error", function(data)
    SetDisplay(false)
end)

function SetDisplay(bool)
    display = bool
    SetNuiFocus(bool, bool)
    SendNUIMessage({
        type = "ui",
        status = bool
    })
end

Citizen.CreateThread(function()
    while display do
        Citizen.Wait(0)
        -- https://runtime.fivem.net/doc/natives/#_0xFE99B66D079CF6BC
        --[[ 
            inputGroup -- integer , 
	        control --integer , 
            disable -- boolean 
        ]]
        DisableControlAction(0, 1, display) -- LookLeftRight
        DisableControlAction(0, 2, display) -- LookUpDown
        DisableControlAction(0, 142, display) -- MeleeAttackAlternate
        DisableControlAction(0, 18, display) -- Enter
        DisableControlAction(0, 322, display) -- ESC
        DisableControlAction(0, 106, display) -- VehicleMouseControlOverride
    end
end)



