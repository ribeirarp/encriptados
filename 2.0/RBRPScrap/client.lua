ESX = nil
local pricePerCar = 1000
local disablecontrols = false
allowedchop = true
payitems = math.random(8, 15)
chopstarted = false
station = nil

local Keys = {

    ["ESC"] = 322,
    ["F1"] = 288,
    ["F2"] = 289,
    ["F3"] = 170,
    ["F5"] = 166,
    ["F6"] = 167,
    ["F7"] = 168,
    ["F8"] = 169,
    ["F9"] = 56,
    ["F10"] = 57,

    ["~"] = 243,
    ["1"] = 157,
    ["2"] = 158,
    ["3"] = 160,
    ["4"] = 164,
    ["5"] = 165,
    ["6"] = 159,
    ["7"] = 161,
    ["8"] = 162,
    ["9"] = 163,
    ["-"] = 84,
    ["="] = 83,
    ["BACKSPACE"] = 177,

    ["TAB"] = 37,
    ["Q"] = 44,
    ["W"] = 32,
    ["E"] = 38,
    ["R"] = 45,
    ["T"] = 245,
    ["Y"] = 246,
    ["U"] = 303,
    ["P"] = 199,
    ["["] = 39,
    ["]"] = 40,
    ["ENTER"] = 18,

    ["CAPS"] = 137,
    ["A"] = 34,
    ["S"] = 8,
    ["D"] = 9,
    ["F"] = 23,
    ["G"] = 47,
    ["H"] = 74,
    ["K"] = 311,
    ["L"] = 182,

    ["LEFTSHIFT"] = 21,
    ["Z"] = 20,
    ["X"] = 73,
    ["C"] = 26,
    ["V"] = 0,
    ["B"] = 29,
    ["N"] = 249,
    ["M"] = 244,
    [","] = 82,
    ["."] = 81,

    ["LEFTCTRL"] = 36,
    ["LEFTALT"] = 19,
    ["SPACE"] = 22,
    ["RIGHTCTRL"] = 70,

    ["HOME"] = 213,
    ["PAGEUP"] = 10,
    ["PAGEDOWN"] = 11,
    ["DELETE"] = 178,

    ["LEFT"] = 174,
    ["RIGHT"] = 175,
    ["TOP"] = 27,
    ["DOWN"] = 173,

    ["NENTER"] = 201,
    ["N4"] = 108,
    ["N5"] = 60,
    ["N6"] = 107,
    ["N+"] = 96,
    ["N-"] = 97,
    ["N7"] = 117,
    ["N8"] = 61,
    ["N9"] = 118

}

coordsScrap = vector3(-557.64, -1695.82, 19.16 - 0.9)
local PlayerData = nil
Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(10)
    end

    while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

chopdoors = {
    [1] = {
        chopdoor = false,
        doorgone = false,
        doordelivered = false,
        CarParts = "wheel_lf",
        PartLabel = "Pneu",
        PartProp = "prop_wheel_tyre",
        PartBone = 28422,
        PartX = 0.0,
        PartY = 0.5,
        PartZ = -0.05,
        PartxR = 0.0,
        PartyR = 0.0,
        PartzR = 0.0,
        label = 'pneu'
    },
    [2] = {
        chopdoor = false,
        doorgone = false,
        doordelivered = false,
        CarParts = "wheel_rf",
        PartLabel = "Pneu",
        PartProp = "prop_wheel_tyre",
        PartBone = 28422,
        PartX = 0.0,
        PartY = 0.5,
        PartZ = -0.05,
        PartxR = 0.0,
        PartyR = 0.0,
        PartzR = 0.0,
        label = 'pneu'

    },
    [3] = {
        chopdoor = false,
        doorgone = false,
        doordelivered = false,
        CarParts = "wheel_lr",
        PartLabel = "Pneu",
        PartProp = "prop_wheel_tyre",
        PartBone = 28422,
        PartX = 0.0,
        PartY = 0.5,
        PartZ = -0.05,
        PartxR = 0.0,
        PartyR = 0.0,
        PartzR = 0.0,
        label = 'pneu'

    },
    [4] = {
        chopdoor = false,
        doorgone = false,
        doordelivered = false,
        CarParts = "wheel_rr",
        PartLabel = "Pneu",
        PartProp = "prop_wheel_tyre",
        PartBone = 28422,
        PartX = 0.0,
        PartY = 0.5,
        PartZ = -0.05,
        PartxR = 0.0,
        PartyR = 0.0,
        PartzR = 0.0,
        label = 'pneu'

    },
    [5] = {
        chopdoor = false,
        doorgone = false,
        doordelivered = false,
        CarParts = "engine",
        PartLabel = "Bateria",
        PartProp = "prop_car_battery_01",
        PartBone = 28422,
        PartX = 0.0,
        PartY = 0.5,
        PartZ = -0.05,
        PartxR = 0.0,
        PartyR = 0.0,
        PartzR = 0.0,
        label = 'bateria'

    },
    [6] = {
        chopdoor = false,
        doorgone = false,
        doordelivered = false,
        CarParts = "engine",
        PartLabel = "Motor",
        PartProp = "imp_prop_impexp_engine_part_01a",
        PartBone = 28422,
        PartX = 0.0,
        PartY = 0.5,
        PartZ = -0.05,
        PartxR = 0.0,
        PartyR = 0.0,
        PartzR = 0.0,
        label = 'motor'

    }
}

ChopCarLocation = { --- Coords [x] = Door Coords [x2] =location if need be
    [1] = {
        Chop = vector3(-557.64, -1695.82, 19.16),
        Sell = vector3(-556.36, -1704.7, 19.04),
        Type = "money"
    }

}

function DrawM(hint, type, coordsget)
    -- 450.50 -977.02 25.69

    ESX.Game.Utils.DrawText3D({
        x = coordsget.x,
        y = coordsget.y,
        z = coordsget.z + 1.0
    }, hint, 0.4)

    DrawMarker(type, coordsget.x, coordsget.y, coordsget.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 1.5, 255, 255, 255,
        100, false, true, 2, false, false, false, false)

end

---todo:
----Notificar policia
----Colocar carro preso quando começas o desmanche 
----drops direitos

Citizen.CreateThread(function()
    while true do
        local ped = GetPlayerPed(-1)

        local distanceToSee = 3
        local coordsplayer = GetEntityCoords(ped)
        PlayerDataCoords = GetEntityCoords(PlayerPedId())
        local sleep = 1000
        if ESX then
            if IsPedInAnyVehicle(ped, false) and GetPedInVehicleSeat(GetVehiclePedIsIn(ped), -1) == ped and GetVehicleClass(GetVehiclePedIsIn(GetPlayerPed(-1), false)) ~= 8 then
                if (#(coordsScrap - PlayerDataCoords)) < distanceToSee then
                    sleep = 1
                    DrawM("[E] para desmanche!", 27, coordsScrap)
                    MenuOpen()
                end
            end
        end
        Citizen.Wait(sleep)
    end
end)

function MenuOpen()
    if IsControlJustReleased(0, Keys['E']) then

        StartChopThisCar()
    end
end

function disable()
    Citizen.CreateThread(function()
        while disablecontrols do
            Citizen.Wait(10)
            DisableControlAction(0, 24, true) -- Attack
            DisableControlAction(0, 257, true) -- Attack 2
            DisableControlAction(0, 25, true) -- Aim
            DisableControlAction(0, 263, true) -- Melee Attack 1
            DisableControlAction(0, 32, true) -- W
            DisableControlAction(0, 34, true) -- A
            DisableControlAction(0, 38, true) -- A
            DisableControlAction(0, 31, true) -- S (fault in Keys table!)
            DisableControlAction(0, 30, true) -- D (fault in Keys table!)

            DisableControlAction(0, 45, true) -- Reload
            DisableControlAction(0, 44, true) -- Cover
            DisableControlAction(0, 37, true) -- Select Weapon
            DisableControlAction(0, 23, true) -- Also 'enter'?

            DisableControlAction(0, 289, true) -- Inventory
            DisableControlAction(0, 170, true) -- Animations
            DisableControlAction(0, 167, true) -- Job

            DisableControlAction(0, 0, true) -- Disable changing view
            DisableControlAction(0, 26, true) -- Disable looking behind
            DisableControlAction(0, 73, true) -- Disable clearing animation
            DisableControlAction(2, 199, true) -- Disable pause screen

            DisableControlAction(0, 59, true) -- Disable steering in vehicle
            DisableControlAction(0, 71, true) -- Disable driving forward in vehicle
            DisableControlAction(0, 72, true) -- Disable reversing in vehicle

            DisableControlAction(2, 36, true) -- Disable going stealth

            DisableControlAction(0, 47, true) -- Disable weapon
            DisableControlAction(0, 264, true) -- Disable melee
            DisableControlAction(0, 257, true) -- Disable melee
            DisableControlAction(0, 140, true) -- Disable melee
            DisableControlAction(0, 141, true) -- Disable melee
            DisableControlAction(0, 142, true) -- Disable melee
            DisableControlAction(0, 143, true) -- Disable melee
            DisableControlAction(0, 75, true) -- Disable exit vehicle
            DisableControlAction(27, 75, true) -- Disable exit vehicle
        end
    end)
end

function start()
    while chopstarted do
        Citizen.Wait(10)
        if station ~= nil then
            if veh == nil or veh == 0 then
                veh = GetVehiclePedIsIn(PlayerPedId(), true)
            end
            for i = 1, #chopdoors, 1 do
                if allowedchop and not chopdoors[i].doorgone and not chopdoors[i].doordelivered then
                    while not chopdoors[i].chopdoor and chopstarted do
                        
                        local x, y, z = table.unpack(GetWorldPositionOfEntityBone(veh, GetEntityBoneIndexByName(veh,
                            chopdoors[i].CarParts)))
                        Citizen.Wait(7)
                        DrawMarker(27, x, y, z - 0.3, 0, 0, 0, 0, 0, 0, 0.5, 0.5, 0.5, 0, 157, 0, 155, 0, 0, 2, 0, 0, 0,
                            0)

                        if GetDistanceBetweenCoords(GetEntityCoords(PlayerPedId()), x, y, z, true) < 2.5 and IsPedStill(PlayerPedId()) and not chopdoors[i].chopdoor then -- 1.2
                            DrawText3Ds(x, y, z, tostring("[~g~E~w~] " .. chopdoors[i].PartLabel))
                            if (IsControlJustPressed(1, 38)) then
                                -- ESX.TriggerServerCallback('tqrp_base:itemQtty', function(cb)
                                -- if cb > 0 then
                                disablecontrols = true
                                disable()
                                chopdoors[i].chopdoor = true

                                ChopDoors(i)
                                -- else
                                --	exports['mythic_notify']:SendAlert('error', 'Não tens o necessário!')
                                -- end
                                -- end, 'blowtorch')
                                
                                if i ~= 6 then
                                    i = i + 1
                                end
                            end
                        else
                            if IsControlJustPressed(1, 38) then
                                exports['mythic_notify']:SendAlert('error', 'Estás muito longe!')
                            end
                        end
                    end
                end
            end
        else
            Citizen.Wait(1500)
        end
    end
end

function OpenCarDoors()
    local ped = GetPlayerPed(-1)
    local veh2 = GetVehiclePedIsIn(ped, true)
    local vehiclePedIsIn = GetVehiclePedIsIn(ped, false)
    SetVehicleDoorOpen(veh2, 0, false, true)
    SetVehicleDoorOpen(veh2, 1, false, true)
    SetVehicleDoorOpen(veh2, 2, false, true)
    SetVehicleDoorOpen(veh2, 3, false, true)
    SetVehicleDoorOpen(veh2, 4, false, true)
    SetVehicleDoorOpen(veh2, 5, false, true)
    TaskLeaveVehicle(ped, vehiclePedIsIn, 256)
    SetVehicleDoorsLocked(veh2, 2)
    Citizen.Wait(1000)
end

function ChopDoors(i)
    local player = PlayerId()
    local plyPed = GetPlayerPed(player)
    veh = GetClosestVehicle(ChopCarLocation[station].Chop, 4.001, 0, 70)
    if chopdoors[i].chopdoor then
        TaskStartScenarioInPlace(plyPed, "WORLD_HUMAN_WELDING", 0, true)
        exports['progressBars']:startUI(6000, "A remover ...")
        Citizen.Wait(6000)
        if not status then
            -- SetVehicleDoorBroken(veh, 0, true)
            ClearPedTasksImmediately(plyPed)
            chopdoors[i].chopdoor = false
            chopdoors[i].doorgone = true
            disablecontrols = false
            local PackageObject = CreateObject(GetHashKey(chopdoors[i].PartProp), 1.0, 1.0, 1.0, 1, 1, 0)
            -- SetEntityCollision(PackageObject, false, false)
            -- PlaceObjectOnGroundProperly(PackageObject)
            CarryingPart(PackageObject, i)
        else
            disablecontrols = false
            ClearPedTasksImmediately(plyPed)
            chopstarted = false
            for i = 1, #chopdoors, 1 do
                chopdoors[i].chopdoor = false
                chopdoors[i].doorgone = false
                chopdoors[i].doordelivered = false
            end
            DeleteEntity(veh)
            veh = nil
        end

    end
end

function FinishedChopping()
    local vehchopping = GetClosestVehicle(ChopCarLocation[station].Chop, 4.001, 0, 70)
    TriggerServerEvent('rbrp_illegalchop:success', math.random(50, 200), "money", GetVehicleNumberPlateText(vehchopping))
    chopstarted = false
    Wait(1000)
    DeleteEntity(vehchopping)
    exports['mythic_notify']:SendAlert('success', 'Todo o veículo foi desmantelado com sucesso')
    for i = 1, #chopdoors, 1 do
        chopdoors[i].chopdoor = false
        chopdoors[i].doorgone = false
        chopdoors[i].doordelivered = false
    end
    veh = nil
end

function tofaraway()
    local vehchopping = GetClosestVehicle(ChopCarLocation[station].Chop, 4.001, 0, 70)
    chopstarted = false
    station = nil
    DeleteEntity(vehchopping)
    exports['mythic_notify']:SendAlert('error', 'Afaste-se daqui!')
end

function StartChopThisCar()
    local veh2 = GetVehiclePedIsIn(PlayerPedId(-1), true)
    station = 1
    SetEntityCoords(veh2, ChopCarLocation[1].Chop)
    SetEntityHeading(veh2, 27.77)
    chopstarted = true
    -- abrirportas
    OpenCarDoors()
    start()
end

function DrawText3Ds(x, y, z, text)
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local px, py, pz = table.unpack(GetGameplayCamCoords())
    local scale = 0.30
    if onScreen then
        SetTextScale(scale, scale)
        SetTextFont(6)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 215)
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x, _y)
    end
end

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(10)
    end
end

function CarryingPart(partID, i)
    if DoesEntityExist(partID) then
        loadAnimDict("anim@heists@box_carry@")
        if not IsEntityPlayingAnim(PlayerPedId(), "anim@heists@box_carry@", "idle", 3) then
            ClearPedTasks(PlayerPedId())
            loadAnimDict("anim@heists@box_carry@")
            TaskPlayAnim((PlayerPedId()), "anim@heists@box_carry@", "idle", 4.0, 1.0, -1, 49, 0, 0, 0, 0)
            AttachEntityToEntity(partID, PlayerPedId(), chopdoors[i].PartBone, chopdoors[i].PartX, chopdoors[i].PartY,
                chopdoors[i].PartZ, chopdoors[i].PartxR, chopdoors[i].PartYR, chopdoors[i].PartZR, 1, 1, 0, true, 2, 1)
        end
    else
        return
    end
    local Packaging = true
    while Packaging do
        Citizen.Wait(7)
        if not IsEntityAttachedToEntity(partID, PlayerPedId()) then
            Packaging = false
            DeleteEntity(partID)
        else
            local PedPosition = GetEntityCoords(PlayerPedId())
            local DistanceCheck = GetDistanceBetweenCoords(PedPosition, ChopCarLocation[station].Sell, true)
            local x, y, z = table.unpack(ChopCarLocation[station].Sell)
            DrawText3Ds(x, y, z, tostring("[~g~E~w~] Vender Peças"))
            if DistanceCheck <= 1.25 then
                if IsControlJustPressed(0, 38) then

                    disablecontrols = true
                    disable()
                    DeleteEntity(partID)
                    -- Abrir as portas

                    ClearPedTasksImmediately(PlayerPedId())
                    Packaging = false
                    if chopdoors[i].doorgone then
                        TriggerServerEvent('rbrp_illegalchop:success', math.random(8, 15), "items", nil, chopdoors[i].label)
                        if math.random(1, 10) > 5 then
                            TriggerServerEvent('rbrp_illegalchop:success', math.random(30, 50), "money", nil, nil)
                        end
                        chopdoors[i].doorgone = false
                        chopdoors[i].doordelivered = true
                        disablecontrols = false
                        if chopdoors[#chopdoors].doordelivered then
                            FinishedChopping()
                        end
                    end
                end
            end
        end
    end
end


function SpawnVehicle(vehicle, garage, KindOfVehicle, tunerdata, bodyHealth, engineHealth, fuel)
	--print("spawnvehicle")
    xx = 2053.6
    yy = 3193.02
    zz = 45.19
    heading = 144.57


	ESX.Game.SpawnVehicle(vehicle.model, {
		x = xx,
		y = yy,
		z = zz											
		},heading, function(callback_vehicle)

			ESX.Game.SetVehicleProperties(callback_vehicle, vehicle)
			TaskWarpPedIntoVehicle(PlayerPedId(), callback_vehicle, -1)
			local carplate = GetVehicleNumberPlateText(callback_vehicle)
			exports["RBRPonyxLocksystem"]:givePlayerKeys(carplate)

            SetVehicleCustomPrimaryColour(callback_vehicle, vehicle.customPrimaryColor.r, vehicle.customPrimaryColor.g, vehicle.customPrimaryColor.b)
			SetVehicleCustomSecondaryColour(callback_vehicle, vehicle.customSecondaryColor.r, vehicle.customSecondaryColor.g, vehicle.customSecondaryColor.b)

			if tunerdata then
				local boost = tunerdata.boost
				local gearchange = tunerdata.gearchange
				local fuelmix = tunerdata.gearchange
				local total = 0
				if boost then
					total = total + boost
				end
				if fuelmix then
					total = total + fuelmix
				end
				if gearchange then
					total = total + gearchange
				end
				SetVehicleEnginePowerMultiplier(callback_vehicle, total*1.0)
			end
			SetVehicleFuelLevel(callback_vehicle, fuel + 0.0)
			DecorSetFloat(callback_vehicle, "_FUEL_LEVEL", GetVehicleFuelLevel(callback_vehicle))
			if bodyHealth >=0 then
				SetVehicleBodyHealth(callback_vehicle, bodyHealth+0.00)

				SetVehicleDamage(callback_vehicle, 0.0, 1.0, 0.1, math.floor(1000 - bodyHealth+0.00) + 0.0, 1850.0, true) --800
				SetVehicleDamage(callback_vehicle, -0.2, 1.0, 0.5, math.floor(1000 - bodyHealth+0.00) + 0.0, 650.0, true) -- 50
				SetVehicleDamage(callback_vehicle, -0.7, -0.2, 0.3, math.floor(1000 - bodyHealth+0.00) + 0.0, 500.0, true) --00 50
			end
			if engineHealth >= 0 then
				SetVehicleEngineHealth(callback_vehicle, engineHealth+0.00)
			end
		end)
	
    TriggerServerEvent('sucata:updateVeh', vehicle.plate, 0)
end

local isMenuOpen = false
RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
    exports.ft_libs:AddArea("sucata_blip", {
        marker = {
            weight = 1.5,
            height = 1.0,
            red = 0,
            green = 0,
            blue = 255,
            type = 27,
            alpha = 70,
        },
        trigger = {
            weight = 1.5,
            exit = {
                callback = function()
                    TriggerEvent('luke_textui:HideUI')
                    ESX.UI.Menu.CloseAll()
                end,
            },

            active = {
                callback = function()
                    exports.ft_libs:HelpPromt('Sucata | ' .. pricePerCar .. '€/carro')
                    if IsControlJustReleased(0, 38) and not isMenuOpen then
                        isMenuOpen = true
                        local elements, vehiclePropsList = {}, {}
                        local vehicleTunerList = {}
                        local vehicleBodyHealthList = {}
                        local vehicleEngineHealthList = {}
                        local vehicleFuelList = {}
                        ESX.TriggerServerCallback('sucateiro:getVehicles', function(vehicles)
                            if #vehicles > 0 then
                                for _,v in pairs(vehicles) do
                                    local vehicleProps = json.decode(v.vehicle)
                                    local vehicleTuner = nil
                                    if (v.tunerdata) then
                                        vehicleTuner = json.decode(v.tunerdata)
                                    else
                                        vehicleTuner = ''
                                    end
                                    vehiclePropsList[vehicleProps.plate] = vehicleProps
                                    vehicleTunerList[vehicleProps.plate] = vehicleTuner
                                    vehicleBodyHealthList[vehicleProps.plate] = v.bodyHealth
                                    vehicleEngineHealthList[vehicleProps.plate] = v.engineHealth
                                    vehicleFuelList[vehicleProps.plate] = v.fuel
                                    local vehicleHash = vehicleProps.model
                                    local vehicleName, vehicleLabel
                                                    
                                    if v.vehiclename == 'vehicle' then
                                        vehicleName = GetDisplayNameFromVehicleModel(vehicleHash)
                                    else
                                        vehicleName = v.vehiclename
                                    end

                                    vehicleLabel = vehicleName..': ' .. 'Requisitar' 
                                    table.insert(elements, {
                                        label = vehicleLabel,
                                        vehicleName = vehicleName,
                                        stored = v.stored,
                                        plate = vehicleProps.plate,
                                        fourrieremecano = v.fourrieremecano,
                                        garage_name = v.garage_name
                                    })
                                    
                                end
                            else
                                table.insert(elements, {label = 'Sem carros', value = 0})
                            end
                            ESX.UI.Menu.Open(
                            'default', GetCurrentResourceName(), 'spawn_vehicle',
                            {
                                title    = 'Sucata | ' .. pricePerCar .. '€/carro',
                                align = 'right',
                                elements = elements,
                            },
                            function(data, menu)
                                if data.current.value ~= 0 then
                                    local CarProps = vehiclePropsList[data.current.plate]
                                    local tunerData = vehicleTunerList[data.current.plate]
                                    local bodyHealth = vehicleBodyHealthList[data.current.plate]
                                    local engineHealth = vehicleEngineHealthList[data.current.plate]
                                    local fuel = vehicleFuelList[data.current.plate]
                                    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_menu', {
                                        title    =  data.current.vehicleName,
                                        align = 'right',
                                        elements = {
                                            {label = 'Tirar carro' , value = 'get_vehicle_out'},
                                    }}, function(data2, menu2)
                                            if data2.current.value == "get_vehicle_out" then
                                                exports["mf-inventory"]:getInventoryItems(PlayerData.identifier,function(items)
                                                    local hasMoney = false
                                                    for iitem,item in pairs(items) do
                                                        if item.name == 'money' and item.count >= pricePerCar then
                                                            hasMoney = true
                                                            break
                                                        end
                                                    end
    
                                                    if hasMoney then
                                                        TriggerServerEvent('sucata:removeMoney', pricePerCar)
                                                        SpawnVehicle(CarProps, garage, KindOfVehicle, tunerData, bodyHealth, engineHealth, fuel)
                                                        
                                                        
                                                        
                                                        ESX.UI.Menu.CloseAll()
                                                    else
                                                        exports['mythic_notify']:SendAlert('error', 'Não tens dinheiro em mão suficiente')
                                                    end
                                                end)
                                            end
                                        end,
                                    function(data2, menu2)
                                        menu2.close()
                                    end)
                                else
                                    exports['mythic_notify']:SendAlert('error', 'Não tens carros')
                                end
                            end,
                            function(data, menu)
                                menu.close()
                            end
                        )
                        end)


                        SetTimeout(2500, function()
                            isMenuOpen = false
                        end)
                    end
                    
                end,
            },
        },
        blip = {
            text = "Sucata",
            colorId = 1,
            imageId = 380,
        },
        locations = {
            {
                x =  2053.6, y = 3193.02, z = 45.19
            },
        },
    })
end)



RegisterCommand('t', function()
    print()
end)




Citizen.CreateThread(function()
    while true do
        Citizen.Wait(30000)
        collectgarbage()
    end
end) -- Prevents RAM LEAKS :)
