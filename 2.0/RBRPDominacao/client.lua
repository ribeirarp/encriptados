local ESX = nil
local data = {
	started = false,
	
	player = {
		ped = nil,
		coords = nil,
		vehicle = {
			isInside = false,
			entity = nil
		}
	},
	
}
local isDead = false
local timerValid = false
local dominationSpots = {
	-- [0] = {
	-- 	player = {
	-- 		isOwner = false,
	-- 		joined = false
	-- 	},
		
	-- 	startTs = GetGameTimer() + 300000,
	-- hasStarted = false
	-- }
}

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
  

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj)
            ESX = obj
        end)
        Citizen.Wait(10)
    end


	while not data.started do
		data.started = true
		if data.player.ped == nil then
			data.started = false
		end


		if data.player.coords == nil then
			data.started = false
		end

		Citizen.Wait(1000)
	end
end)


Citizen.CreateThread(function()
    while true do
        -- Coisas que não precisam de correr muito frequentemente
        data.player.ped = GetPlayerPed(-1)
        Citizen.Wait(5000)
    end
end)

Citizen.CreateThread(function()
    while true do
		if data.player.ped ~= nil then
			local vehicle = GetVehiclePedIsIn(data.player.ped, false)

			-- Player Coords

			local coords = GetEntityCoords(data.player.ped)

			data.player.coords = coords

			-- Vehicle

			if vehicle ~= 0 then
				data.player.vehicle = {
					isInside = true,
					entity = vehicle
				}
			elseif vehicle == 0 and data.player.vehicle ~= nil and data.player.vehicle.entity ~= nil then
				data.player.vehicle = {
					isInside = false,
					entity = nil
				}
			end
		end

		Citizen.Wait(1000)
    end
end)

DrawText3D = function(x, y, z, text)
    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)
 
    local scale = (1/dist)*2
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
   
    if onScreen then
        SetTextScale(0.0*scale, 0.2*scale)
        SetTextFont(0)
        SetTextProportional(1)
        -- SetTextScale(0.0, 0.55)
        SetTextColour(255, 255, 255, 255)
        SetTextDropshadow(0, 0, 0, 0, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end

function round(num, numDecimalPlaces)
	local mult = 10^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end

Citizen.CreateThread(function()
	while true do
		local sleep = 1000
		if data.started then
			for ispot, spot in pairs(Config.dominationSpots) do
				local distance = #(spot.coords - data.player.coords) 
				if distance < 10 and not isDead then
					sleep = 1
	
					-- Vê blip
					if dominationSpots[ispot] ~= nil then
						local tspot = dominationSpots[ispot]
						if tspot.player.isOwner or tspot.player.joined then
							if not tspot.hasStarted then
								DrawText3D(spot.coords.x, spot.coords.y, spot.coords.z, 'Espera algum tempo')
							end
						else
							DrawText3D(spot.coords.x, spot.coords.y, spot.coords.z, 'Clica [E] para te juntares')
							if distance < 2 then
								if IsControlJustPressed(0, Keys['E']) and not data.player.vehicle.isInside then
									TriggerServerEvent('RBRPDominacao:join', ispot)
								end
							end
						end
					else
						DrawText3D(spot.coords.x, spot.coords.y, spot.coords.z, 'Clica ~r~[E]~w~ para começar a dominação')
						if distance < 2 then
							if IsControlJustPressed(0, Keys['E']) and not data.player.vehicle.isInside then
								TriggerServerEvent('RBRPDominacao:start', ispot)
							end
						end

					end
				end
			end
		end

		Citizen.Wait(sleep)
	end
end)

function getLengthOfArray(arr)
    local len = 0
    for _,__ in pairs(arr) do
        len = len + 1
    end
    return len
end

Citizen.CreateThread(function()
	while true do
		local sleep = 1000
		if data.started then
			if getLengthOfArray(dominationSpots) > 0 then
				for ispot, spot in pairs(dominationSpots) do
					if spot.player.isOwner or spot.player.joined then
						if #(Config.dominationSpots[ispot].coords - data.player.coords) > 100 then
							TriggerServerEvent('RBRPDominacao:leftArea', ispot)
							sleep = 5000
						end
					end
					
				end
			end
		end
		

		Citizen.Wait(sleep)
	end
end)



function createRedZone(coords, spotID)
	local blip = AddBlipForRadius(coords.x, coords.y, coords.z , 75.0) -- you can use a higher number for a bigger zone
	SetBlipHighDetail(blip, true)
	SetBlipColour(blip, 1)
	SetBlipAlpha (blip, 128)
	dominationSpots[spotID].blip = blip
end


AddEventHandler('playerSpawned', function(spawn)
	isDead = false
end)

AddEventHandler('esx:onPlayerDeath', function(data)
	isDead = true
	for ispot,spot in pairs(dominationSpots) do
		if spot.player.isOwner or spot.player.joined then
			TriggerServerEvent('RBRPDominacao:leftArea', ispot)
		end
	end
end)


RegisterNetEvent('RBRPDominacao:recieveData')
AddEventHandler('RBRPDominacao:recieveData', function(type, data)
	if type == 'create' then
		dominationSpots[data.dominationSpotID] = {
			player = {
				isOwner = true,
				joined = false
			},

			hasStarted = false,
			radius = data.radius
		}
	elseif type == 'join' then
		dominationSpots[data.dominationSpotID] = {
			player = {
				isOwner = false,
				joined = true
			},

			hasStarted = false,
			radius = data.radius
		}
	elseif type == 'createToOthers' then
		if dominationSpots[data.dominationSpotID] == nil then
			dominationSpots[data.dominationSpotID] = {
				player = {
					isOwner = false,
					joined = false
				},

				hasStarted = false,
				radius = data.radius
			}
		end
	elseif type == 'start' then
		if dominationSpots[data.dominationSpotID] == nil then
			dominationSpots[data.dominationSpotID].hasStarted = true
			if dominationSpots[data.dominationSpotID] ~= nil then
				if dominationSpots[data.dominationSpotID].isOwner or dominationSpots[data.dominationSpotID].joined then
					startTimer(data.ms/1000)
				end
			end
		end
	elseif type == 'stop' then
		if dominationSpots[data.dominationSpotID].blip ~= nil then
			RemoveBlip(dominationSpots[data.dominationSpotID].blip)
		end
		timerValid = false
		dominationSpots[data.dominationSpotID] = nil
	elseif type == 'createBlip' then
		createRedZone(Config.dominationSpots[data.dominationSpotID].coords, data.dominationSpotID)
	end
end)

function drawLevel(r, g, b, a, t)
	SetTextFont(4)
	SetTextProportional(1)
	SetTextScale(0.5, 0.5)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0, 255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()

	BeginTextCommandDisplayText("STRING")
	AddTextComponentSubstringPlayerName(t)
	EndTextCommandDisplayText(0.175, 0.92)
end

function startTimer(seconds)
	local seconds = seconds
	while timerValid or seconds > 0 do
		drawLevel(255, 255, 255, 255, 'Faltam: ' .. seconds .. "s" )
		Citizen.Wait(1000)
	end

	timerValid = false

end


-- 	player = {
	-- 		isOwner = false,
	-- 		joined = false
	-- 	},
		

	-- }











function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
	if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end