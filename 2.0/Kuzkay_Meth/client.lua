local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
local started = false
local displayed = false
local progress = 0
local CurrentVehicle 
local pause = false
local selection = 0
local quality = 0
ESX = nil


local LastCar

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx_putadametanfetaminamethcar:stop')
AddEventHandler('esx_putadametanfetaminamethcar:stop', function()
	started = false
	DisplayHelpText("Paraste de processar...")
	FreezeEntityPosition(LastCar, false)
end)
RegisterNetEvent('esx_putadametanfetaminamethcar:stopfreeze')
AddEventHandler('esx_putadametanfetaminamethcar:stopfreeze', function(id)
	FreezeEntityPosition(id, false)
end)
RegisterNetEvent('esx_putadametanfetaminamethcar:notify')
AddEventHandler('esx_putadametanfetaminamethcar:notify', function(message)
	ESX.ShowNotification(message)
end)

local neededItems = { -- ISTO TAMBÉM TEM NO SERVER-SIDE PARA REMOVER OS ITEMS DOS PLAYERS
	['acidosulfurico'] = {min = 24},
	['acetona'] = {min = 48},
	['ephedrine'] = {min = 48},
	['hidroxidodesodio'] = {min = 24},
	['bateriadelitio'] = {min = 24},
	['oleotravoes'] = {min = 24},
	['sacodroga'] = {min = 24},
}

RegisterNetEvent('esx_putadametanfetaminamethcar:startprod')
AddEventHandler('esx_putadametanfetaminamethcar:startprod', function()
	local playerPed = GetPlayerPed(-1)
	CurrentVehicle = GetVehiclePedIsUsing(playerPed)
	car = GetVehiclePedIsIn(playerPed, false)
	LastCar = GetVehiclePedIsUsing(playerPed)
	local pos = GetEntityCoords(playerPed)
	local model = GetEntityModel(CurrentVehicle)
	local modelName = GetDisplayNameFromVehicleModel(model)
	
	if modelName == 'JOURNEY' and car then
		if GetPedInVehicleSeat(car, -1) == playerPed then
			if not started and pos.y >= 3500 then
				started = true
				FreezeEntityPosition(CurrentVehicle, true)
				ESX.ShowNotification("A produção da metanfetamina começou")	
				SetPedIntoVehicle(GetPlayerPed(-1), CurrentVehicle, 3)
				SetVehicleDoorOpen(CurrentVehicle, 2)
				for itemName,itemDetails in pairs(neededItems) do
					TriggerServerEvent('esx_putadametanfetaminamethcar:removeItem', itemName, itemDetails.min)
				end
			else
				ESX.ShowNotification('Não achas que estás demasiado perto da cidade? A bongó vai te canar assim. Tens que ir mais para o norte para "cozinhar"')
			end
		else
			DisplayHelpText('Ja está gente no carro.')
		end
	end
end)

RegisterNetEvent('esx_putadametanfetaminamethcar:blowup')
AddEventHandler('esx_putadametanfetaminamethcar:blowup', function(posx, posy, posz)
	AddExplosion(posx, posy, posz + 2,23, 20.0, true, false, 1.0, true)
	if not HasNamedPtfxAssetLoaded("core") then
		RequestNamedPtfxAsset("core")
		while not HasNamedPtfxAssetLoaded("core") do
			Citizen.Wait(1)
		end
	end
	SetPtfxAssetNextCall("core")
	local fire = StartParticleFxLoopedAtCoord("ent_ray_heli_aprtmnt_l_fire", posx, posy, posz-0.8 , 0.0, 0.0, 0.0, 0.8, false, false, false, false)
	Citizen.Wait(6000)
	StopParticleFxLooped(fire, 0)
	
end)


RegisterNetEvent('esx_putadametanfetaminamethcar:smoke')
AddEventHandler('esx_putadametanfetaminamethcar:smoke', function(posx, posy, posz, bool)
	if bool == 'a' then
		if not HasNamedPtfxAssetLoaded("core") then
			RequestNamedPtfxAsset("core")
			while not HasNamedPtfxAssetLoaded("core") do
				Citizen.Wait(1)
			end
		end
		SetPtfxAssetNextCall("core")
		local smoke = StartParticleFxLoopedAtCoord("exp_grd_flare", posx, posy, posz + 1.7, 0.0, 0.0, 0.0, 2.0, false, false, false, false)
		SetParticleFxLoopedAlpha(smoke, 0.8)
		SetParticleFxLoopedColour(smoke, 0.0, 0.0, 0.0, 0)
		Citizen.Wait(22000)
		StopParticleFxLooped(smoke, 0)
	else
		StopParticleFxLooped(smoke, 0)
	end

end)
RegisterNetEvent('esx_putadametanfetaminamethcar:drugged')
AddEventHandler('esx_putadametanfetaminamethcar:drugged', function()
	SetTimecycleModifier("drug_drive_blend01")
	SetPedMotionBlur(GetPlayerPed(-1), true)
	SetPedMovementClipset(GetPlayerPed(-1), "MOVE_M@DRUNK@SLIGHTLYDRUNK", true)
	SetPedIsDrunk(GetPlayerPed(-1), true)

	Citizen.Wait(300000)
	ClearTimecycleModifier()
end)

local playerPed = nil 
Citizen.CreateThread(function()
	while true do
		playerPed = GetPlayerPed(-1)
		Citizen.Wait(5000)
	end
end)


local isShowingUI = false

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)
		
		playerPed = GetPlayerPed(-1)
		local pos = GetEntityCoords(GetPlayerPed(-1))
		if not IsPedInAnyVehicle(playerPed) and started then
			started = false
			displayed = false
			TriggerEvent('esx_putadametanfetaminamethcar:stop')
			FreezeEntityPosition(LastCar,false)
		end
		
		if started then
			if progress < 99 then
				Citizen.Wait(6000)
				if not pause and IsPedInAnyVehicle(playerPed) then
					progress = progress +  1
					ESX.ShowNotification('Progresso da Meta: ' .. progress .. '%')
					Citizen.Wait(6000) 
				end

				--
				--   EVENT 1
				--
				if progress == 6 or progress == 7 or progress == 8 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "O tubo do gás tem um furo, o que fazes?",
							dados = {
								"Arranjar com fita-cola",
								"Sa foda, deixa assim",
								"Substituí-lo"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('A fita-cola por acaso resultou, estas com sorte')
								quality = quality - 3
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Fizeste merda, foi tudo com o caralho...')
								TriggerServerEvent('esx_putadametanfetaminamethcar:blow', pos.x, pos.y, pos.z)
								SetVehicleEngineHealth(CurrentVehicle, 0.0)
								quality = 0
								progress = 0
								started = false
								ApplyDamageToPed(GetPlayerPed(-1), 10, false)
					
							elseif result == 2 then
								ESX.ShowNotification('Bom trabalho, o tubo estava todo fudido')
								quality = quality + 5
								progress = progress + 1
							end
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 5
				--
				if progress == 30 or progress == 31 or progress == 32 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Entornas-te uma garrafa de acetona no chão, o que fazes?",
							dados = {
								"Abrir a janela para arejar a caravana",
								"Sa foda, quão mau pode ser?",
								"Colocar uma máscara c filtro de ar"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Abriste a janela para deixar entrar o ar')
								quality = quality - 1
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Apanhaste a moca por inalar demasiada acetona')
								TriggerEvent('esx_putadametanfetaminamethcar:drugged')
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Podia ser pior... digo eu')
								SetPedPropIndex(playerPed, 1, 26, 7, true)
								progress = progress + 1
							end

							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 2
				--
				if progress == 38 or progress == 39 or progress == 40 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "A Meta está a ficar solida demasiado rápido, o que fazes?",
							dados = {
								"Aumentar a pressão",
								"Aumentar a temperatura",
								"Baixar a pressão"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Aumentaste a temperatura e o gás começou a escapar, voltaste a baixar e tudo parece bem por agora')
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Aumentar a temperatura ajudou...')
								quality = quality + 5
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Baixar a pressão só piorou as coisas...')
								quality = quality - 4
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 8 - 3
				--
				if progress == 41 or progress == 42 or progress == 43 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Usas acidentalmente demasiada acetona, o que fazes?",
							dados = {
								"Nada, caguei",
								"Tentar remover o excesso com uma seringa",
								"Meter mais litio para compensar"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Que cheiro a acetona que vem desta Meta merdosa')
								quality = quality - 3
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Acho que funcionou, mas continua a ser demais')
								quality = quality - 1
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Conseguiste equilibrar os quimicos com sucesso')
								quality = quality + 3
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 3
				--
				if progress == 46 or progress == 47 or progress == 48 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Encontraste uma beca de corante, o que fazes?",
							dados = {
								"Meter na Meta",
								"Guardar",
								"Beber"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Boa ideia, os drogados gostam de cores')
								quality = quality + 4
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Realmente talvez fosse dar cabo da Meta')
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('És uma beca burro por teres bebido isso, mas pronto')
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 4
				--
				if progress == 55 or progress == 56 or progress == 57 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "O filtro está entupido, o que fazes?",
							dados = {
								"Limpar o filtro com ar comprimido",
								"Substituir o filtro",
								"Limpar com uma escova de dentes"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('O ar comprimido salpicou te todo de Meta')
								quality = quality - 2
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Trocar o filtro foi a melhor das hipoteses')
								quality = quality + 3
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Funcionou bastante bem, mas continua uma beca sujo')
								quality = quality - 1
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 5
				--
				if progress == 58 or progress == 59 or progress == 60 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Entornas-te uma garrafa de acetona no chão, o que fazes?",
							dados = {
								"Abrir a janela para arejar a caravana",
								"Sa foda, quão mau pode ser?",
								"Colocar uma máscara c filtro de ar"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Abriste a janela para deixar entrar o ar')
								quality = quality - 1
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Apanhaste a moca por inalar demasiada acetona')
								TriggerEvent('esx_putadametanfetaminamethcar:drugged')
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Podia ser pior... digo eu')
								SetPedPropIndex(playerPed, 1, 26, 7, true)
								progress = progress + 1
							end

							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 1 - 6
				--
				if progress == 63 or progress == 64 or progress == 54 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "O tubo do gás tem um furo, o que fazes?",
							dados = {
								"Arranjar com fita-cola",
								"Sa foda, deixa assim",
								"Substituí-lo"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('A fita-cola por acaso resultou, estas com sorte')
								quality = quality - 3
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Fizeste merda, foi tudo com o caralho...')
								TriggerServerEvent('esx_putadametanfetaminamethcar:blow', pos.x, pos.y, pos.z)
								SetVehicleEngineHealth(CurrentVehicle, 0.0)
								quality = 0
								progress = 0
								started = false
								ApplyDamageToPed(GetPlayerPed(-1), 10, false)
					
							elseif result == 2 then
								ESX.ShowNotification('Bom trabalho, o tubo estava todo fudido')
								quality = quality + 5
								progress = progress + 1
							end
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 4 - 7
				--
				if progress == 71 or progress == 72 or progress == 73 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "O filtro está entupido, o que fazes?",
							dados = {
								"Limpar o filtro com ar comprimido",
								"Substituir o filtro",
								"Limpar com uma escova de dentes"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('O ar comprimido salpicou te todo de Meta')
								quality = quality - 2
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Trocar o filtro foi a melhor das hipoteses')
								quality = quality + 3
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Funcionou bastante bem, mas continua uma beca sujo')
								quality = quality - 1
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 8
				--
				if progress == 76 or progress == 77 or progress == 78 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Entornas-te uma garrafa de acetona no chão, o que fazes?",
							dados = {
								"Abrir a janela para arejar a caravana",
								"Sa foda, quão mau pode ser?",
								"Colocar uma máscara com filtro de ar"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Abriste a janela para deixar entrar o ar')
								quality = quality - 1
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Apanhaste a moca por inalar demasiada acetona')
								TriggerEvent('esx_putadametanfetaminamethcar:drugged')
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Podia ser pior... digo eu')
								SetPedPropIndex(playerPed, 1, 26, 7, true)
								progress = progress + 1
							end

							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 9
				--
				if progress == 82 or progress == 83 or progress == 84 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Tás com uma enorme vontade de cagar, o que fazes?",
							dados = {
								"Tentas aguentar",
								"Sair para cagar",
								"Cagar cá dentro"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Bom trabalho, droga primeiro, cagar depois')
								quality = quality + 1
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('Enquanto estavas na rua esta merda ferveu demais...')
								quality = quality - 2
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('O ar cheira a merda, a caravana cheira a merda e a Meta cheira a merda')
								quality = quality - 1
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				--
				--   EVENT 10
				--
				if progress == 88 or progress == 89 or progress == 90 then
					pause = true
					if not isShowingUI then
						isShowingUI = true
						exports['RBRPdecisions']:openDecisionGame({
							title = "Queres cortar a Meta com bocados de vidro para teres mais?",
							dados = {
								"Sim!",
								"Não",
								"E se eu adicionar Meta ao vidro?"
							}
						}, function(result)
							if result == 0 then
								ESX.ShowNotification('Conseguiste mais umas gramas de Meta com esse truque')
								quality = quality + 1
								progress = progress + 1
																
							elseif result == 1 then
								ESX.ShowNotification('És um bom traficante, tens um produto de qualidade')
								quality = quality + 1
								progress = progress + 1
							
							elseif result == 2 then
								ESX.ShowNotification('Meteste demasiado, agora é mais vidro que Meta')
								quality = quality - 1
								progress = progress + 1
							end
							
							pause = false
							isShowingUI = false
						end)
					end
				end
				
				
				if IsPedInAnyVehicle(playerPed) then
					TriggerServerEvent('esx_putadametanfetaminamethcar:make', pos.x,pos.y,pos.z)
					if not pause then
						selection = 0
						quality = quality + 1

						progress = progress +  math.random(1, 2)
						ESX.ShowNotification('Progresso da Meta: ' .. progress .. '%')
					end
				else
					progress = 0
					TriggerEvent('esx_putadametanfetaminamethcar:stop')
				end

			else
				TriggerEvent('esx_putadametanfetaminamethcar:stop')
				progress = 100
				ESX.ShowNotification('Progresso da Meta: ' .. progress .. '%')
				ESX.ShowNotification('Acabaste de processar.')
				progress = 0
				TriggerServerEvent('esx_putadametanfetaminamethcar:finish', quality)
				quality = 0

				FreezeEntityPosition(LastCar, false)
			end	
		end
	end
end)


Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1000)
			if IsPedInAnyVehicle(GetPlayerPed(-1)) then
			else
				if started then
					started = false
					displayed = false
					TriggerEvent('esx_putadametanfetaminamethcar:stop')
					FreezeEntityPosition(LastCar,false)
				end		
			end
	end

end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)	
		if pause == true then
			if IsControlJustPressed(0, Keys['Z']) then
				selection = 1
				ESX.ShowNotification('Selected option number 1')
			end
			if IsControlJustPressed(0, Keys['N']) then
				selection = 2
				ESX.ShowNotification('Selected option number 2')
			end
			if IsControlJustPressed(0, Keys['M']) then
				selection = 3
				ESX.ShowNotification('Selected option number 3')
			end
		end
	end
end)


