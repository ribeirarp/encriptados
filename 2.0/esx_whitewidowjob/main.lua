local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local showBlips 			= false
local needToCreateThread 	= true
local ESX                   = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

-- Funções


local function ftlibsBlips(showBlips, PlayerData)
	for k,v in pairs(ConfigWhiteWidow.whiteWidowStations) do
		Citizen.Wait(5000)

		if v.Armories then
			for i=1, #v.Armories, 1 do
				exports.ft_libs:SwitchArea("esx_whitewidowjob:BlipArmories_" .. v.Armories[i].x, showBlips)
			end
		end

		if v.BossActions then
			for i=1, #v.BossActions, 1 do
				if PlayerData.job.grade_name == "dono" and showBlips then
					exports.ft_libs:SwitchArea("esx_whitewidowjob:BlipBossActions_" .. v.BossActions[i].x, showBlips)
				else
					exports.ft_libs:SwitchArea("esx_whitewidowjob:BlipBossActions_" .. v.BossActions[i].x, false)
				end
			end
		end

	end

	-- Key Controls
	--if needToCreateThread == true then
end



local function exitmarker()
	TriggerEvent('luke_textui:HideUI')
end



RegisterNetEvent('esx_whitewidowjob:openInv')
AddEventHandler('esx_whitewidowjob:openInv', function(id)
	exports["mf-inventory"]:openOtherInventory("esx_whitewidowjob:inv1")
end)


RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job

	if PlayerData.job.name == "whitewidow" then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
	Citizen.Wait(3000)
end)


RegisterNetEvent("ft_libs:OnClientReady")
AddEventHandler('ft_libs:OnClientReady', function()
	exports.ft_libs:AddBlip("whitewidow_blipmapa", {
		x = -1170.41, 
		y = -1572.53,
		z = 4.66,
		text = "White Widow",
		imageId = 140,
		colorId = 2
	})

	

	exports['bt-target']:AddBoxZone("WhiteWidow_Balanca1", vector3(-1169.32, -1571.95, 4.61), 0.8, 0.4, {
		name = "Balança",
		heading = 125.0,
		debugPoly = false,
		minZ = 5.18-1.5,
		maxZ = 5.18+1.5
	}, {
		options = {{
			event = "esx_whitewidowjob:tabuleiro",
			icon = "fas fa-temperature-high",
			label = "Balança"
		}},
		job = {"all"},
		distance = 1.5
	}) 

	exports['bt-target']:AddBoxZone("WhiteWidow_Balanca2", vector3(-1168.86, -1572.40, 5.00), 0.8, 0.4, {
		name = "Balança",
		heading = 125.0,
		debugPoly = false,
		minZ = 5.18-1.5,
		maxZ = 5.18+1.5
	}, {
		options = {{
			event = "esx_whitewidowjob:tabuleiro",
			icon = "fas fa-weight",
			label = "Balança"
		}},
		job = {"all"},
		distance = 1.5
	}) 
	
	exports['bt-target']:AddBoxZone("WhiteWidow_Craft", vector3(-1173.04, -1577.26, 4.433), 0.8, 0.4, {
		name = "Mesa",
		heading = 125.0,
		debugPoly = false,
		minZ = 5.18-1.5,
		maxZ = 5.18+1.5
	}, {
		options = {
			{
				event = "esx_whitewidowjob:craftfertilizante",
				icon = "fas fa-tools",
				label = "Fazer fertilizantes"
			},
			{
				event = "esx_whitewidowjob:craftmortalhasefiltros",
				icon = "fas fa-tools",
				label = "Fazer mortalhas e filtros"
			},
		},
		job = {"whitewidow"},
		distance = 1.5
	})


	exports['bt-target']:AddBoxZone("WhiteWidow_TP1", vector3(-1126.32, -1446.99, 5.38), 0.8, 0.4, {
		name = "tp1",
		heading = 125.0,
		debugPoly = false,
		minZ = 5.5-1.5,
		maxZ = 5.5+1.5
	}, {
		options = {
			{
				event = "esx_whitewidowjob:TP1",
				icon = "fas fa-door-open",
				label = "Subir ao telhado"
			}
		},
		job = {"whitewidow"},
		distance = 1.5
	})
	exports['bt-target']:AddBoxZone("WhiteWidow_TP2", vector3(-1125.42, -1441.89, 12.67), 0.8, 0.4, {
		name = "tp2",
		heading = 125.0,
		debugPoly = false,
		minZ = 12-1.5,
		maxZ = 12+1.5
	}, {
		options = {
			{
				event = "esx_whitewidowjob:TP2",
				icon = "fas fa-door-open",
				label = "Descer do telhado"
			}
		},
		job = {"whitewidow"},
		distance = 1.5
	})

	exports['bt-target']:AddBoxZone("WhiteWidow_Loja", vector3(2487.52, 3726.38, 44.19), 0.8, 0.4, {
		name = "Loja White Widow",
		heading = 36.0,
		debugPoly = false,
		minZ = 44-1.5,
		maxZ = 44+1.5
	}, {
		options = {
			{
				event = "esx_whitewidowjob:abrirloja",
				icon = "fas fa-shopping-basket",
				label = "Comprar coisas do drogado"
			}
		},
		job = {"whitewidow"},
		distance = 1.5
	})

	
	for k,v in pairs(ConfigWhiteWidow.whiteWidowStations) do
		for i=1, #v.Armories, 1 do
			exports.ft_libs:AddArea("esx_whitewidowjob:BlipArmories_" .. v.Armories[i].x, {
				enable = false,
				marker = {
					type = ConfigWhiteWidow.MarkerType,
					weight = 1,
					height = 1,
					red = ConfigWhiteWidow.MarkerColor.r,
					green = ConfigWhiteWidow.MarkerColor.g,
					blue = ConfigWhiteWidow.MarkerColor.b,
					showDistance = 5,
					alpha = 50
				},
				trigger = {
					weight = 1,
					exit = {
						callback = exitmarker
					},
					active = {
						callback = function()
							exports.ft_libs:HelpPromt("Inventário")
							if IsControlJustReleased(0, Keys["E"]) then
								OpenArmoryMenu(v)
							end
						end,
					},
				},
				locations = {
					{
						x = v.Armories[i].x,
						y = v.Armories[i].y,
						z = v.Armories[i].z+1,
					},
				},
			})
		end

		for i=1, #v.BossActions, 1 do
			exports['bt-target']:AddBoxZone("WhiteWidow_Boss", vector3(v.BossActions[i].x, v.BossActions[i].y, v.BossActions[i].z), 0.8, 0.4, {
				name = "Boss",
				heading = 125.0,
				debugPoly = false,
				minZ = v.BossActions[i].z-1.5,
				maxZ = v.BossActions[i].z+1.5
			}, {
				options = {
					{
						event = "esx_whitewidowjob:openBossMenu",
						icon = "fas fa-clipboard",
						label = "Menu Patrão"
					},
					{
						event = "esx_whitewidowjob:openInv",
						icon = "fas fa-boxes",
						label = "Inventário"
					}
				},
				job = {"whitewidow"},
				distance = 1.5
			}) 
		end
	end
end)


RegisterNetEvent('esx_whitewidowjob:abrirloja')
AddEventHandler('esx_whitewidowjob:abrirloja', function()
	exports["mf-inventory"]:openOtherInventory("esx_shops:whitewidow")
end)

RegisterNetEvent('esx_whitewidowjob:TP1')
AddEventHandler('esx_whitewidowjob:TP1', function()
	SetEntityCoords(GetPlayerPed(-1), vector3(-1125.16, -1441.78, 12.45))
end)

RegisterNetEvent('esx_whitewidowjob:TP2')
AddEventHandler('esx_whitewidowjob:TP2', function()
	SetEntityCoords(GetPlayerPed(-1), vector3(-1126.39, -1447.38, 5.05))
end)


local craftingFertilizante = false
RegisterNetEvent('esx_whitewidowjob:craftfertilizante')
AddEventHandler('esx_whitewidowjob:craftfertilizante', function()
	if not craftingMortalhasEFiltros and not craftingFertilizante then
		craftingFertilizante = true
		exports["mf-inventory"]:getInventoryItems(PlayerData.identifier,function(items)
			local hasItems = {
				['ossos'] = {
					has = false,
					qtt = 1
				},
				['fertilizantes'] = {
					has = false,
					qtt = 2
				}
			}

			for iitem,item in pairs(items) do
				if hasItems[item.name] ~= nil then
					if item.count >=  hasItems[item.name].qtt then
						hasItems[item.name].has = true
					end
				end
			end

			if hasItems['ossos'].has and hasItems['fertilizantes'].has then
				exports['RBRPUser']:playAnim({
					animDetails = {
						dict = "anim@amb@clubhouse@tutorial@bkr_tut_ig3@",
						anim = "machinic_loop_mechandplayer",
						duration = 5000
					},

					progressBar = {
						activate = true,
						duration = 5000,
						text = "A fazer . . ."
					},
			
					options = {
						freezePosition = true
					},
			
					blockKeys = {}
				})

				TriggerServerEvent('esx_whitewidowjob:craftfertilizante')
			else
				if not hasItems['ossos'].has then
					exports['mythic_notify']:SendAlert('error','Não tens ossos suficientes')

				elseif not hasItems['fertilizantes'].has then
					exports['mythic_notify']:SendAlert('error','Não tens adubo suficiente')

				end
			end

			craftingFertilizante = false
		end)
	else
		exports['mythic_notify']:SendAlert('error','Viras-te um polvo?')

	end
end)


local craftingMortalhasEFiltros = false
RegisterNetEvent('esx_whitewidowjob:craftmortalhasefiltros')
AddEventHandler('esx_whitewidowjob:craftmortalhasefiltros', function()
	if not craftingMortalhasEFiltros and not craftingFertilizante then
		craftingMortalhasEFiltros = true
		exports["mf-inventory"]:getInventoryItems(PlayerData.identifier,function(items)
			local hasItem = false
			for iitem,item in pairs(items) do
				if item.name == "fabric" and item.count >= 5 then
					hasItem = true
				end
			end
	
			if hasItem then
				exports['RBRPUser']:playAnim({
					animDetails = {
						dict = "anim@amb@clubhouse@tutorial@bkr_tut_ig3@",
						anim = "machinic_loop_mechandplayer",
						duration = 5000
					},
	
					progressBar = {
						activate = true,
						duration = 5000,
						text = "A fazer . . ."
					},
			
					options = {
						freezePosition = true
					},
			
					blockKeys = {}
				})

				craftingMortalhasEFiltros = false
				TriggerServerEvent('esx_whitewidowjob:craftmortalhasefiltros')
			else
				exports['mythic_notify']:SendAlert('error','Não tens pano suficiente')
			end
		end)
	else
		exports['mythic_notify']:SendAlert('error','Viras-te um polvo?')
	end
end)

RegisterNetEvent('esx_whitewidowjob:tabuleiro')
AddEventHandler('esx_whitewidowjob:tabuleiro', function()
    exports["mf-inventory"]:openOtherInventory('esx_whitewidowjob:tabuleirosexy')
end)

RegisterNetEvent('esx_whitewidowjob:openBossMenu')
AddEventHandler('esx_whitewidowjob:openBossMenu', function()
	if PlayerData.job.grade_name == "boss" then
		ESX.UI.Menu.CloseAll()
		TriggerEvent('esx_society:openBossMenu', 'whitewidow', function(data, menu)
			menu.close()
		end, { wash = false })
	else
		exports['mythic_notify']:SendAlert('error', "Não tens permissão para fazer a gestão da empresa")
	end
	
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer

	if PlayerData.job.name == "whitewidow" then
		showBlips = true
	else
		showBlips = false
	end

	ftlibsBlips(showBlips, PlayerData)
end)


local npcspawned = false
local npc = nil

function createNPC(coords, heading)
	local hash = GetHashKey('cs_omega')
    if not HasModelLoaded(hash) then
        RequestModel(hash)
        Wait(10)
    end
    while not HasModelLoaded(hash) do
        Wait(10)
    end
    
    npcspawned = true
    npc = CreatePed(5, hash, coords, heading, false, false)
    FreezeEntityPosition(npc, true)
    SetEntityInvincible(npc, true)
    SetBlockingOfNonTemporaryEvents(npc, true)
end

local npcoords = vector3(2487.63, 3726.16, 42.91)
Citizen.CreateThread(function()
    while true do
		local plycoords = exports['RBRPUser']:getCoords()
		if plycoords ~= nil then
			local dst = #(npcoords - plycoords)
			if dst < 200 and npcspawned == false then
				createNPC(npcoords, 36.85)
				npcspawned = true
			end
			if dst >= 201 and npcspawned then
				npcspawned = false
				DeleteEntity(npc)
			end
		end
		
        Citizen.Wait(3000)
    end
end)


function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
	if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end