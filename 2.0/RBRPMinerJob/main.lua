ESX = nil
local PlayerData = {}
local isMenuOn = false

local shopCostMenu = 0
local goldOresCount = 0
local pickaxeCounter = 0
local ironOresCount = 0
local impacts = 0

local event_destination = nil
local isMining = false
local newSpawnReady = true
NPCInfo = {
    coords = vector3(-600.53, 2091.23, 130.51),
    heading = 348.00,
    timeout = 15 * 60 * 1000, -- primeiro número = minutos

    informations = {
        [0] = 'Precisas de picareta, lanterna e capacete para iniciar o trabalho',
        [1] = 'Lembra-te que as pedras limpinhas são mais giras!'
    }
}




local npcspawned = false
local npc = nil

function createNPC(coords, heading)
	local hash = GetHashKey('s_m_y_construct_01')
    if not HasModelLoaded(hash) then
        RequestModel(hash)
        Wait(10)
    end
    
    while not HasModelLoaded(hash) do
        Wait(10)
    end
    
    npcspawned = true
    npc = CreatePed(5, hash, coords, heading, false, false)
    FreezeEntityPosition(npc, true)
    SetEntityInvincible(npc, true)
    SetBlockingOfNonTemporaryEvents(npc, true)
end


local cooldown = false
RegisterNetEvent('miner:talkWithNPC')
AddEventHandler('miner:talkWithNPC', function()
	if not cooldown then
		cooldown = true
		TriggerEvent('luke_textui:ShowUI', NPCInfo.informations[math.random(0, #NPCInfo.informations)])
		Citizen.Wait(5000)
		TriggerEvent('luke_textui:HideUI')
		SetTimeout(NPCInfo.timeout, function()
			cooldown = false
		end)
	else
		TriggerEvent('luke_textui:ShowUI', 'Agora pera, tenho de mamar uma cerveja')
		Citizen.Wait(5000)
		TriggerEvent('luke_textui:HideUI')
	end
end)

Citizen.CreateThread(function()
    while true do
        local dst = #(NPCInfo.coords - GetEntityCoords(GetPlayerPed(-1)))
        if dst < 200 and npcspawned == false then
            createNPC(NPCInfo.coords, NPCInfo.heading)
            npcspawned = true
        end
        if dst >= 201 and npcspawned then
            npcspawned = false
            DeleteEntity(npc)
        end
        Citizen.Wait(3000)
    end
end)




local rocks = {
    [0] =  {
        coords = vector3(-590.17, 2073.83, 131.83),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining0",
        heading = 280.63
    },
    [1] =  {
        coords = vector3(-592.03, 2066.24, 131.62),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining1",
        heading = 96.38
    },
    [2] =  {
        coords = vector3(-587.17, 2058.84, 131.02),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining2",
        heading = 280.63

    },
    [3] =  {
        coords = vector3(-589.80, 2051.57, 130.55),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining3",
        heading = 96.38
    },
    [4] =  {
        coords = vector3(-583.89, 2044.34, 129.75),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining4",
        heading = 280.63
    },
    [5] =  {
        coords = vector3(-583.75, 2036.56, 129.29),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining5",
        heading = 96.38
    },
    [6] =  {
        coords = vector3(-576.04, 2031.78, 128.79),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining6",
        heading = 280.63
    },
    [7] =  {
        coords = vector3(-574.64, 2024.19, 128.28),
        isMined = false,
        minZ = 127.0,
        maxZ = 133.0,
        name = "Mining7",
        heading = 96.38
    }
}

local rocks_wash = {
    [0] =  {
        coords = vector3(1982.71, 584.50, 160.14),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash0"
    },
    [1] =  {
        coords = vector3(1981.97, 583.18, 160.14),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash1"
    },
    [2] =  {
        coords = vector3(1981.05, 581.61, 160.14),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash2"
    },
    [3] =  {
        coords = vector3(1983.45, 586.68, 160.09),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash3"
    },
    [4] =  {
        coords = vector3(1983.86, 588.16, 160.05),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash4"
    },
    [5] =  {
        coords = vector3(1983.69, 589.12, 160.01),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash5"
    },
    [6] =  {
        coords = vector3(1981.10, 580.55, 160.13),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash6"
    },
    [7] =  {
        coords = vector3(1979.99, 576.81, 160.07),
        minZ = 150.0,
        maxZ = 165.0,
        name = "Wash7"
    }
}

local rocks_melt = {
    [0] =  {
        coords = vector3(1085.22, -2002.82, 31.61),
        minZ = 29.0,
        maxZ = 32.0,
        name = "Melt0"
    },
    [1] =  {
        coords = vector3(1087.64, -2002.16, 31.19),
        minZ = 29.0,
        maxZ = 32.0,
        name = "Melt1"
    },
    [2] =  {
        coords = vector3(1087.65, -2004.20, 31.52),
        minZ = 29.0,
        maxZ = 32.0,
        name = "Melt3"
    }
}


local Keys = {

    ["ESC"] = 322,
    ["F1"] = 288,
    ["F2"] = 289,
    ["F3"] = 170,
    ["F5"] = 166,
    ["F6"] = 167,
    ["F7"] = 168,
    ["F8"] = 169,
    ["F9"] = 56,
    ["F10"] = 57,

    ["~"] = 243,
    ["1"] = 157,
    ["2"] = 158,
    ["3"] = 160,
    ["4"] = 164,
    ["5"] = 165,
    ["6"] = 159,
    ["7"] = 161,
    ["8"] = 162,
    ["9"] = 163,
    ["-"] = 84,
    ["="] = 83,
    ["BACKSPACE"] = 177,

    ["TAB"] = 37,
    ["Q"] = 44,
    ["W"] = 32,
    ["E"] = 38,
    ["R"] = 45,
    ["T"] = 245,
    ["Y"] = 246,
    ["U"] = 303,
    ["P"] = 199,
    ["["] = 39,
    ["]"] = 40,
    ["ENTER"] = 18,

    ["CAPS"] = 137,
    ["A"] = 34,
    ["S"] = 8,
    ["D"] = 9,
    ["F"] = 23,
    ["G"] = 47,
    ["H"] = 74,
    ["K"] = 311,
    ["L"] = 182,

    ["LEFTSHIFT"] = 21,
    ["Z"] = 20,
    ["X"] = 73,
    ["C"] = 26,
    ["V"] = 0,
    ["B"] = 29,
    ["N"] = 249,
    ["M"] = 244,
    [","] = 82,
    ["."] = 81,

    ["LEFTCTRL"] = 36,
    ["LEFTALT"] = 19,
    ["SPACE"] = 22,
    ["RIGHTCTRL"] = 70,

    ["HOME"] = 213,
    ["PAGEUP"] = 10,
    ["PAGEDOWN"] = 11,
    ["DELETE"] = 178,

    ["LEFT"] = 174,
    ["RIGHT"] = 175,
    ["TOP"] = 27,
    ["DOWN"] = 173,

    ["NENTER"] = 201,
    ["N4"] = 108,
    ["N5"] = 60,
    ["N6"] = 107,
    ["N+"] = 96,
    ["N-"] = 97,
    ["N7"] = 117,
    ["N8"] = 61,
    ["N9"] = 118

}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
    PlayerData.job = job
end)

local blips = {
    {
        title = "Mina",
        colour = 22,
        id = 653,
        x = -593.95855712891,
        y = 2091.75390625,
        z = 131.71856689453
    },
    {
        title = "Lavagem da pedra",
        colour = 3,
        id = 650,
        x = 1982.89,
        y = 586.88,
        z = 161.08
    },
    {
        title = "Fundição",
        colour = 1,
        id = 648,
        x = 1091.33,
        y = -1997.54,
        z = 31.94
    }
}

Citizen.CreateThread(function()

    for _, info in pairs(blips) do
        info.blip = AddBlipForCoord(info.x, info.y, info.z)
        SetBlipSprite(info.blip, info.id)
        SetBlipDisplay(info.blip, 4)
        SetBlipScale(info.blip, 0.9)
        SetBlipColour(info.blip, info.colour)
        SetBlipAsShortRange(info.blip, true)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(info.title)
        EndTextCommandSetBlipName(info.blip)
    end

    exports['bt-target']:AddBoxZone('ped_miner', vector3(-600.499, 2091.47, 131.91), 0.8, 1.2, {
        name = 'Mineiro',
        heading = 302.679,
        debugPoly = false,
        minZ = 131.91-1.5,
        maxZ = 131.91+1.5
    }, {
        options = {{
            event = "miner:talkWithNPC",
            icon = "fas fa-gem",
            label = "Falar com o mineiro",
            params = {},
        }},
        job = {"all"},
        distance = 2
    })

	for irock, rock in pairs(rocks) do
		exports['bt-target']:AddBoxZone(rock.name, rock.coords, 0.8, 1.2, {
			name = rock.name,
			heading = 302.679,
			debugPoly = false,
			minZ = rock.minZ,
			maxZ = rock.maxZ
		}, {
			options = {{
				event = "minerarBT:minerJob",
				icon = "fas fa-gem",
				label = "Minerar",
				params = {param = irock, heading = rock.heading},
			}},
			job = {"all"},
			distance = 2
		})
	end

	for iwash, wash in pairs(rocks_wash) do
		exports['bt-target']:AddBoxZone(wash.name, wash.coords, 0.8, 1.2, {
			name = wash.name,
			heading = 302.679,
			debugPoly = false,
			minZ = wash.minZ,
			maxZ = wash.maxZ
		}, {
			options = {{
				event = "washstone:minerJob",
				icon = "fas fa-gem",
				label = "Lavar",
			}},
			job = {"all"},
			distance = 15
		})
	end

	for imeth, meth in pairs(rocks_melt) do
		exports['bt-target']:AddBoxZone(meth.name, meth.coords, 0.8, 1.2, {
			name = meth.name,
			heading = 302.679,
			debugPoly = false,
			minZ = meth.minZ,
			maxZ = meth.maxZ
		}, {
			options = {{
				event = "meltstone:minerJob",
				icon = "fas fa-gem",
				label = "Fundir",
			}},
			job = {"all"},
			distance = 8
		})
	end

	

	
	

end)

local firstMine = true
local secondMine = true
local thirdMine = true
local fourthMine = true
local fifthMine = true
local sixMine = true
local sevenMine = true
local eightMine = true
local minedSpots = 0

RegisterNetEvent("minerarBT:minerJob")
AddEventHandler("minerarBT:minerJob", function(data)
    local player = GetPlayerPed(-1)
    local playerLoc = GetEntityCoords(player)

    if isMining == false then
		
        if rocks[data.param].isMined == false then
            minedSpots = minedSpots + 1
            -- print("cheguei")
            --TriggerEvent("CheckPickaxe:minerJob")
            Citizen.Wait(500)
            exports["mf-inventory"]:getInventoryItems(PlayerData.identifier,function(items)
                local hasItem = false
                for iitem, item in pairs(items) do
                    if item.name == "picareta" and item.count >= 1 then
                        hasItem = true
                    end
                end


                if hasItem then
                    miningAnim(data.heading)
                    local randomDestroy = math.random(1, 100)
                    if randomDestroy <= 5 then
                        exports['mythic_notify']:SendAlert('error', 'Picareta partiu!', 3000)
                        TriggerServerEvent("removePickaxe:minerJob")
                    end
			        rocks[data.param].isMined = true

                else
                    exports['mythic_notify']:SendAlert('error', 'Não tens picareta!', 3000)
                end
              end)

        else
            exports['mythic_notify']:SendAlert('error', 'Já mineraste esta parte, salta para outra tono!', 2000)
        end
    else
        exports['mythic_notify']:SendAlert('error', 'Já estás a minar, não abuses!', 2000)
    end

    if minedSpots == #rocks then
		clearIsMined()
	end



end)

function clearIsMined()
    minedSpots = 0
	for irock, rock in pairs(rocks) do
		rock.isMined = false
	end
end

function miningAnim(heading)
    isMining = true
    if math.random(1,10000) <= 2 then
        exports['mythic_notify']:SendAlert('inform', 'Não é assim que se segura uma picareta!', 1500)
    end
    Citizen.CreateThread(function()
        while impacts < 4 do
            Citizen.Wait(1)
            local ped = PlayerPedId()
            RequestAnimDict("amb@world_human_hammering@male@base")
            Citizen.Wait(100)
            TaskPlayAnim((ped), 'amb@world_human_hammering@male@base', 'base', 12.0, 12.0, -1, 80, 0, 0, 0, 0)
            

            SetEntityHeading(ped, heading)
            if impacts == 0 then
                pickaxe = CreateObject(GetHashKey("prop_tool_pickaxe"), 0, 0, 0, true, true, true)
                AttachEntityToEntity(pickaxe, PlayerPedId(), GetPedBoneIndex(PlayerPedId(), 57005), -0.10, -0.02, -0.02,
                    350.0, 100.00, 110.0, true, true, false, true, 1, true)
            end
            Citizen.Wait(2500)
            impacts = impacts + 1
            if impacts == 4 then
                DetachEntity(pickaxe, 1, true)
                DeleteEntity(pickaxe)
                DeleteObject(pickaxe)
                impacts = 0
                Citizen.Wait(1000)

                TriggerServerEvent("addItemsz:minerJobz")
                isMining = false

                break
            end
        end
    end)
end

RegisterNetEvent("CheckPickaxe:minerJob")
AddEventHandler("CheckPickaxe:minerJob", function()
    TriggerServerEvent("refreshItems:minerJob")
end)

RegisterNetEvent("hasPickaxe:minerJob")
AddEventHandler("hasPickaxe:minerJob", function(pickaxeCount)
    pickaxeCounter = pickaxeCount
end)

local isWashing = false

RegisterNetEvent("washstone:minerJob")
AddEventHandler("washstone:minerJob", function()
	if isWashing == false then
		isWashing = true
		exports['progressBars']:startUI(15000, "A lavar a pedra . . .")
		loadAnimDict('anim@amb@clubhouse@tutorial@bkr_tut_ig3@')
		TaskPlayAnim(GetPlayerPed(-1),'anim@amb@clubhouse@tutorial@bkr_tut_ig3@', 'machinic_loop_mechandplayer',5.0, 1.0, 1.0, 1, 0.0, 0, 0, 0)
		Citizen.Wait(15000)
		ClearPedTasks(GetPlayerPed(-1))
		TriggerServerEvent("washstoneSV:minerJob")
		isWashing = false
	else
		exports['mythic_notify']:SendAlert('error', 'Já estás a lavar, não spames pah!', 1500)
	end
	
end)

function loadAnimDict(dict)
    while (not HasAnimDictLoaded(dict)) do
        RequestAnimDict(dict)
        Citizen.Wait(0)
    end
end

local isMelting = false

RegisterNetEvent("meltstone:minerJob")
AddEventHandler("meltstone:minerJob", function()
	if isMelting == false then
		isMelting = true
		exports['progressBars']:startUI(10000, "A fundir a pedra . . .")
		loadAnimDict('mini@repair')
		TaskPlayAnim(GetPlayerPed(-1),'mini@repair', 'fixing_a_ped',5.0, 1.0, 1.0, 1, 0.0, 0, 0, 0)
		Citizen.Wait(10000)
		ClearPedTasks(GetPlayerPed(-1))
		TriggerServerEvent("meltstoneSV:minerJob")
		isMelting = false
	else
		exports['mythic_notify']:SendAlert('error', 'Já estás a tratar, não spames pah!', 1500)
	end
	
end)
