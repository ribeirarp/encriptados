local drugtype, selling, numberofcops, drugselling = nil, false, 0, false
ESX = nil
local drugname = {
	meth = "Metafetamina",
	coke = "Coca",
	opium = "Ópio",
	weed = "Erva"
}

Citizen.CreateThread(function()
  	while ESX == nil do
    	TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    	Citizen.Wait(250)
  	end

  	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(250)
	end
	
	ESX.PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	ESX.PlayerData.job = job
end)


RegisterCommand("venderdroga", function()

	drugselling = not drugselling
	--exports['mythic_notify']:SendAlert(tostring(drugselling), 'Modo dealer!')
	if drugselling then
		exports["mythic_notify"]:SendAlert( 'inform', "Entraste em modo dealer!", 5000)
	else
		exports["mythic_notify"]:SendAlert( 'error', "Saiste do modo dealer!", 5000)
	end
	


end, false)

Citizen.CreateThread(function()
	TriggerEvent('chat:addSuggestion', '/venderdroga', 'Vender droga.')
end)


Citizen.CreateThread(function()
	while true do
		while drugselling do
			local player = PlayerPedId()
    		--local vehicle = GetVehiclePedIsIn(player, false)

			Citizen.Wait(8)
			if ped ~= 0 and not IsPedDeadOrDying(ped) and not IsPedInAnyVehicle(player) then 
				local pedType = GetPedType(ped)
				if ped ~= oldped and not selling and (IsPedAPlayer(ped) == false and pedType ~= 28) then
						--if numberofcops >= Config.NumberOfCops then
							local pos = GetEntityCoords(ped)
							local distanceFromCity = GetDistanceBetweenCoords(24.1806, -1721.6968, -29.2993, pos['x'], pos['y'], pos['z'], true)
							if distanceFromCity < 2500 then	
								DrawText3Ds(pos.x, pos.y, pos.z, 'Pressiona E para vender droga')
								if IsControlJustPressed(1, 86) then
									--script stress
									TriggerEvent('esx_status:add',  'stress', 10000)
									interact()
								end
							end
				else
					Citizen.Wait(500)
				end
			end
		end
		Citizen.Wait(3000)
	end
end)

--[[RegisterNetEvent('checkR')
AddEventHandler('checkR', function(drug)
  drugtype = drug
end)--]]

Citizen.CreateThread(function()
	while true do
		local playerPed = GetPlayerPed(-1)
		if not IsPedInAnyVehicle(playerPed) or not IsPedDeadOrDying(playerPed) then
			ped = GetPedInFront()
		else
			Citizen.Wait(1000)
		end
		Citizen.Wait(1000)
    end
end)

function GetPedInFront()
	local player = PlayerId()
	local plyPed = GetPlayerPed(player)
	local plyPos = GetEntityCoords(plyPed, false)
	local plyOffset = GetOffsetFromEntityInWorldCoords(plyPed, 0.0, 1.3, 0.0)
	local rayHandle = StartShapeTestCapsule(plyPos.x, plyPos.y, plyPos.z, plyOffset.x, plyOffset.y, plyOffset.z, 1.0, 12, plyPed, 7)
	local _, _, _, _, ped = GetShapeTestResult(rayHandle)
	return ped
end

function DrawText3Ds(x, y, z, text)
	local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local scale = 0.30
    if onScreen then
        SetTextScale(scale, scale)
        SetTextFont(6)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 215)
        SetTextOutline()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end

GetPlayers = function()
    local players = {}
    for _, player in ipairs(GetActivePlayers()) do
        local ped = GetPlayerPed(player)
        if DoesEntityExist(ped) then
            table.insert(players, player)
        end
    end
    return players
end

function interact()
	ESX.TriggerServerCallback('RBRPsellnpc:getData', function( dpr)
		numberofcops = dpr
	end)
	print(numberofcops)

	oldped, selling, drugtype = ped, true, nil
	SetEntityAsMissionEntity(ped)
	TaskStandStill(ped, 9.0)
	exports['progressBars']:startUI(12500, "A negociar")
    Citizen.Wait(12500)
	if not status then
		ESX.TriggerServerCallback('RBRPsellnpc:getItemsToSell', function (cb)
		
			if cb ~= false then
				if Config.IgnorePolice == false then
					if ESX.PlayerData.job.name == 'dpr' then
						exports['mythic_notify']:SendAlert('error', 'O gajo sabe que és polícia, poem-te a milhas!', 4000)
						SetPedAsNoLongerNeeded(oldped)
						selling = false
						return
					end
				end
			
				-- Checks the distance between the PED and the seller before continuing.
				if Config.DistanceCheck then
					if ped ~= oldped then
						exports['mythic_notify']:SendAlert('error', 'Tens de estar perto do vendedor se não ele manda-te dar uma volta!', 5000)
						--TriggerServerEvent("big_skills:addStress", 70000)
						SetPedAsNoLongerNeeded(oldped)
						selling = false
						return
					end
				end
				
				-- It all begins.
				local percent = math.random(1, 10)
				
				if percent <= 1 then
					TriggerEvent('esx_status:add',  'stress', 10000)
					TriggerServerEvent("RBRPsellnpc:allertcops", ped)
					exports['mythic_notify']:SendAlert('error', 'O comprador não queria o teu produto.', 1000)
				elseif percent <= 10 then
					TriggerEvent('esx_status:add',  'stress', 2000)
					local percentRare = math.random(1, 10)
					if percentRare == 1 then
						TriggerServerEvent("RBRPsellnpc:allertcops", ped)
					end
										
					if Config.EnableAnimation == true then
						TriggerEvent("animation", cb)
					end
					Wait(1500)
				
				end
				
				selling = false
				
			end	
		end)
	else
		selling = false
		SetPedAsNoLongerNeeded(oldped)
		FreezeEntityPosition(ped, false)
	end
end

function giveMoneyAnimPed()

    local coords = GetEntityCoords(ped)
    local bone = GetPedBoneIndex(ped, 28422)
    RequestAnimDict("mp_safehouselost@")
    TaskPlayAnim(ped, "mp_safehouselost@", "package_dropoff", 8.0, 1.0, -1, 16, 0, 0, 0, 0)
    SetEntityAsMissionEntity(ped, 1, 1)
    Wait(500)
    local moneyObject = CreateObject(GetHashKey('p_banknote_onedollar_s'), coords.x, coords.y, coords.z, 1, 1, 1)
    AttachEntityToEntity(moneyObject, ped, bone, 0.0, 0.0, 0.0, 80.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
    Wait(3000)
    DeleteObject(moneyObject)
	SetEntityAsMissionEntity(ped, 0, 0)
    ClearPedTasks(ped)
	SetPedAsNoLongerNeeded(oldped)
	FreezeEntityPosition(ped, false)
end

function pedAnimCop()

	local crds = GetEntityCoords(oldped)
	SetRelationshipBetweenGroups(255, GetPedRelationshipGroupHash(oldped), GetPedRelationshipGroupHash(GetPlayerPed(-1)))
	SetEntityAsMissionEntity(oldped, 1, 1)
	RequestAnimDict("cellphone@")
	ClearPedTasks(oldped)
	CellphoneObject = CreateObject(GetHashKey('prop_phone_ing'), crds.x, crds.y, crds.z, 1, 1, 1)
	AttachEntityToEntity(CellphoneObject, oldped, GetPedBoneIndex(oldped, 28422), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
	TaskPlayAnim(oldped, "cellphone@", "cellphone_call_listen_base", 8.0, 1.0, -1, 49, 0, 0, 0, 0)

end


RegisterNetEvent('animation')
AddEventHandler('animation', function(itemtosell)
	local pid = PlayerPedId()
	local itemcount = 0
	RequestAnimDict("mp_safehouselost@")
	while (not HasAnimDictLoaded("mp_safehouselost@")) do
		Citizen.Wait(10)
	end
	 if IsEntityPlayingAnim(ped, "mp_safehouselost@", "package_dropoff", 3) then
	 TaskPlayAnim(pid, "mp_safehouselost@", "package_dropoff", 8.0, 1.0, -1, 16, 0, 0, 0, 0)
	 else
	 TaskPlayAnim(pid, "mp_safehouselost@", "package_dropoff", 8.0, 1.0, -1, 16, 0, 0, 0, 0)
	end
	attachModel = GetHashKey("prop_drug_package_02")
	SetCurrentPedWeapon(PlayerPedId(), 0xA2719263)
	local bone = GetPedBoneIndex(PlayerPedId(), 28422)
	RequestModel(attachModel)
	while not HasModelLoaded(attachModel) do
		Citizen.Wait(100)
	end
	closestEntity = CreateObject(attachModel, 1.0, 1.0, 1.0, 1, 1, 0)
	AttachEntityToEntity(closestEntity, PlayerPedId(), bone, 0.02, 0.02, -0.08, 270.0, 180.0, 0.0, 1, 1, 0, true, 2, 1)
	Citizen.Wait(4000)
	if DoesEntityExist(closestEntity) then
		DeleteEntity(closestEntity)
	end
	SetCurrentPedWeapon(PlayerPedId(), GetHashKey("weapon_unarmed"), 1)
	if itemtosell.count >= 3 then
		itemcount = math.random(1,3)
		TriggerServerEvent('RBRPsellnpc:removeitem', itemtosell.name, itemcount)
	else 
		itemcount = 1
		TriggerServerEvent('RBRPsellnpc:removeitem', itemtosell.name, itemcount)
	end	
	if itemtosell.count >= 1 then
		Citizen.Wait(1000)
		giveMoneyAnimPed()
     	TriggerServerEvent('RBRPsellnpc:addmoneyz', itemtosell.price * itemcount)

		-- if numberofcops == 0 then
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(-200, -100)) * itemcount)
		-- elseif numberofcops > 0 and numberofcops < 3 then
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(-100, 0)) * itemcount)
		-- elseif numberofcops >= 3 and numberofcops < 5 then
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(-50, 0)) * itemcount)
		-- elseif numberofcops >= 5 and numberofcops < 8 then
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(0, 25)) * itemcount)
		-- elseif numberofcops >= 8 and numberofcops < 12 then
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(25, 75)) * itemcount)
		-- elseif numberofcops >= 12 then
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(75, 150)) * itemcount)
		-- else
		-- 	TriggerServerEvent('RBRPsellnpc:addmoneyz', (itemtosell.price + math.random(0, 50)) * itemcount)
		-- end
		SetEntityAsMissionEntity(ped)	
		StopAnimTask(pid, "mp_common","givetake1_a", 1.0)
	else
		exports["mythic_notify"]:SendAlert( 'error', "Não tens mais ganza Pah!", 5000)
	end
end)
