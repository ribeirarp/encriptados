Config = {

OpenKey = '',
OpenCommand = 'abrircaixas',

UsingLimitSystem = false,

DefaultCategory = 'vehicles', -- This category will show up first
ShopCategories = {
	['vehicles'] = 'Veículos',
	['weapons'] = 'Armas',
	['money'] = 'Dinheiro',
    ['items'] = 'Items'
},
--TYPES AVAILABLE BY DEFAULT (Other types can be added in server lua file or request them for another update)
-- car, money, weapon, item
Shop = {
	-- ['bentleygt'] = {type = 'car', model = 'rmodbentleygt', price = 20000, category = 'vehicles'},
    -- ['rs6'] = {type = 'car', model = 'rs6', price = 17000, category = 'vehicles'},
    -- ['money'] = {type = 'money', money= 100000, price = 5000, category = 'money'},
    -- ['medikit'] = {type = 'item', item= 'medikit', count = 1.0, price = 200, category = 'items'},
    -- ['WEAPON_APPISTOL'] = {type = 'weapon', weapon= 'WEAPON_APPISTOL', ammo = 0.0, price = 8000, category = 'weapons'}
},


--TYPES AVAILABLE BY DEFAULT (Other types can be added in server lua file or request them for another update)
--run, drive, fly, 
Tasks = {
    [1] = {reward = 50, type='run', value=100, description = 'CORRE 100 KILÓMETROS'},
    [2] = {reward = 100, type='drive', value=100, description = 'CONDUZ 100 KILÓMETROS'},
    [3] = {reward = 100, type='fly', value=100, description = 'VOA 100 KILÓMETROS'}

},

--RARITIES (From most common to most rare)
-- supercommon, common, rare, superrare, import
--TYPES OF REWARDS
-- car, credits, weapon, item, money
LowestBet = 500, -- The lowest bet amount
MaxBet = 1000,
CaseOpeningItems = { -- The image name should be as the id for this reward


    --supercommon
    ['credits500'] = {type = 'credits', credits = 500.0, exchange = 500.0, rarity = 'supercommon'},
    ['10xmeal3'] = {type = 'item', item = 'meal3', exchange = 0.0, count = 10.0, rarity = 'supercommon'},
    ['10xcharros'] = {type = 'item', item = 'charro', exchange = 0.0, count = 10.0, rarity = 'supercommon'},
    ['WEAPON_KNUCKLE'] = {type = 'weapon', weapon = 'WEAPON_KNUCKLE', ammo = 0.0, exchange = 0.0,  rarity = 'supercommon'},
    ['100xsacodroga'] = {type = 'item', item = 'sacodroga', exchange = 0.0, count = 100.0, rarity = 'supercommon'},
    ['10xweapon_luxary_finish'] = {type = 'item', item = 'weapon_luxary_finish', exchange = 0.0, count = 10.0, rarity = 'supercommon'},
    ['WEAPON_KNIFE'] = {type = 'weapon', weapon = 'WEAPON_KNIFE', ammo = 0.0, exchange = 0.0,  rarity = 'supercommon'},
    ['20xclip'] = {type = 'item', item = 'clip', exchange = 0.0, count = 20.0, rarity = 'supercommon'},
    ['60xfabric'] = {type = 'item', item = 'fabric', exchange = 0.0, count = 60.0, rarity = 'supercommon'},
    ['60xnailon'] = {type = 'item', item = 'nailon', exchange = 0.0, count = 60.0, rarity = 'supercommon'},

	


    -- common
    --['VOUCHER STAND 10K'] = {type = 'item', item = '', exchange = 0.0, count = 1.0, rarity = 'supercommon'},
    ['WEAPON_M700'] = {type = 'weapon', weapon = 'WEAPON_M700', ammo = 0.0, exchange = 0.0,  rarity = 'common'},
    ['credits500'] = {type = 'credits', credits = 500.0, exchange = 500.0, rarity = 'common'},
    ['bp_heavypistol'] = {type = 'item', item = 'bp_heavypistol', exchange = 0.0, count = 1.0, rarity = 'common'},
    ['10xhighgradefert'] = {type = 'item', item = 'highgradefert', exchange = 0.0, count = 10.0, rarity = 'common'},
    ['50xclip'] = {type = 'item', item = 'clip', exchange = 0.0, count = 50.0, rarity = 'common'},
    ['10xhighgradefemaleseed'] = {type = 'item', item = 'highgradefemaleseed', exchange = 0.0, count = 10.0, rarity = 'common'},
    ['20xcomprimido'] = {type = 'item', item = 'comprimido', exchange = 0.0, count = 20.0, rarity = 'common'},
    ['5xarmor'] = {type = 'item', item = 'armor', exchange = 0.0, count = 5.0, rarity = 'common'},
    ['10xlockpick'] = {type = 'item', item = 'lockpick', exchange = 0.0, count = 10.0, rarity = 'common'},
    ['10xeuromilhoes'] = {type = 'item', item = 'euromilhoes', exchange = 0.0, count = 10.0, rarity = 'common'},
    ['100xsacodroga'] = {type = 'item', item = 'sacodroga', exchange = 0.0, count = 100.0, rarity = 'common'},
    


    -- rare
    ['WEAPON_HEAVYPISTOL'] = {type = 'weapon', weapon = 'WEAPON_HEAVYPISTOL', ammo = 0.0, exchange = 0.0,  rarity = 'rare'},
    ['credits1000'] = {type = 'credits', credits = 1000.0, exchange = 1000.0, rarity = 'rare'},
    ['10xadvancedlockpick'] = {type = 'item', item = 'advancedlockpick', exchange = 0.0, count = 10.0, rarity = 'rare'},
    ['15xarmor'] = {type = 'item', item = 'armor', exchange = 0.0, count = 15.0, rarity = 'rare'},
    ['50xligadura'] = {type = 'item', item = 'ligadura', exchange = 0.0, count = 50.0, rarity = 'rare'},
    ['kitsocorros'] = {type = 'item', item = 'kitsocorros', exchange = 0.0, count = 10.0, rarity = 'rare'},
    ['25xhighgradefemaleseed'] = {type = 'item', item = 'highgradefemaleseed', exchange = 0.0, count = 25.0, rarity = 'rare'},
    --['VOUCHER STAND 50K'] = {type = 'item', item = '', exchange = 0.0, count = 1.0, rarity = 'supercommon'},
    ['hacking_laptop'] = {type = 'item', item = 'hacking_laptop', exchange = 0.0, count = 1.0, rarity = 'rare'},
    ['bp_revolver'] = {type = 'item', item = 'bp_revolver', exchange = 0.0, count = 1.0, rarity = 'rare'},
    ['bp_dbshotgun'] = {type = 'item', item = 'bp_dbshotgun', exchange = 0.0, count = 1.0, rarity = 'rare'},
    ['bp_compactrifle'] = {type = 'item', item = 'bp_compactrifle', exchange = 0.0, count = 1.0, rarity = 'rare'},
    ['500xsacodroga'] = {type = 'item', item = 'sacodroga', exchange = 0.0, count = 1.0, rarity = 'rare'},
    

    -- superrare
    ['bp_musket'] = {type = 'item', item = 'bp_musket', exchange = 0.0, count = 1.0, rarity = 'superrare'},
    ['bp_assaultrifle'] = {type = 'item', item = 'bp_assaultrifle', exchange = 0.0, count = 1.0, rarity = 'superrare'},
    ['100xhighgradefemaleseed'] = {type = 'item', item = 'highgradefemaleseed', exchange = 0.0, count = 100.0, rarity = 'superrare'},
    --['VOUCHER STAND 150K'] = {type = 'item', item = '', exchange = 0.0, count = 1.0, rarity = 'supercommon'},
    ['remus'] = {type = 'car', model = 'remus', exchange = 0.0, rarity = 'superrare'},
    ['dominator8'] = {type = 'car', model = 'dominator8', exchange = 0.0, rarity = 'superrare'},
    ['euros'] = {type = 'car', model = 'euros', exchange = 0.0, rarity = 'superrare'},


    -- import
    ['comet6'] = {type = 'car', model = 'comet6', exchange = 0.0, rarity = 'import'},
    --['credits10000'] = {type = 'credits', credits = 10000.0, exchange = 10000.0, rarity = 'rare'},
    ['xt700'] = {type = 'car', model = 'xt700', exchange = 0.0, rarity = 'import'},
    ['fnfjetta'] = {type = 'car', model = 'fnfjetta', exchange = 0.0, rarity = 'import'},
    ['rmod240sx'] = {type = 'car', model = 'rmod240sx', exchange = 0.0, rarity = 'import'},
    ['benze55'] = {type = 'car', model = 'benze55', exchange = 0.0, rarity = 'import'},
    ['fd'] = {type = 'car', model = 'fd', exchange = 0.0, rarity = 'import'},
    ['HONDACIVICTR'] = {type = 'car', model = 'HONDACIVICTR', exchange = 0.0, rarity = 'import'},

    -- ['bandage'] = {type = 'item', item = 'bandage', exchange = 50.0, count = 1.0, rarity = 'common'},
    -- ['medikit'] = {type = 'item', item = 'medikit', exchange = 100.0, count = 1.0, rarity = 'common'},

    -- ['bentleygt'] = {type = 'car', model = 'rmodbentleygt', exchange = 10000.0, rarity = 'import'},
    -- ['rs6'] = {type = 'car', model = 'rmodrs6', exchange = 8000.0, rarity = 'import'},

    -- ['credits20'] = {type = 'credits', credits = 20.0, exchange = 20.0, rarity = 'supercommon'},
    -- ['fixkit'] = {type = 'item', item = 'fixkit', count = 1.0, exchange = 50.0, rarity = 'supercommon'},

    -- ['credits1000'] = {type = 'credits', credits = 1000.0, exchange = 1000.0, rarity = 'rare'},
    -- ['money'] = {type = 'money', money = 100000.0, exchange = 2000.0, rarity = 'rare'},

    -- ['WEAPON_APPISTOL'] = {type = 'weapon', weapon = 'WEAPON_APPISTOL', ammo = 0.0, exchange = 5000.0,  rarity = 'superrare'},
    -- ['credits4000'] = {type = 'credits', credits = 4000.0, exchange = 4000.0, rarity = 'superrare'}

},


BuyCreditsLink = 'ribeira.com.pt/sistemas-de-doacoes/',
BuyCreditsDescription = 'Ao adquirires Ribeira Coins estás a ajudar o Servidor!',

Text = {


    ['item_purschased'] = 'Item Adquirido',
    ['not_enough_credits'] = 'Sem Ribeira Coins suficientes',
    ['wrong_usage'] = 'Uso errado',
    ['item_redeemed'] = 'Regastaste um Item!',
    ['item_exchanged'] = 'Item trocado por Ribeira Coins',
    ['bet_limit_low'] = 'Tens de apostar no mínimo 500 Coins!',
    ['bet_limit_max'] = 'Só podes apostar no máximo 1000 Coins!',
    ['task_completed'] = 'Tarefa Concluída!',
    ['wait_for_spin_end'] = 'Espera que a roleta acabe!',


}

}



function SendTextMessage(msg)

        --SetNotificationTextEntry('STRING')
        --AddTextComponentString(msg)
        --DrawNotification(0,1)

        --EXAMPLE USED IN VIDEO
        exports['mythic_notify']:SendAlert('inform', msg)

end
