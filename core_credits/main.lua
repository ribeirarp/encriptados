local Keys = {
    ["ESC"] = 322,
    ["F1"] = 288,
    ["F2"] = 289,
    ["F3"] = 170,
    ["F5"] = 166,
    ["F6"] = 167,
    ["F7"] = 168,
    ["F8"] = 169,
    ["F9"] = 56,
    ["F10"] = 57,
    ["~"] = 243,
    ["-"] = 84,
    ["="] = 83,
    ["BACKSPACE"] = 177,
    ["TAB"] = 37,
    ["Q"] = 44,
    ["W"] = 32,
    ["E"] = 38,
    ["R"] = 45,
    ["T"] = 245,
    ["Y"] = 246,
    ["U"] = 303,
    ["P"] = 199,
    ["["] = 39,
    ["]"] = 40,
    ["ENTER"] = 18,
    ["CAPS"] = 137,
    ["A"] = 34,
    ["S"] = 8,
    ["D"] = 9,
    ["F"] = 23,
    ["G"] = 47,
    ["H"] = 74,
    ["K"] = 311,
    ["L"] = 182,
    ["LEFTSHIFT"] = 21,
    ["Z"] = 20,
    ["X"] = 73,
    ["C"] = 26,
    ["V"] = 0,
    ["B"] = 29,
    ["N"] = 249,
    ["M"] = 244,
    [","] = 82,
    ["."] = 81,
    ["LEFTCTRL"] = 36,
    ["LEFTALT"] = 19,
    ["SPACE"] = 22,
    ["RIGHTCTRL"] = 70,
    ["HOME"] = 213,
    ["PAGEUP"] = 10,
    ["PAGEDOWN"] = 11,
    ["DELETE"] = 178,
    ["LEFT"] = 174,
    ["RIGHT"] = 175,
    ["TOP"] = 27,
    ["DOWN"] = 173,
    ["NENTER"] = 201,
    ["N4"] = 108,
    ["N5"] = 60,
    ["N6"] = 107,
    ["N+"] = 96,
    ["N-"] = 97,
    ["N7"] = 117,
    ["N8"] = 61,
    ["N9"] = 118
}

ESX = nil

local job = ""
local grade = 0

Citizen.CreateThread(
    function()
        while ESX == nil do
            TriggerEvent(
                "esx:getSharedObject",
                function(obj)
                    ESX = obj
                end
            )
            Citizen.Wait(0)
        end

        while ESX.GetPlayerData().job == nil do
            Citizen.Wait(10)
        end

        job = ESX.GetPlayerData().job.name
        grade = ESX.GetPlayerData().job.grade
    end
)

RegisterNetEvent("esx:setJob")
AddEventHandler(
    "esx:setJob",
    function(j)
        job = j.name
        grade = j.grade
    end
)

RegisterNUICallback(
    "exchange",
    function(data)
        TriggerServerEvent("core_sexy_credits:exchange", data["item"])
    end
)

RegisterNUICallback(
    "redeem",
    function(data)
        TriggerServerEvent("core_sexy_credits:redeem", data["item"])
    end
)

RegisterNUICallback(
    "removeCredits",
    function(data)
        TriggerServerEvent("core_sexy_credits:removeCredits", data["credits"])
    end
)

RegisterNUICallback(
    "openedTasks",
    function(data)
        ESX.TriggerServerCallback(
            "core_sexy_credits:getInfo",
            function(info)
                for k, v in pairs(Config.Tasks) do
                    if not info.tasks_completed[k] then
                        if info.tasks[v.type] == nil then
                            info.tasks[v.type] = 0
                        end
                        if info.tasks[v.type] >= v.value then
                            info.tasks_completed[k] = true
                            TriggerServerEvent("core_sexy_credits:taskCompleted", v)
                            SendNUIMessage(
                                {
                                    type = "completed"
                                }
                            )
                        else
                            info.tasks_completed[k] = false
                        end
                    end
                end

                TriggerServerEvent("core_sexy_credits:updateTasks", info.tasks_completed)
            end
        )
    end
)

RegisterNUICallback(
    "buyItem",
    function(data)
        TriggerServerEvent("core_sexy_credits:buyItem", data["item"])
    end
)

function print_table(node)
    local cache, stack, output = {},{},{}
    local depth = 1
    local output_str = "{\n"

    while true do
        local size = 0
        for k,v in pairs(node) do
            size = size + 1
        end

        local cur_index = 1
        for k,v in pairs(node) do
            if (cache[node] == nil) or (cur_index >= cache[node]) then

                if (string.find(output_str,"}",output_str:len())) then
                    output_str = output_str .. ",\n"
                elseif not (string.find(output_str,"\n",output_str:len())) then
                    output_str = output_str .. "\n"
                end

                -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
                table.insert(output,output_str)
                output_str = ""

                local key
                if (type(k) == "number" or type(k) == "boolean") then
                    key = "["..tostring(k).."]"
                else
                    key = "['"..tostring(k).."']"
                end

                if (type(v) == "number" or type(v) == "boolean") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = "..tostring(v)
                elseif (type(v) == "table") then
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = {\n"
                    table.insert(stack,node)
                    table.insert(stack,v)
                    cache[node] = cur_index+1
                    break
                else
                    output_str = output_str .. string.rep('\t',depth) .. key .. " = '"..tostring(v).."'"
                end
            if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                else
                    output_str = output_str .. ","
                end
            else
                -- close the table
                if (cur_index == size) then
                    output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
                end
            end

            cur_index = cur_index + 1
        end

        if (size == 0) then
            output_str = output_str .. "\n" .. string.rep('\t',depth-1) .. "}"
        end

        if (#stack > 0) then
            node = stack[#stack]
            stack[#stack] = nil
            depth = cache[node] == nil and depth + 1 or depth - 1
        else
            break
        end
    end

    -- This is necessary for working with HUGE tables otherwise we run out of memory using concat on huge strings
    table.insert(output,output_str)
    output_str = table.concat(output)

    print(output_str)
end

RegisterNUICallback(
    "sendMessage",
    function(data)
        SendTextMessage(Config.Text[data["message"]])
    end
)

RegisterNUICallback(
    "addWinning",
    function(data)
        TriggerServerEvent("core_sexy_credits:addWinning", data["item"])
    end
)

RegisterNUICallback(
    "close",
    function(data)
        TriggerScreenblurFadeOut(1000)
        SetNuiFocus(false, false)
    end
)

RegisterNetEvent("core_sexy_credits:updateCredits")
AddEventHandler(
    "core_sexy_credits:updateCredits",
    function()
        ESX.TriggerServerCallback(
            "core_sexy_credits:getInfo",
            function(info)
                SendNUIMessage(
                    {
                        type = "credits",
                        credits = info.credits,
                        task_progress = info.tasks,
                        winnings = info.winnings
                    }
                )
            end
        )
    end
)

RegisterNetEvent("core_sexy_credits:sendMessage")
AddEventHandler(
    "core_sexy_credits:sendMessage",
    function(msg)
        SendTextMessage(msg)
    end
)

function openCredits()
    ESX.TriggerServerCallback(
        "core_sexy_credits:getInfo",
        function(info)
            SendNUIMessage(
                {
                    type = "credits",
                    credits = info.credits,
                    task_progress = info.tasks,
                    winnings = info.winnings
                }
            )
        end
    )

    SetNuiFocus(true, true)
    TriggerScreenblurFadeIn(1000)

    SendNUIMessage(
        {
            type = "open",
            name = string.upper(GetPlayerName(PlayerId())),
            shop_categories = Config.ShopCategories,
            lowest_bet = Config.LowestBet,
            max_bet = Config.MaxBet,
            header = string.upper(Config.BuyCreditsLink),
            description = Config.BuyCreditsDescription,
            shop_items = Config.Shop,
            shop_def = Config.DefaultCategory,
            tasks = Config.Tasks,
            caseopening_items = Config.CaseOpeningItems
        }
    )
end

RegisterCommand(
    Config.OpenCommand,
    function()
        openCredits()
    end
)

RegisterKeyMapping(Config.OpenCommand, "Open Credits Menu", "keyboard", Config.OpenKey)

local lastrun, run, running = 0, 0, false
local lastdrive, drive, driving = 0, 0, false
local lastfly, fly, flying = 0, 0, false

Citizen.CreateThread(
    function()
        local cord = GetEntityCoords(PlayerPedId())
        lastrun = cord
        lastdrive = cord

        while true do
            Citizen.Wait(1000)

            local ped = PlayerPedId()
            local coords = GetEntityCoords(ped)
            local speed = GetEntitySpeed(ped)

            if speed > 6.0 and IsPedOnFoot(ped) then
                run = run + #(coords - lastrun)
                lastrun = coords
                running = true
            else
                if running then
                    running = false
                    TriggerServerEvent("core_sexy_credits:progressTask", "run", round(run) / 1000)
                    run = 0
                end

                lastrun = coords
            end

            if speed > 10.0 and IsPedInAnyVehicle(ped, false) then
                if IsVehicleOnAllWheels(GetVehiclePedIsIn(ped, false)) then
                    drive = drive + #(coords - lastdrive)
                    lastdrive = coords
                    driving = true
                end
            else
                if driving then
                    driving = false
                    TriggerServerEvent("core_sexy_credits:progressTask", "drive", round(drive) / 1000)
                    drive = 0
                end

                lastdrive = coords
            end

            if speed > 10.0 and IsPedInFlyingVehicle(ped) then
                fly = fly + #(coords - lastfly)
                lastfly = coords
                flying = true
            else
                if flying then
                    flying = false
                    TriggerServerEvent("core_sexy_credits:progressTask", "fly", round(fly) / 1000)
                    fly = 0
                end

                lastfly = coords
            end
        end
    end
)

function round(x)
    return x >= 0 and math.floor(x + 0.5) or math.ceil(x - 0.5)
end

function DrawText3D(x, y, z, text)
    local onScreen, _x, _y = World3dToScreen2d(x, y, z)
    local px, py, pz = table.unpack(GetGameplayCamCoord())
    local dist = GetDistanceBetweenCoords(px, py, pz, x, y, z, 1)

    local scale = ((1 / dist) * 2) * (1 / GetGameplayCamFov()) * 100

    if onScreen then
        SetTextColour(255, 255, 255, 255)
        SetTextScale(0.0 * scale, 0.35 * scale)
        SetTextFont(4)
        SetTextProportional(1)
        SetTextCentre(true)

        SetTextDropshadow(1, 1, 1, 1, 255)

        BeginTextCommandWidth("STRING")
        AddTextComponentString(text)
        local height = GetTextScaleHeight(0.55 * scale, 4)
        local width = EndTextCommandGetWidth(4)

        SetTextEntry("STRING")
        AddTextComponentString(text)
        EndTextCommandDisplayText(_x, _y)
    end
end
