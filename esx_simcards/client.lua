ESX                           = nil  
local PlayerData              = {}  

Citizen.CreateThread(function()
    while ESX == nil do
      TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
      Citizen.Wait(0)
    end
    while ESX.GetPlayerData().job == nil do
      Citizen.Wait(10)
    end
    PlayerData = ESX.GetPlayerData()
end)
  
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
    PlayerData = xPlayer    
end)

RegisterNetEvent('simCard$:changeNumber')
AddEventHandler('simCard$:changeNumber', function(xPlayer) 
    PlayerData = xPlayer   
    ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'number', {
        title = "Novo número (Ex: 1234567)"
    }, function(data, menu)
        local number = data.value
        if number ~= nil then
            local rawNumber = number
            local isValid = tonumber(rawNumber) ~= nil
            if string.len(rawNumber) == 7 then
                if isValid then
                    local inventory = ESX.GetPlayerData().inventory
                    local simCardCount = nil
                    for i=1, #inventory, 1 do                          
                        if inventory[i].name == 'sim_card' then
                            simCardCount = inventory[i].count
                        end
                    end
                    if simCardCount > 0 then
                        TriggerServerEvent('simCard$:useSimCard', number)               
                    else 
                        ESX.ShowNotification("Não tens nenhum cartão SIM novo")
                    end  
                else
                    ESX.ShowNotification('O número de telefone só pode conter números!')
                end             
            else
                ESX.ShowNotification('O número de telefone tem de ter ~r~7 ~w~dígitos (Ex: 1234567)')
            end
            menu.close()                 
        else
            ESX.ShowNotification('~r~Não foi introduzido nenhum número!')
            menu.close()
        end

    end, 
    function(data, menu)
        menu.close()
    end) 
end)

RegisterNetEvent('matriarch_simcards:startNumChange')
AddEventHandler('matriarch_simcards:startNumChange', function(newNum)
    if Config.UseAnimation then
        
        -- PROGRESS START
        -- REPLACE THIS WITH WHATEVER PROGRESS MOD YOU USE
        local complete = false
        Citizen.Wait(Config.TimeToChange)
        complete = true
        -- PROGRESS END        
        if complete then
            TriggerServerEvent('simCard$:changeNumber', newNum)        
            ESX.ShowNotification('Novo número de telemóvel: ~g~' .. newNum)                                        
            if Config.gcphoneEnabled then
                Citizen.Wait(3000) -- just give the update 2 seconds to hit DB before calling the gcphone update
                TriggerServerEvent('gcPhone:allUpdate')
            end         
        end   
    else
        TriggerServerEvent('simCard$:changeNumber', newNum)   
        ESX.ShowNotification('Novo número de telemóvel ~g~' .. newNum)
        Citizen.Wait(3000)                             
        if Config.gcphoneEnabled then
            TriggerServerEvent('gcPhone:allUpdate')
        end  
    end
	 
end)

function loadanimdict(dictname)
	if not HasAnimDictLoaded(dictname) then
		RequestAnimDict(dictname) 
		while not HasAnimDictLoaded(dictname) do 
			Citizen.Wait(1)
		end
	end
end